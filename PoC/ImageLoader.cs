﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{
    class ImageLoader
    {


        readonly static Bitmap[] _array = new Bitmap[byte.MaxValue + 1];

        private static ImageLoader instance = new ImageLoader();
        

        /// <summary>
        /// Load each texture that is in the ressource directory in a bitmap array 
        /// </summary>
        public ImageLoader() 
        {
            if (!File.Exists("resources/missing.png"))
            {
                throw new FileNotFoundException("Missing texture not found");
            }
            for (int i = 0; i < _array.Length; i++)
            {
                if (!File.Exists("resources/" + i + ".png"))
                    _array[i] = new Bitmap("resources/missing.png");
                else 
                    _array[i] = new Bitmap("resources/" + i + ".png");

            }
        }

        public static ImageLoader Instance
        {
            get { return instance; }
        }

        public Bitmap this[int index]
        {
            get { return _array[index]; }
        }

        public int BitmapArrayLength
        {
            get { return _array.Length; }
        }

        public int TotalWidth()
        {
            int resultat = 0;
            for (int i = 0; i < instance.BitmapArrayLength ; i++)
            {
                resultat += instance[i].Width;
            }
            return resultat;
        }

        public int Higher()
        {
            int max = 0;
            for (int i = 0; i < instance.BitmapArrayLength; i++)
            {
                if (_array[i].Height > max)
                    max = _array[i].Height;
            }
            return max;
        }
    }
}
