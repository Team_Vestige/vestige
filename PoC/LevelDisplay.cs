﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;

namespace PoC
{
    class LevelDisplay
    {
        RectangleShape[,] _terrain2D;
        RenderWindow _window;
        char[,] _pos;
        public char[,] _terrain;
        public LevelDisplay(RenderWindow window, string fkingentry, int fkingheight, int fkingwidth) {
            _window = window;
            string[] fkingarray = fkingentry.Split('\n');
            foreach(var temp in fkingarray ) {
                if(temp.Length > 1)
                temp.Remove('\r');
            }
            Texture chemin = new Texture("ressources/chemin.png");
            Texture  mur = new Texture("ressources/mur.png");

            _terrain = new char[fkingarray.Length, fkingarray[0].Length];
            _terrain2D = new RectangleShape[fkingarray.Length, fkingarray[0].Length];
            for(int x = 0;x < 49;x++) {
                for(int y = 0;y <49;y++) {
                    _terrain[y, x] = fkingarray[x][y];
                }
            }


            for(int x = 0;x < 49;x++) {
                for(int y = 0;y < 49;y++) {
                    _terrain2D[x, y] = new RectangleShape();
                    _terrain2D[x, y].OutlineThickness = 1;
                    _terrain2D[x, y].Size = new Vector2f(fkingheight / 50, fkingwidth / 50);
                    _terrain2D[x, y].Position = new Vector2f(x*(fkingheight / 50), y*(fkingwidth / 50));
                    _terrain2D[x, y].FillColor = Color.Black;
                    if(_terrain[x, y] == 'o') {
                        _terrain2D[x, y].Texture = chemin;
                    } else {
                        _terrain2D[x, y].Texture = mur;
                    }
                }
            }
        }

        public void Display() {
            for(int x = 0;x < 49;x++) {
                for(int y = 0;y < 49;y++) {
                    _window.Draw(_terrain2D[x,y]);
                }
            }
        }
        internal int Absolu (int b)
        {
            if(b < 0)
            {
                b *= -1;
                return b;
            } else
            {
                return b;
            }
        }


        public void ResetLight()
        {
            for (int x = 0; x < 49; x++)
            {
                for (int y = 0; y < 49; y++)
                {
                    _terrain2D[x, y].FillColor = Color.Black;
                }
            }
        }

        public void Light (Vector2i pos)
        {

            if(pos.X >= 0 && pos.X  < _terrain2D.GetLength(0)-5 && pos.Y  >= 0 && pos.Y < _terrain2D.GetLength(1)-5)
            {
                _terrain2D[pos.X, pos.Y].FillColor = Color.White;
            }
            

            for (int i = -2; i < 3; i++)
            {
                for (int o = -2; o < 3; o++)
                {
                    if(pos.X + i >= 0 && pos.X + i < _terrain2D.GetLength(0)-5 && pos.Y + o >= 0 && pos.Y + o < _terrain2D.GetLength(1)-1)
                    {
                        _terrain2D[pos.X + i, pos.Y + o].FillColor = Color.White;
                        int distance = Absolu(i) + Absolu(o);
                        int degree = 255 - (distance * 255) / 5;
                        _terrain2D[pos.X + i, pos.Y + o].FillColor = new Color((byte)degree, (byte)degree, (byte)degree);
                    }
                   

                }
            }


        }
        
    }
}
