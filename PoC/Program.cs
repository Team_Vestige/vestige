﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;

namespace PoC
{
    class Program
    {
        static void Main(string[] args) {
            const int gameWidth = 1100;
            const int gameHeight = 1100;
            int x = 0;
            int y = 50;
            RenderWindow window = new RenderWindow(new VideoMode(gameWidth, gameHeight), "Liquid Test", Styles.Titlebar | Styles.Close);
            string text = System.IO.File.ReadAllText(@"ressources/laby.txt");
            LevelDisplay jeu = new LevelDisplay(window, text, gameWidth,gameHeight);
            Perso perso = new Perso(x, y, window);

            while(true) {
                window.DispatchEvents();
                // Clear the window
                window.Clear(new Color(255, 255, 255));
                // Window closed or escape key pressed: exit
                if(Keyboard.IsKeyPressed(Keyboard.Key.Escape)) {
                    window.Close();
                    break;
                }
                Vector2f oldPos = perso.Pos;

                if(Keyboard.IsKeyPressed(Keyboard.Key.Up))
                {
                    
                    perso.Pos = new Vector2f(perso.Pos.X, perso.Pos.Y - 2);
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Down))
                {

                    perso.Pos = new Vector2f(perso.Pos.X, perso.Pos.Y + 2);
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Right))
                {

                    perso.Pos = new Vector2f(perso.Pos.X + 2, perso.Pos.Y );
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Left))
                {

                    perso.Pos = new Vector2f(perso.Pos.X - 2, perso.Pos.Y);
                }
                // Finally, display the rendered frame on screen

               if(perso.CurrentTile(1000, 1000).X >= 0 && perso.CurrentTile(1000, 1000).X < jeu._terrain.GetLength(0) - 5 && perso.CurrentTile(1000, 1000).Y >= 0 && perso.CurrentTile(1000, 1000).Y < jeu._terrain.GetLength(1) - 5 && jeu._terrain[perso.CurrentTile(1000, 1000).X, perso.CurrentTile(1000, 1000).Y] == 'x' )
                {
                    perso.Pos = oldPos;
                }

                Vector2i currentTile = perso.CurrentTile(1100, 1100);
                jeu.ResetLight();
                jeu.Light(currentTile);
                jeu.Display();
                perso.Display();
                window.Display();
            }
        }
    }
}
