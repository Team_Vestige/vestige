﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;

namespace PoC
{
    class Perso
    {
        RenderWindow _window;
        RectangleShape _perso;

        public Perso(int x, int y, RenderWindow window)
        {
            _window = window;
            Texture img = new Texture("ressources/alien.png");
            _perso = new RectangleShape();
            _perso.Size = new Vector2f(22, 22);
            _perso.Texture = img;
            _perso.FillColor = Color.White;
            _perso.Position = new Vector2f(x, y);
        }

        public Vector2f Pos {
            get { return _perso.Position; }
            set { _perso.Position = value; }
        }

        public Vector2i CurrentTile(int height, int width)
        {
           
            int tileSize = 22;
            return new Vector2i((int)(_perso.Position.X - _perso.Position.X % tileSize) / tileSize, (int)(_perso.Position.Y - _perso.Position.Y % tileSize) / tileSize);
        }

        public void Display()
        {
            _window.Draw(_perso);
        }
    }
}