﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;

namespace PoC
{
    class PathFinder
    {
        RenderWindow _window;
        Vector2f _start;
        Vector2f _end;
        int[,] _map;
        public PathFinder(RenderWindow window, Vector2f start, Vector2f end, char[,] map)
        {
            _start = start;
            _end = end;
            _window = window;
            _map = new int[map.GetLength(0), map.GetLength(1)];
            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int y = 0; y < map.GetLength(1); y++)
                {
                    if (map[x, y] == 'x')
                    {
                        _map[x, y] = -1;
                    } else
                    {
                        _map[x, y] = 0;
                    }
                }
            }
            List<Vector2f> points = Path();
        }
        public int Abs(int num)
        {
            if (num < 0) return num * -1;
            return num;
        }
        public List<Vector2f> Path()
        {
            int max = _map.Cast<int>().Max();
            int x = (int)_end.X;
            int y = (int)_end.Y;
            List<Vector2f> Paths = new List<Vector2f>();
            if (!FindPath())
            {
                return new List<Vector2f>();
            }
            int[] newCoo;
            Paths.Add(new Vector2f(x, y));
            while (max > 1)
            {
                newCoo = FindSmaller(x, y);
                x = newCoo[0];
                y = newCoo[1];
                max = _map[x, y];
                Paths.Add(new Vector2f(x, y));
            }
            return Paths;
        }

        public int[] FindSmaller(int x, int y)
        {
            for (int i = 1; x < _map.GetLength(0); x++)
            {
                for (int j = -1; y < _map.GetLength(1); y++)
                {
                    if (Abs(i) + Abs(j) <= 1 && x + i >= 0 && x + i < _map.GetLength(0) && y + j >= 0 && y + j < _map.GetLength(1) && _map[(int)x + i, (int)y + j] < _map[x, y])
                    {
                        return new int[2] { x + i, y + j };
                    }
                }
            }
            return new int[2] { -1, -1 };//BALANCE UNE PUTAIN D'ERREUR, ET OUI C'EST MOCHE. MAIS C'EST LE POC.
        }
        public bool FindPath()
        {
            bool foundPath;
            int Distance = 2;
            ChangeProximity(_start, 1);
            while (true)
            {
                foundPath = false;
                for (int x = 0; x < _map.GetLength(0); x++)
                {
                    for (int y = 0; y < _map.GetLength(1); y++)
                    {
                        if(_map[x,y] == Distance-1)
                        {
                            
                            if (ChangeProximity(new Vector2f(x, y), Distance)) {
                                foundPath = true;
                            }
                            if (new Vector2f(x, y) == _end)
                            {
                                return true;
                            }
                        }
                    }
                }
                if (!foundPath)
                    return false;
                Distance++;
            }
        }

      

        public bool ChangeProximity(Vector2f pos, int actualNumber)
        {
            bool modified = false;
            for(int i =-1; i<2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if(Abs(i)+Abs(j) <= 1 && pos.X+i>=0 && pos.X+i<_map.GetLength(0) && pos.Y+j>=0 && pos.Y + j < _map.GetLength(1) && _map[(int) pos.X+i, (int) pos.Y+j] == 0)
                    {
                        modified = true;
                        _map[(int)pos.X + i, (int)pos.Y + j] = actualNumber;
                    }

                }
            }
            return modified;
        }
    }
}
