﻿using System;
using SFML.Graphics;
using SFML.System;
using Vestige.GUI;


namespace Vestige
{
    static class Menu
    {
        static MenuType _choice;
        static RectangleShape _ledPlay;
        static RectangleShape _ledLoad;
        static RectangleShape _ledSettings;
        static RectangleShape _ledExit;
        static readonly Texture TextureRedLed = new Texture("resources/RedLed.png");
        static readonly Texture TextureNewGame = new Texture("resources/NewGame.png");
        static readonly Texture TextureNewGameOver = new Texture("resources/NewGame_Over.png");
        static readonly Texture TextureLoadGame = new Texture("resources/LoadGame.png");
        static readonly Texture TextureLoadGameOver = new Texture("resources/LoadGame_Over.png");
        static readonly Texture TextureSettings = new Texture("resources/Settings.png");
        static readonly Texture TextureSettingsOver = new Texture("resources/Settings_Over.png");
        static readonly Texture TextureExit = new Texture("resources/ExitGame.png");
        static readonly Texture TextureExitOver = new Texture("resources/ExitGame_Over.png");
        static readonly Texture TextureGreenLed = new Texture("resources/GreenLed.png");
        public static MenuType RunMenu(GUIMananager guiMananager)
        {
            RenderWindow window = guiMananager.Window;
            Vector2f buttonSize = new Vector2f(370, 164);
            float buttonPositionX = window.DefaultView.Center.X - (buttonSize.X / 2);
            float buttonPositionY = window.DefaultView.Center.Y - (buttonSize.Y / 2);
            
            _ledPlay = new RectangleShape()
            {
                Size = new Vector2f(53, 53),
                Position = new Vector2f(buttonPositionX - 70, buttonPositionY - 265),
                Texture = TextureRedLed
            };
            _ledLoad = new RectangleShape()
            {
                Size = new Vector2f(53, 53),
                Position = new Vector2f(buttonPositionX - 70, buttonPositionY - 60),
                Texture = TextureRedLed
            };
            _ledSettings = new RectangleShape()
            {
                Size = new Vector2f(53, 53),
                Position = new Vector2f(buttonPositionX - 70, buttonPositionY + 140),
                Texture = TextureRedLed
            };
            _ledExit = new RectangleShape()
            {
                Size = new Vector2f(53, 53),
                Position = new Vector2f(buttonPositionX - 70, buttonPositionY + 335),
                Texture = TextureRedLed
            };
            Button play = new Button(guiMananager,0,false)
            {
                Size = buttonSize,
                Position = new Vector2f(buttonPositionX, buttonPositionY - 300),
                Texture = TextureNewGame
            };
            play.OnClickRelease += Play_OnClickRelease;
            play.OnOverEnter += Play_OnOverEnter;
            play.OnOverExit += Play_OnOverExit;
            Button loadGame = new Button(guiMananager, 0, false)
            {
                Position = new Vector2f(buttonPositionX, buttonPositionY - 100),
                Size = buttonSize,
                Texture = TextureLoadGame,
            };
            loadGame.OnClickRelease += LoadGame_OnClickRelease;
            loadGame.OnOverEnter += LoadGame_OnOverEnter;
            loadGame.OnOverExit += LoadGame_OnOverExit;
            Button options = new Button(guiMananager, 0, false)
            {
                Position = new Vector2f(buttonPositionX, buttonPositionY + 100),
                Size = buttonSize,
                Texture = TextureSettings,
            };
            options.OnClickRelease += Options_OnClickRelease;
            options.OnOverEnter += Options_OnOverEnter;
            options.OnOverExit += Options_OnOverExit; 
            Button quit = new Button(guiMananager, 0, false)
            {
                Position = new Vector2f(buttonPositionX, buttonPositionY + 300),
                Size = buttonSize,
                Texture = TextureExit
            };
            quit.OnClickRelease += Quit_OnClickRelease;
            quit.OnOverEnter += Quit_OnOverEnter;
            quit.OnOverExit += Quit_OnOverExit;
            _choice = MenuType.Menu;
            while (_choice == MenuType.Menu)
            {
                window.DispatchEvents();
                window.Clear();
                window.Draw(play);
                window.Draw(_ledPlay);
                window.Draw(loadGame);
                window.Draw(_ledLoad);
                window.Draw(options);
                window.Draw(_ledSettings);
                window.Draw(quit);
                window.Draw(_ledExit);
                window.Display();
            }
            guiMananager.Unsuscribe();
            return _choice;
        }

        static void Quit_OnOverExit(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture = TextureExit;
                _ledExit.Texture = TextureRedLed;
            }
        }

        static void Quit_OnOverEnter(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture = TextureExitOver;
                _ledExit.Texture = TextureGreenLed;
            }
        }

        static void Options_OnOverExit(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture = TextureSettings;
                _ledSettings.Texture = TextureRedLed;
            }
        }

        static void Options_OnOverEnter(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture = TextureSettingsOver;
                _ledSettings.Texture = TextureGreenLed;
            }
        }

        static void LoadGame_OnOverExit(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture = TextureLoadGame;
                _ledLoad.Texture = TextureRedLed;
            }
        }

        static void LoadGame_OnOverEnter(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture = TextureLoadGameOver;
                _ledLoad.Texture = TextureGreenLed;
            }
        }

        private static void Play_OnOverEnter(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture = TextureNewGameOver;
                _ledPlay.Texture = TextureGreenLed;
            }
        }

        private static void Play_OnOverExit(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                button.Texture =TextureNewGame;
                _ledPlay.Texture =TextureRedLed;
            }
        }

        static void Quit_OnClickRelease(object sender, EventArgs e)
        {
            _choice = MenuType.Quit;
        }

        static void Options_OnClickRelease(object sender, EventArgs e)
        {
            _choice = MenuType.Option;
        }

        static void LoadGame_OnClickRelease(object sender, EventArgs e)
        {
            _choice = MenuType.Load;
        }

        static void Play_OnClickRelease(object sender, EventArgs e)
        {
            _choice = MenuType.Game;
        }
    }
}
