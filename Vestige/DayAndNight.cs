﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace Vestige
{ 
    class DayAndNight : Drawable
    {
        Game _game;
        RectangleShape _light;

        public DayAndNight(Game game)
        {
            _game = game;
            _light = new RectangleShape {Size = (Vector2f) game.Window.Size};
        }


        public void Draw(RenderTarget target, RenderStates states)
        {          
            float second = Math.Abs(_game.DayTime.AsSeconds() / _game.TimeInAday.AsSeconds()-0.5f);
            if (second * 255 < 225)
                _light.FillColor = new Color(0,0,0,(byte)(second*255));
            if (second * 255 == 200)
            {
                _light.FillColor = new Color(0, 0, 0, (byte)(second * 255));
            }
            target.Draw(_light);
        }
    }
}
