﻿using SFML.Graphics;
using SFML.System;

namespace Vestige
{
    sealed class ButtonText : Button
    {
        readonly Text _buttonText;
        public ButtonText(RenderWindow window, string _text, Font font) : base(window)
        {
            _buttonText = new Text(_text, font);
        }
        public ButtonText(RenderWindow window, string _text, Font font, Vector2f size) : base(window)
        {
            _buttonText = new Text(_text, font);
            Size = size;
        }
        public string Text
        {
            get => _buttonText.DisplayedString;
            set => _buttonText.DisplayedString = value;
        }
        public override Vector2f Position
        {
            get => base.Position;
            set { base.Position = value;
                _buttonText.Position = value;
            }
        }

        public uint CharacterSize
        {
            get => _buttonText.CharacterSize;
            set => _buttonText.CharacterSize = value;
        }

        public Font Font
        {
            get => _buttonText.Font;
            set => _buttonText.Font = value;
        }
        public Vector2f Scale
        {
            get => _buttonText.Scale;
            set => _buttonText.Scale = value;
        }

        public Color Color
        {
            get => _buttonText.Color;
            set => _buttonText.Color = value;
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            base.Draw(target, states);
            _buttonText.Draw(target, states);
        }
    }
}
