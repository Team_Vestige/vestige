﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using Vestige.Unit;

namespace Vestige
{
    class Hud : Drawable
    {
        readonly Game _game;
        readonly RectangleShape _miniMap;
        readonly RectangleShape _inventory;
        readonly RectangleShape _stats;
        readonly RectangleShape _action;
        readonly Text _text;
        readonly Text _text1;
        readonly Text _text2;
        readonly Text _text3;
        Text _text4;
        readonly List<Text> _texts = new List<Text>();

        public Hud(Game game)
        {
            _game = game;
            float heightWindow = game.Window.DefaultView.Size.Y;
            float widthWindow = game.Window.DefaultView.Size.X;
            Color colorHud = new Color(110, 110, 110, 175);

            _miniMap = new RectangleShape
            {
                Size = new Vector2f(widthWindow / 7, (float)(heightWindow / 4.5)),
                Position = new Vector2f(widthWindow / 100, (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };

            _inventory = new RectangleShape
            {
                Size = new Vector2f(widthWindow / 3, (float)(heightWindow / 4.5)),
                Position = new Vector2f(widthWindow / 6, (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };

            _stats = new RectangleShape
            {
                Size = new Vector2f(widthWindow / 4, (float)(heightWindow / 4.5)),
                Position = new Vector2f((float)(widthWindow / 1.95), (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };

            _action = new RectangleShape
            {
                Size = new Vector2f((float)(widthWindow / 4.7), (float)(heightWindow / 4.5)),
                Position = new Vector2f((float)(widthWindow / 1.29), (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };

            //Texture 

            _text = new Text("Mini-Map", Fonts.Basic)
            {
                Color = Color.Black,
                CharacterSize = 50,
                Position = new Vector2f(widthWindow / 100, (float)(heightWindow / 1.3))
            };


            _text1 = new Text("Inventory", Fonts.Basic)
            {
                Color = Color.Black,
                CharacterSize = 50,
                Position = new Vector2f(widthWindow / 6, (float)(heightWindow / 1.3))
            };

            _text2 = new Text("stats", Fonts.Basic)
            {
                Color = Color.Black,
                CharacterSize = 50,
                Position = new Vector2f((float)(widthWindow / 1.95), (float)(heightWindow / 1.3))
            };

            _text3 = new Text("Action", Fonts.Basic)
            {
                Color = Color.Black,
                CharacterSize = 50,
                Position = new Vector2f((float)(widthWindow / 1.29), (float)(heightWindow / 1.3))
            };

            _text4 = new Text("", Fonts.Basic)
            {
                Color = Color.Black,
                CharacterSize = 25,
                Position = new Vector2f((float)(widthWindow / 1.95), (float)(heightWindow / 1.22))
            };
            game.Update += Game_Update;
        }

        void Game_Update(object sender, Time e)//TODO we need to change the text only when there is changement, not every frame.
        {
            float heightWindow = _game.Window.DefaultView.Size.Y;
            float widthWindow = _game.Window.DefaultView.Size.X;
            _texts.Clear();
            int i = 0;
            foreach (Unit.Unit unit in _game.UnitController.SelectedUnit)
            {
                _text4 = new Text("ID = " + Convert.ToString(unit.GetId), Fonts.Basic)
                {
                    Color = Color.Black,
                    CharacterSize = 25,
                    Position = new Vector2f((float)(widthWindow / 1.95), (float)(heightWindow / 1.22 + (25 * i)))
                };
                _texts.Add(_text4);
                i++;
            }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {

            _miniMap.Draw(target, states);
            _inventory.Draw(target, states);
            _stats.Draw(target, states);
            _action.Draw(target, states);
            _text.Draw(target, states);
            _text1.Draw(target, states);
            _text2.Draw(target, states);
            _text3.Draw(target, states);
            foreach (var text in _texts)
            {
                text.Draw(target, states);
            }

        }
    }
}