﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Vestige
{
    class ImageLoader
    {

        static readonly Bitmap[] Array = new Bitmap[byte.MaxValue + 1];

        static ImageLoader _instance = new ImageLoader();     

        /// <summary>
        /// Load each texture that is in the ressource directory in a bitmap array 
        /// </summary>
        ImageLoader() 
        {
            if (!File.Exists("resources/missing.png"))
            {
                throw new FileNotFoundException("Missing texture not found");
            }
            for (int i = 0; i < Array.Length; i++)
            {
                if (!File.Exists("resources/" + i + ".png"))
                {
                    Array[i] = new Bitmap("resources/missing.png");
                }
                else
                {
                    Array[i] = new Bitmap("resources/" + i + ".png");
                }
            }
        }

        public static ImageLoader Instance => _instance ?? (_instance = new ImageLoader());

        public Bitmap this[int index] => Array[index];

        public static int BitmapArrayLength => Array.Length;

        public static int TotalWidth()
        {
            int resultat = 0;
            for (int i = 0; i < BitmapArrayLength ; i++)
            {
                resultat += Array[i].Width;
            }
            return resultat;
        }

        public static int Higher()
        {
            int max = Array[0].Height;

            for (int i = 0; i < BitmapArrayLength; i++)
            {
                if (Array[i].Height > max)
                {
                    max = Array[i].Height;
                }
            }
            return max;
        }
    }
}
