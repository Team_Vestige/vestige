﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vestige.Map;

namespace Vestige
{
    class DrawBuilding : Drawable
    {
        readonly Game _game;
        readonly RectangleShape _shape;
        CreateUnit _unitCreator;
        public delegate void CreateUnit(Vector2l pos);

        TileType _canOverride;
        public DrawBuilding(Game game, TileType tileType, CreateUnit unitCreator, TileType canOverride)
        {
            _canOverride = canOverride;
            _unitCreator = unitCreator;
            _game = game;
            game.DrawBuilding = this;
            _shape = new RectangleShape
            {
                FillColor = new Color(0, 120, 0, 200),
                Size = new Vector2f(1, 1),
                Texture = TextureArray.TileSet,
                TextureRect = TextureArray.GetTextureCoord((int)tileType).IntRect()
            };
            game.GameEvents.MouseButtonPressed += MouseButtonPressed;
        }

        void MouseButtonPressed(object sender, MouseButtonEventArgs keyEventArgs)
        {
            Chunk chunk = _game.Map.GetChunkOfPoint((Vector2l) _shape.Position);
            var relative = chunk.GetRelativeCoordinate((Vector2l) _shape.Position);
            if (chunk[relative].Type == _canOverride)
            {
                _game.DrawBuilding = null;
                _unitCreator((Vector2l)_shape.Position);
                _game.GameEvents.MouseButtonPressed -= MouseButtonPressed;
            }
        }


        public void Draw(RenderTarget target, RenderStates states)
        {
            Chunk chunk = _game.Map.GetChunkOfPoint((Vector2l)_shape.Position);
            var relative = chunk.GetRelativeCoordinate((Vector2l)_shape.Position);
            if (chunk[relative].Type == _canOverride)
            {
                _shape.FillColor = new Color(0, 120, 0, 200);
            }
            else
            {
                _shape.FillColor = new Color(120, 0, 0, 200);
            }
                var view = _game.CameraView;
            var cursorPos = view.Center - view.Size / 2 + Common.MultiplyVector(view.Size, Common.Divide(Mouse.GetPosition(_game.Window), (Vector2d)_game.Window.Size));
            _shape.Position = new Vector2f((int)Math.Floor(cursorPos.X), (int)Math.Floor(cursorPos.Y));
            _shape.Draw(target, states);
        }
    }
}
