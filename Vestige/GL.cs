﻿using System;
using System.Runtime.InteropServices;
using System.Security;

// ReSharper disable once InconsistentNaming
static class GL
{
    public enum PlatformInfo
    {
        Renderer = 0x1F01,
    }

    [DllImport("OpenGL32", EntryPoint = "glGetString", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi), SuppressUnmanagedCodeSecurity]
    static extern unsafe byte* GetString(int name);

    public static string GetString(PlatformInfo pi)
    {
        IntPtr ptr;
        unsafe
        {
            byte* str = GetString((int)pi);
            ptr = new IntPtr(str);
        }
        return Marshal.PtrToStringAnsi(ptr);
    }
}