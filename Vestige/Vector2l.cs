﻿using SFML.System;

namespace Vestige
{
    // ReSharper disable once InconsistentNaming
    public struct Vector2l
    {
        public long X;
        public long Y;
        public Vector2l(long x, long y)
        {
            X = x;
            Y = y;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var k = (Vector2l)obj;
            return k.X == X && k.Y == Y;

        }
        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }

        public Vector2i Vector2i()
        {
            return new Vector2i((int)X, (int)Y);
        }
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator - overload ; returns the opposite of a vector
        /// </summary>
        /// <param name="v">Vector to negate</param>
        /// <returns>-v</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2l operator -(Vector2l v)
        {
            return new Vector2l(-v.X, -v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator - overload ; subtracts two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 - v2</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2l operator -(Vector2l v1, Vector2l v2)
        {
            return new Vector2l(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static Vector2f operator -(Vector2f v1, Vector2l v2)
        {
            return new Vector2f(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static Vector2f operator -(Vector2l v1, Vector2f v2)
        {
            return new Vector2f(v1.X - v2.X, v1.Y - v2.Y);
        }
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator + overload ; add two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 + v2</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2l operator +(Vector2l v1, Vector2l v2)
        {
            return new Vector2l(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Vector2f operator +(Vector2f v1, Vector2l v2)
        {
            return new Vector2f(v1.X + v2.X, v1.Y + v2.Y);
        }
        public static Vector2f operator +(Vector2l v1, Vector2f v2)
        {
            return new Vector2f(v1.X + v2.X, v1.Y + v2.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator * overload ; multiply a vector by a scalar value
        /// </summary>
        /// <param name="v">Vector</param>
        /// <param name="x">Scalar value</param>
        /// <returns>v * x</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2l operator *(Vector2l v, long x)
        {
            return new Vector2l(v.X * x, v.Y * x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator * overload ; multiply a scalar value by a vector
        /// </summary>
        /// <param name="x">Scalar value</param>
        /// <param name="v">Vector</param>
        /// <returns>x * v</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2l operator *(long x, Vector2l v)
        {
            return new Vector2l(v.X * x, v.Y * x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator / overload ; divide a vector by a scalar value
        /// </summary>
        /// <param name="v">Vector</param>
        /// <param name="x">Scalar value</param>
        /// <returns>v / x</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2l operator /(Vector2l v, long x)
        {
            return new Vector2l(v.X / x, v.Y / x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator == overload ; check vector equality
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 == v2</returns>
        ////////////////////////////////////////////////////////////
        public static bool operator ==(Vector2l v1, Vector2l v2)
        {
            return v1.Equals(v2);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator != overload ; check vector inequality
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 != v2</returns>
        ////////////////////////////////////////////////////////////
        public static bool operator !=(Vector2l v1, Vector2l v2)
        {
            return !v1.Equals(v2);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Provide a string describing the object
        /// </summary>
        /// <returns>String description of the object</returns>
        ////////////////////////////////////////////////////////////
        public override string ToString()
        {
            return "[Vector2l]" +
                   " X(" + X + ")" +
                   " Y(" + Y + ")";
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2f(Vector2l v)
        {
            return new Vector2f(v.X, v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2l(Vector2f v)
        {
            return new Vector2l((long)v.X, (long)v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2l(Vector2i v)
        {
            return new Vector2l((long)v.X, (long)v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2l(Vector2d v)
        {
            return new Vector2l((long)v.X, (long)v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2i(Vector2l v)
        {
            return new Vector2i((int)v.X, (int)v.Y);
        }
    }
}
