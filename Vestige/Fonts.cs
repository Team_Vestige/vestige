﻿using SFML.Graphics;

namespace Vestige
{
    static class Fonts
    {
        public static readonly Font Basic = new Font("resources/sansation.ttf");
        public static readonly Font Vestige = new Font("resources/CLARKE_black.otf");
        public static readonly Font VestigeOblic = new Font("resources/CLARKE_black-oblique.otf");
    }
}
       