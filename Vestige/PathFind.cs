﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using SFML.Graphics;

namespace Vestige
{
    static class PathFind
    {
        class PathPoint
        {
            public readonly float Cost;
            public readonly Vector2d Coo;
            public PathPoint(SimplePoint point, float cost)
            {
                Cost = cost;
                Coo = point.Coo;
            }
        }
        public struct SimplePoint : IEquatable<SimplePoint>
        {
            public Vector2d Coo;
            public readonly float Distance;
            public SimplePoint(Vector2d coo, Vector2d end)
            {
                Coo = coo;
                Distance = (float)Common.DistancePow2(coo, end);
            }
            public bool Equals(SimplePoint otherPoint) => Math.Abs(otherPoint.Coo.X - Coo.X) < float.Epsilon && Math.Abs(otherPoint.Coo.Y - Coo.Y) < float.Epsilon;
        }
        static void AddProximityPoints(Vector2d start, Vector2d arrival, ICollection<SimplePoint> toProcess, Map.TileDataManager<PathPoint> dataMap)
        {
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if (Math.Abs(x) + Math.Abs(y) != 1) continue;
                    var candidate = new SimplePoint(start + new Vector2d(x, y), arrival);
                    if (!toProcess.Contains(candidate) && dataMap[(Vector2l)(start + new Vector2d(x, y))] == null)
                    {
                        toProcess.Add(candidate);
                    }
                }
            }
        }

        static float SmallestCostNearPoint(Vector2l coo, Map.TileDataManager<PathPoint> dataMap)
        {
            float mini = float.MaxValue;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (dataMap[coo + new Vector2l(i, j)] == null || Math.Abs(i) + Math.Abs(j) != 1) continue;
                    var temp = dataMap[coo + new Vector2l(i, j)].Cost + 1;
                    if (temp < mini)
                    {
                        mini = temp;
                    }
                }
            }

            return mini;
        }

        static void ProcessMinimalPoint(ICollection<SimplePoint> toProcess, Map.TileDataManager<PathPoint> dataMap, int unitHitboxId, Map map, Vector2d end)
        {
            SimplePoint mini = toProcess.First();
            foreach (SimplePoint point in toProcess)
            {
                if (mini.Distance > point.Distance)
                {
                    mini = point;
                }
            }
            PreChunk chunkCandidate = map.PreChunkOfPoint(mini.Coo);
            Vector2f relativCoo = chunkCandidate.RelativeCoordinate(mini.Coo);
            if (chunkCandidate is Chunk chunk && //Is a chunk
                chunk.UnitHoldingSize((Vector2i)relativCoo) >= MovableUnit.Hitboxes[unitHitboxId])
            {
                dataMap[(Vector2l)mini.Coo] = new PathPoint(mini, SmallestCostNearPoint((Vector2l)mini.Coo, dataMap));
                AddProximityPoints(mini.Coo, end, toProcess, dataMap);
            }
            toProcess.Remove(mini);
        }

        static PathPoint FindPreviousPoint(PathPoint point, Map.TileDataManager<PathPoint> dataMap)
        {
            PathPoint output = null;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (Math.Abs(i) + Math.Abs(j) != 1) continue;
                    var testPoint = dataMap[(Vector2l)point.Coo + new Vector2l(i, j)];
                    if (testPoint == null || !(testPoint.Cost < point.Cost)) continue;
                    if (output == null)
                    {
                        output = testPoint;
                    }
                    else if (output.Cost > testPoint.Cost)
                    {
                        output = testPoint;
                    }
                }
            }
            if (output == null)
            {
                throw new Exception("Didnt found way back");
            }
            return output;
        }

        public static List<Vector2d> PathFinding(Vector2d start, Vector2d end, int hitBoxId, Map map)
        {
            //Check if the pathfind is possible
            if (map.ChunkOfPoint(start) == null || map.ChunkOfPoint(end) == null)
            {
                Console.WriteLine("Path not in a chunk");
                return new List<Vector2d>();
            }
            //Check if the pathfind is in the same area
            Chunk startingChunk = map.ChunkOfPoint(start);
            Chunk endingChunk = map.ChunkOfPoint(end);
            Vector2i startingTile = startingChunk.RelativeCoordinate((Vector2l)start);
            Zone startingZone = startingChunk.Zones[hitBoxId][startingTile.X, startingTile.Y];
            Vector2i endingTile = endingChunk.RelativeCoordinate((Vector2l)end);
            Zone endingZone = endingChunk.Zones[hitBoxId][endingTile.X, endingTile.Y];
            if (endingZone != startingZone)
            {
                Console.WriteLine("Path not in the same zone");
                return new List<Vector2d>();
            }
            //Starting the pathfinding
            var dataMap = new Map.TileDataManager<PathPoint>
            {
                [(long) Math.Floor(start.X), (long) Math.Floor(start.Y)] = new PathPoint(new SimplePoint(start, end), 0)
            };
            var toProcess = new List<SimplePoint>();
            AddProximityPoints(start, end, toProcess, dataMap);

            while (toProcess.Count > 0 && dataMap[(Vector2l)end] == null)
            {
                ProcessMinimalPoint(toProcess, dataMap, hitBoxId, map, end);
            }
            if (toProcess.Count == 0)
            {
                return null;
            }
            PathPoint previousPoint = dataMap[(Vector2l)end];
            var output = new List<Vector2d>
            {
                previousPoint.Coo
            };
            while (previousPoint != dataMap[(Vector2l)start])
            {
                Debug.Instance.MapDebug[(Vector2l)previousPoint.Coo] = new RectangleShape()
                {
                    FillColor = Color.Red,
                    Size = new Vector2f(0.5f, 0.5f),
                    Position = (Vector2f)(Vector2l)previousPoint.Coo
                };
                output.Add(previousPoint.Coo);
                previousPoint = FindPreviousPoint(previousPoint, dataMap);
            }
            return output;
        }
    }
}
