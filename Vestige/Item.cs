﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige
{
    public enum Item
    {
        Weapon,
        Wood,
        Stone,
        Gold,
        Artifact,
        Equipement,
        Ammo,
        Wall,
        StoneExtractor,
        Turret,
        CraftingTable
    }
}
