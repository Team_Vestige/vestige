﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige
{
    class Recipe
    {
        public Dictionary<Item, int> NeededItems { get; set; }

        public Dictionary<Item, int> ResultItems { get; set; }

        public Recipe(Dictionary<Item,int> neededItems, Dictionary<Item, int> resultItems)
        {
            NeededItems = neededItems;
            ResultItems = resultItems;
        }
    }
}
