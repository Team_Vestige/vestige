﻿using SFML.System;
using System;
using SFML.Graphics;

namespace Vestige
{
    /// <summary>
    /// Store 4 Vector2f
    /// </summary>
    public struct QuadCoords
    {
        public readonly Vector2f TopLeft;
        public readonly Vector2f TopRight;
        public readonly Vector2f BottomLeft;
        public readonly Vector2f BottomRight;
        public QuadCoords(Vector2f topLeft, Vector2f topRight, Vector2f bottomLeft, Vector2f bottomRight)
        {
            TopLeft = topLeft;
            TopRight = topRight;
            BottomLeft = bottomLeft;
            BottomRight = bottomRight;
        }

        public QuadCoords(Vector2f topLeft, Vector2f bottomRight)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
            BottomLeft = new Vector2f(topLeft.X, bottomRight.Y);
            TopRight = new Vector2f(bottomRight.X, topLeft.Y);
        }

        public IntRect IntRect()
        {
            return new IntRect((Vector2i) TopLeft, (Vector2i) (BottomRight - TopLeft));
        }
    }
}
