﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Globalization;

namespace Vestige
{


    class GameDebug : Drawable, IDisposable
    {
        readonly Text _fpsInstant;
        readonly Text _fpsFirstQuartile;
        readonly Text _fpsMedianQuartile;
        readonly Text _fpsThirdQuartile;
        readonly Text _maxTimeFrame;
        readonly Text _cursorCoordinate;
        readonly Text _cursorChunkCoordinate;
        readonly Text _cursorNormalCoordinate;
        readonly Text _dayNumber;
        readonly Text _timeOfTheDay;
        Vector2d _cursorPos;
        readonly Clock _clock;
        readonly RectangleShape _currentTile;
        readonly RectangleShape _currentChunk;
        int _actualtime;
        readonly long[] _lastFpsStorage;
        int _cursors;
        readonly Game _game;
        Map.Map.TileDataManager<RectangleShape> _mapDebug;
        public static GameDebug Instance;
        public bool Enabled;
        public bool Advanced;
        public GameDebug(Game game)
        {
            Instance = this;
            _game = game;
            _dayNumber = new Text("0", Fonts.Basic)
            {
                Position = new Vector2f(700, 0)
            };
            _timeOfTheDay = new Text("0", Fonts.Basic)
            {
                Position = new Vector2f(900, 0)
            };

            _currentTile = new RectangleShape
            {
                Size = new Vector2f(1, 1),
                FillColor = new Color(200, 200, 255, 100)
            };
            _currentChunk = new RectangleShape
            {
                Size = new Vector2f(Map.ChunkData.TileSize, Map.ChunkData.TileSize),
                FillColor = new Color(200, 200, 200, 50)
            };
            _mapDebug = new Map.Map.TileDataManager<RectangleShape>();
            _clock = new Clock();
            _lastFpsStorage = new long[601];
            _cursors = 0;
            _fpsInstant = new Text("0", Fonts.Basic);
            _fpsFirstQuartile = new Text("0", Fonts.Basic)
            {
                Position = new Vector2f(120, 0)
            };
            _fpsMedianQuartile = new Text("0", Fonts.Basic)
            {
                Position = new Vector2f(250, 0)
            };
            _fpsThirdQuartile = new Text("0", Fonts.Basic)
            {
                Position = new Vector2f(430, 0)
            };
            _maxTimeFrame = new Text("0", Fonts.Basic)
            {
                Position = new Vector2f(550, 0)
            };
            _cursorCoordinate = new Text("", Fonts.Basic)
            {
                Position = new Vector2f()
            };
            _cursorNormalCoordinate = new Text("", Fonts.Basic)
            {
                Position = new Vector2f()
            };
            _cursorChunkCoordinate = new Text("", Fonts.Basic)
            {
                Position = new Vector2f()
            };
            game.Update += LogicProcess;
            game.GameEvents.KeyPressed += Window_KeyPressed;
        }

        void Window_KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.F1)
            {
                Enabled = !Enabled;
            }

            if (e.Code == Keyboard.Key.F2 && Enabled)
            {
                Advanced = !Advanced;
            }
        }

        public void DrawCamera()
        {
            if (!Enabled) return;
            _game.Window.Draw(_currentChunk);
            _game.Window.Draw(_currentTile);
            foreach (RectangleShape rect in _mapDebug)
            {
                _game.Window.Draw(rect);
            }
        }

        public Map.Map.TileDataManager<RectangleShape> MapDebug
        {
            get => _mapDebug;
            set => _mapDebug = value;
        }


        int[] MedianFps(long currentFrameTime)
        {
            if (_cursors == _lastFpsStorage.Length)
            {
                _cursors = 0;
            }
            _lastFpsStorage[_cursors] = currentFrameTime;

            _cursors++;
            var temp = new long[_lastFpsStorage.Length];
            _lastFpsStorage.CopyTo(temp, 0);
            Array.Sort(temp);
            return new[] { 1000000 / (int)(temp[150] + 1), 1000000 / (int)(temp[300] + 1), 1000000 / (int)(temp[450] + 1), 1000000 / (int)(temp[temp.Length - 1] + 1) };

        }


        void LogicProcess(object sender, Time e)
        {
            View view = ((Game)sender).CameraView;
            _actualtime = (int)_clock.Restart().AsMicroseconds(); //If last frame duration higher than recorded, record it !
            _cursorPos = view.Center - view.Size / 2 + Common.MultiplyVector(view.Size, Common.Divide(Mouse.GetPosition(_game.Window), (Vector2d)_game.Window.Size));
            _currentChunk.Position = (Vector2f)Map.Map.PositionOfChunk(_cursorPos) * Map.ChunkData.TileSize;
            _currentTile.Position = new Vector2f((int)Math.Floor(_cursorPos.X), (int)Math.Floor(_cursorPos.Y));
            _cursorNormalCoordinate.DisplayedString = VectorText(view.Center - view.Size / 2 + Common.MultiplyVector(view.Size, Common.Divide(Mouse.GetPosition(_game.Window), (Vector2f)_game.Window.Size)));
        }


        static string VectorText(Vector2f vec)
        {
            return "x: " + vec.X + "y: " + vec.Y;
        }

        static string VectorText(Vector2i vec)
        {
            return "x: " + vec.X + "y: " + vec.Y;
        }

        public static string NumberToText(float number)
        {
            string output = Math.Round(number, 2).ToString(CultureInfo.InvariantCulture);
            while (output.Length < 5)
            {
                output += " ";
            }
            return output + " ";
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            var quartiles = MedianFps(_actualtime);
            _cursorCoordinate.DisplayedString = VectorText(new Vector2i((int)Math.Floor(_cursorPos.X), (int)Math.Floor(_cursorPos.Y)));
            _cursorChunkCoordinate.DisplayedString = "PreChunk: " + VectorText(Map.Map.PositionOfChunk(_cursorPos));
            _fpsInstant.DisplayedString = "fps: " + 1000000 / _actualtime;
            _fpsFirstQuartile.DisplayedString = "1Q: " + quartiles[0];
            _fpsMedianQuartile.DisplayedString = "median: " + quartiles[1];
            _fpsThirdQuartile.DisplayedString = "3Q: " + quartiles[2];
            _maxTimeFrame.DisplayedString = "worst: " + quartiles[3];
            _timeOfTheDay.DisplayedString = "time: " + NumberToText(_game.DayTime.AsSeconds());
            _dayNumber.DisplayedString = "Day n°" + _game.NumberOfDay;
            _cursorCoordinate.Position = (Vector2f)Mouse.GetPosition(_game.Window) + new Vector2f(0, 0);
            _cursorNormalCoordinate.Position = (Vector2f)Mouse.GetPosition(_game.Window) + new Vector2f(0, 50);
            _cursorChunkCoordinate.Position = (Vector2f)Mouse.GetPosition(_game.Window) + new Vector2f(0, 25);
            _fpsInstant.Draw(target, states);
            _fpsFirstQuartile.Draw(target, states);
            _fpsMedianQuartile.Draw(target, states);
            _fpsThirdQuartile.Draw(target, states);
            _cursorCoordinate.Draw(target, states);
            _cursorNormalCoordinate.Draw(target, states);
            _cursorChunkCoordinate.Draw(target, states);
            _dayNumber.Draw(target, states);
            _timeOfTheDay.Draw(target, states);
            _maxTimeFrame.Draw(target, states);
        }

        public void Dispose()
        {
            _fpsInstant?.Dispose();
            _fpsFirstQuartile?.Dispose();
            _fpsMedianQuartile?.Dispose();
            _fpsThirdQuartile?.Dispose();
            _maxTimeFrame?.Dispose();
            _cursorCoordinate?.Dispose();
            _cursorChunkCoordinate?.Dispose();
            _cursorNormalCoordinate?.Dispose();
            _dayNumber?.Dispose();
            _timeOfTheDay?.Dispose();
            _clock?.Dispose();
            _currentTile?.Dispose();
            _currentChunk?.Dispose();
        }
    }
}
