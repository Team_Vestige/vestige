﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace Vestige
{
    class GameEvents
    {
        public RenderWindow _window;
        public EventHandler<MouseButtonEventArgs> MouseButtonPressed;
        public EventHandler<MouseButtonEventArgs> MouseButtonReleased;
        public EventHandler<KeyEventArgs> KeyPressed;
        public EventHandler<KeyEventArgs> KeyReleased;
        public EventHandler<MouseMoveEventArgs> MouseMoved;
        public GameEvents(RenderWindow window)
        {
            _window = window;
            window.MouseButtonPressed += Window_MouseButtonPressed;
            window.MouseButtonReleased += Window_MouseButtonReleased;
            window.KeyPressed += Window_KeyPressed;
            window.KeyReleased += Window_KeyReleased;
            window.MouseMoved += Window_MouseMoved;
        }

        void Window_MouseMoved(object sender, MouseMoveEventArgs e)
        {
            MouseMoved.Invoke(sender, e);
        }

        void Window_KeyReleased(object sender, KeyEventArgs e)
        {
            KeyReleased.Invoke(sender, e);
        }

        void Window_KeyPressed(object sender, KeyEventArgs e)
        {
            KeyPressed.Invoke(sender, e);
        }

        void Window_MouseButtonReleased(object sender, MouseButtonEventArgs e)
        {
            MouseButtonReleased.Invoke(sender, e);
        }

        void Window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            MouseButtonPressed.Invoke(sender, e);
        }

        public void Unsuscribe()
        {
            _window.MouseButtonPressed -= Window_MouseButtonPressed;
            _window.MouseButtonReleased -= Window_MouseButtonReleased;
            _window.KeyPressed -= Window_KeyPressed;
            _window.KeyReleased -= Window_KeyReleased;
            _window.MouseMoved -= Window_MouseMoved;
        }
    }
}
