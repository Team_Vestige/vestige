﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Vestige
{
    class MovableUnit : Unit
    {
        public static readonly float[] Hitboxes = { 1, 2f };
        readonly int _hitBoxId;
        readonly float _speed;
        List<Vector2d> _followingPath;
        public MovableUnit(Game game, byte id, Vector2f position, float speed, int hitBoxId)
            : base(game, id, position)
        {
            _hitBoxId = hitBoxId;
            _speed = speed;
            _followingPath = new List<Vector2d> { (Vector2d)position };
            game.Update += Game_Update;
        }
        void Game_Update(object sender, Time e)
        {
            Follow(_followingPath, e);
        }

        void ChangeChunk(Chunk newChunk)
        {
            Chunk.RemoveUnit(this);
            newChunk.AddUnit(this);
            Chunk = newChunk;
        }

        /// <summary>
        /// Move the unit to a direction indicated by point with a certain distance
        /// </summary>
        /// <param name="destination">Point to go</param>
        /// <param name="distanceToTravel">Distance to travel</param>
        /// <returns>Remaining distance to travel after the movement</returns>
        float SmoothMove(Vector2d destination, float distanceToTravel)
        {
            var distanceMaximumToTravel = Common.Distance(Position, destination);
            if(distanceMaximumToTravel.Equals(0))
            {
                return distanceToTravel;
            }

            var ratioToMove = distanceToTravel / distanceMaximumToTravel;
            if (ratioToMove > 1)
            {
                float movedDistance = Common.Distance(Position, destination);
                Move(destination);
                return distanceToTravel-movedDistance;
            }

            var displacementVector = (Vector2f)(destination - (Vector2d)Position);
            Vector2f positionToGo = Position + displacementVector * ratioToMove;
            Move((Vector2d)positionToGo);
            return 0;
        }

        void Follow(ICollection<Vector2d> checkPoints, Time time)
        {
            if (checkPoints.Count == 0 || _speed.Equals(0))
            {
                return;
            }
            Vector2d checkPoint = checkPoints.Last();
            float distanceToTravel = time.AsSeconds()*(_speed / 1) * 0.1f;
            distanceToTravel = SmoothMove(checkPoint, distanceToTravel);
            while (distanceToTravel > 0 && checkPoints.Count > 1)
            {
                checkPoints.Remove(checkPoint);
                checkPoint = checkPoints.Last();
                distanceToTravel = SmoothMove(checkPoint, distanceToTravel);
            }
        }

        public void GoTo(Vector2d absoluteCoordinate)
        {
            _followingPath = PathFind.PathFinding((Vector2d)Position, absoluteCoordinate, _hitBoxId, Game.Map);
        }
        void Move(Vector2d coo)
        {
            ChangeChunk(Game.Map.ChunkOfPoint(coo));
            Position = (Vector2f)coo;
        }

        public void SmoothMove2(Vector2d destination, Map map)
        {
            // Calcule la distance à parcourir en fonction de la vitesse de l'unité
            float distanceToTravel = (_speed / 1) * 0.1f;
            //Calcule le vecteur de direction
            Vector2f translation = new Vector2f((float)destination.X - Position.X, (float)destination.Y - Position.Y);
            float directionvector = (float)Math.Sqrt(translation.X * translation.X + translation.Y * translation.Y);
            Vector2f direction = new Vector2f(translation.X / directionvector, translation.Y / directionvector);
            if (float.IsNaN(direction.X))
            {
                direction.X = 0;
            }
            if (float.IsNaN(direction.Y))
            {
                direction.Y = 0;
            }
            // Mouvement de l'unité
            Vector2f moveposition = new Vector2f(direction.X, direction.Y) * distanceToTravel;
            Move(new Vector2f(Position.X + moveposition.X, Position.Y + moveposition.Y));
        }

        public void PointMove(RubberBand selection)
        {
            Vector2f destination = (Vector2f)Mouse.GetPosition(_window);
            Vector2f cursordestination = _map.WindowPointOnMap(destination, _camera, _window);
            List<Unit> _curunits = selection.CurrentUnits;

            foreach (Unit unit in _curunits)
            {
                if (unit is MovableUnit)
                {
                    List<Vector2d> posctx = new List<Vector2d>();
                    posctx = PathFind.PathFinding((Vector2d)unit.Position, (Vector2d)cursordestination, 0, _map);
                    if (posctx != null)
                    {
                        ((MovableUnit)unit).Follow(_camera, _map, posctx);
                    }
                }
            }
        }
    }
}

