﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige
{
    class RecipesList
    {
       public List<Recipe> Recipes { get; set; }

        public enum RecipeType
        {
            CraftWall,
            CraftTurret,
            CraftAmmo,
            CraftCraftTable,
            CraftOreProducer
        }
        public RecipesList()
        {
            Recipes = new List<Recipe>
            {
                new Recipe(
                    new Dictionary<Item, int> {{Item.Stone, 4}},
                    new Dictionary<Item, int> {{Item.Wall, 1}}),
                new Recipe(
                    new Dictionary<Item, int>{{ Item.Stone, 10 }},
                    new Dictionary<Item, int>{{Item.Turret, 1}}),
                new Recipe(
                    new Dictionary<Item, int>{{Item.Stone, 1}},
                    new Dictionary<Item, int>{{Item.Ammo, 2}}),
                new Recipe(
                    new Dictionary<Item, int>{{Item.Stone, 4}},
                    new Dictionary<Item, int>{{Item.CraftingTable,1}}),
                new Recipe(
                    new Dictionary<Item, int>{{Item.Stone, 4}},
                    new Dictionary<Item, int>{{Item.StoneExtractor, 1}})
            };
        }
    }
}
