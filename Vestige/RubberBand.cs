﻿using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace Vestige
{
    class RubberBand : Drawable
    {
        Vector2f _firstpos;
        Vector2f _secondpos;
        readonly RectangleShape _rec;
        bool _isclicked;
        readonly Game _game;
        public RubberBand(Game game)
        {
            _game = game;
            _game.Window.MouseButtonPressed += Window_MouseButtonPressed;
            _game.Window.MouseButtonReleased += Window_MouseButtonReleased;
            _game.Window.MouseMoved += UpdateRubberBand;

            _rec = new RectangleShape
            {
                FillColor = new Color(150, 0, 0, 120),
                OutlineColor = new Color(200, 0, 0, 200),
                OutlineThickness = 3
            };
        }

        void UpdateRubberBand(object sender, MouseMoveEventArgs e)
        {
            if (!_isclicked) return;
            _secondpos = (Vector2f)Mouse.GetPosition(_game.Window);
            var firstReversed = _firstpos;
            var seconReversed = _secondpos;
            if (_firstpos.X > _secondpos.X)
            {
                firstReversed.X = _secondpos.X;
                seconReversed.X = _firstpos.X;
            }
            if (_firstpos.Y > _secondpos.Y)
            {
                firstReversed.Y = _secondpos.Y;
                seconReversed.Y = _firstpos.Y;
            }
            SelectUnitInRectangle(firstReversed, seconReversed, _game);
        }
        /// <summary>
        /// Put the unit thate are in the rectangle in the selected unit of UnitController
        /// </summary>
        /// <param name="firstPos"></param>
        /// <param name="secondPos"></param>
        /// <param name="game"></param>
        void SelectUnitInRectangle(Vector2f firstPos, Vector2f secondPos, Game game)
        {
            Vector2d mapSecondPos = Map.WindowPositionToMapPositon(secondPos, game.CameraView, game.Window);
            Vector2d mapFirstPos = Map.WindowPositionToMapPositon(firstPos, game.CameraView, game.Window);
            var chunks = ChunkSelection(mapFirstPos, mapSecondPos, game);
            game.UnitController.SelectedUnit.Clear();
            foreach(Chunk chunk in chunks)
            {
                foreach(Unit unit in chunk.Units)
                {
                    if (unit.Position.X >= mapFirstPos.X && unit.Position.X < mapSecondPos.X && unit.Position.Y >= mapFirstPos.Y && unit.Position.Y < mapSecondPos.Y)
                    {
                        game.UnitController.SelectedUnit.Add(unit);
                    }
                }
            }
        }

        static IEnumerable<Chunk> ChunkSelection(Vector2d firstchunk, Vector2d lastchunk, Game game)
        {
            var selectedchunks = new List<Chunk>();
            Vector2i first = Map.PositionOfChunk(firstchunk);
            Vector2i last = Map.PositionOfChunk(lastchunk);
            for (int i = first.X; i <= last.X; i++)
            {
                for (int y = first.Y; y <= last.Y; y++)
                {
                    var candidate = new Vector2i(i, y);

                    if (game.Map.ChunkExist(candidate) && game.Map[candidate] is Chunk)
                    {
                        selectedchunks.Add((Chunk)game.Map[candidate]);
                    }
                }
            }
            return selectedchunks;
        }

        void Window_MouseButtonReleased(object sender, MouseButtonEventArgs e)
        {
            if(e.Button == Mouse.Button.Left)
            {
                _isclicked = false;
            }
            
        }

        void Window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            if (e.Button != Mouse.Button.Left) return;
            _isclicked = true;
            _firstpos = (Vector2f)Mouse.GetPosition(_game.Window);
            _secondpos = _firstpos;
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            if (!_isclicked) return;
            _rec.Position = _firstpos;
            _rec.Size = _secondpos - _firstpos;
            _rec.Draw(target, states);
        }
    }
}
