﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using Vestige.Map;

namespace Vestige
{
    class CustomBinaryReader : BinaryReader
    {
        public CustomBinaryReader(Stream input) : base(input)
        {
        }

        public CustomBinaryReader(Stream input, Encoding encoding) : base(input, encoding)
        {
        }

        public CustomBinaryReader(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding, leaveOpen)
        {
        }

        // ReSharper disable once InconsistentNaming
        public Vector2i ReadVector2i()
        {
            return new Vector2i(ReadInt32(), ReadInt32());
        }
    }
}
