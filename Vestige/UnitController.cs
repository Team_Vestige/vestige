﻿using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace Vestige
{
    class UnitController : Drawable
    {
        readonly Game _game;
        public UnitController(Game game)
        {
            _game = game;
            SelectedUnit = new List<Unit>();
            game.Window.MouseButtonPressed += Window_MouseButtonPressed;
        }

        void Window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            if (e.Button != Mouse.Button.Right)
            {
                return;
            }
            
            foreach (var unit in SelectedUnit)
            {
                if(unit is MovableUnit movableUnit)
                {
                    movableUnit.GoTo(Map.WindowPositionToMapPositon((Vector2f)Mouse.GetPosition(_game.Window), _game.CameraView, _game.Window));
                }
            }

        }

        public List<Unit> SelectedUnit { get; }

        public void Draw(RenderTarget target, RenderStates states)
        {
            foreach (Unit unit in SelectedUnit)
            {
                unit.SelectionShape.Draw(target, states);
            }
        }
    }
}
