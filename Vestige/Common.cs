﻿using System;
using SFML.System;

namespace Vestige
{
    static class Common
    {
        /// <summary>
        /// Return the multiplication of vector v1 and v2
        /// </summary>
        /// <returns>multiplication of vector v1 and v2</returns>
        public static Vector2f MultiplyVector(Vector2f v1, Vector2f v2)
        {
            return new Vector2f(v1.X * v2.X, v1.Y * v2.Y);
        }
        public static Vector2d MultiplyVector(Vector2f v1, Vector2d v2)
        {
            return new Vector2d(v1.X * v2.X, v1.Y * v2.Y);
        }
        public static Vector2f MultiplyVector(Vector2i v1, Vector2f v2)
        {
            return new Vector2f(v1.X * v2.X, v1.Y * v2.Y);
        }
        public static Vector2f MultiplyVector(Vector2f v1, Vector2i v2)
        {
            return new Vector2f(v1.X * v2.X, v1.Y * v2.Y);
        }
        public static Vector2i MultiplyVector(Vector2i v1, Vector2i v2)
        {
            return new Vector2i(v1.X*v2.X, v1.Y*v2.Y);
        }
        public static Vector2f MultiplyVector(Vector2i v1, float v2)
        {
            return new Vector2f(v1.X * v2, v1.Y * v2);
        }

        public static Vector2l FloorVector2l(Vector2d coo)
        {
            return new Vector2l((long)Math.Floor(coo.X), (long)Math.Floor(coo.Y));
        }


        /// <summary>
        /// Return the remainder after division of a vector by another vector or a number. 
        /// </summary>
        /// <param name="v1">Vector to be divided</param>
        /// <param name="v2">Divider</param>
        /// <returns></returns>
        public static Vector2f ModuloVector(Vector2f v1, Vector2f v2)
        {
            return new Vector2f(v1.X % v2.X, v1.Y % v2.Y);
        }
        public static Vector2f ModuloVector(Vector2i v1, Vector2f v2)
        {
            return new Vector2f(v1.X%v2.X, v1.Y%v2.Y);
        }
        public static Vector2f ModuloVector(Vector2f v1, Vector2i v2)
        {
            return new Vector2f(v1.X % v2.X, v1.Y % v2.Y);
        }
        public static Vector2i ModuloVector(Vector2i v1, Vector2i v2)
        {
            return new Vector2i(v1.X % v2.X, v1.Y % v2.Y);
        }
        public static Vector2i ModuloVector(Vector2i v1, int v2)
        {
            return new Vector2i(v1.X % v2, v1.Y % v2);
        }
        public static Vector2f ModuloVector(Vector2i v1, float v2)
        {
            return new Vector2f(v1.X % v2, v1.Y % v2);
        }
        public static Vector2f ModuloVector(Vector2f v1, int v2)
        {
            return new Vector2f(v1.X % v2, v1.Y % v2);
        }
        public static Vector2f ModuloVector(Vector2f v1, float v2)
        {
            return new Vector2f(v1.X % v2, v1.Y % v2);
        }

        public static Vector2i VectorCeiling(Vector2f input)
        {
            return new Vector2i((int)Math.Ceiling(input.X),(int) Math.Ceiling(input.Y));
        }

        public static Vector2f VectorAddition(Vector2f vector, float addition)
        {
            return new Vector2f(vector.X + addition, vector.Y + addition);
        }
        public static Vector2i VectorAddition(Vector2i vector, int addition)
        {
            return new Vector2i(vector.X + addition, vector.Y + addition);
        }

        /// <summary>
        /// Return the distance between two point power 2
        /// </summary>
        /// <param name="a">Point A</param>
        /// <param name="b">Point B</param>
        /// <returns>Distance between two point power 2</returns>
        public static float DistancePow2(Vector2f a, Vector2f b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static double DistancePow2(Vector2d a, Vector2f b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static double DistancePow2(Vector2f a, Vector2d b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static double DistancePow2(Vector2d a, Vector2d b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static float DistancePow2(Vector2i a, Vector2f b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static float DistancePow2(Vector2f a, Vector2i b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static int DistancePow2(Vector2i a, Vector2i b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static long DistancePow2(Vector2l a, Vector2l b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }
        public static float DistancePow2(long x1, long y1, long x2, long y2)
        {
            return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        }
        /// <summary>
        /// a/b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2f Divide(Vector2f a, Vector2f b)
        {
            return new Vector2f(a.X/b.X, a.Y/b.Y);
        }
        public static Vector2d Divide(Vector2f a, Vector2u b)
        {
            return new Vector2d(a.X / b.X, a.Y / b.Y);
        }
        public static Vector2d Divide(Vector2i a, Vector2d b)
        {
            return new Vector2d(a.X / b.X, a.Y / b.Y);
        }
        public static Vector2f Divide(Vector2i a, Vector2f b) => Divide((Vector2f)a,b);
        public static Vector2f Divide(Vector2f a, Vector2i b) => Divide(a, (Vector2f)b);
        public static Vector2f Divide(Vector2i a, Vector2i b) => Divide((Vector2f)a, (Vector2f)b);
        /// <summary>
        /// Return the distance between two point
        /// Slower than DistancePow2 because using Math.Sqrt
        /// </summary>
        /// <param name="a">Point A</param>
        /// <param name="b">Point B</param>
        /// <returns>Distance between two point</returns>
        public static float Distance(Vector2f a, Vector2f b)
        {
            return (float)Math.Sqrt((DistancePow2(a,b)));
        }
        public static float Distance(Vector2d a, Vector2f b)
        {
            return (float)Math.Sqrt((DistancePow2(a, b)));
        }
        public static float Distance(Vector2f a, Vector2d b)
        {
            return (float)Math.Sqrt((DistancePow2(a, b)));
        }
        public static float Distance(Vector2d a, Vector2d b)
        {
            return (float)Math.Sqrt((DistancePow2(a, b)));
        }

        public static bool IsPointInCircle(Vector2f point, Vector2f circlePoint, float radius)
        {
            if( radius*radius > DistancePow2(point, circlePoint)) {
                return true;
            }
            return false;
        }
        public static float Slope(Vector2f a, Vector2f b)
        {
            return (a.Y - b.Y) / (a.X - b.X);
        }
        public static double Slope(Vector2d a, Vector2f b)
        {
            return (a.Y - b.Y) / (a.X - b.X);
        }
        public static double Slope(Vector2f a, Vector2d b)
        {
            return (a.Y - b.Y) / (a.X - b.X);
        }
        public static double Slope(Vector2d a, Vector2d b)
        {
            return (a.Y - b.Y) / (a.X - b.X);
        }
        public static double Slope(double ax, double ay, double bx, double by)
        {
            return (ay - @by) / (ax - bx);
        }

        /// <summary>
        /// Return true if the point is on axes aligned rectangle.
        /// </summary>
        /// <param name="a">top left rectangle</param>
        /// <param name="b">buttom right</param>
        /// <param name="c">mouse position</param>
        /// <returns>return true if "c" is in rectangle between "a" and "b"</returns>
        public static bool CursorInRect(Vector2f a, Vector2f b, Vector2f c)
        {
            if (b.X < a.X || b.Y < a.Y)
            {
                if (a.X > b.X)
                {
                    var d = b.X;
                    b.X = a.X;
                    a.X = d;
                }
                if (a.Y > b.Y)
                {
                    var d = b.Y;
                    b.Y = a.Y;
                    a.Y = d;
                }
            }
            return c.X > a.X && c.X < b.X && c.Y > a.Y && c.Y < b.Y;
        }

        public static string Vector2iToFileName(Vector2i coo)
        {
            return "[Chunk] X(" + coo.X + ") Y(" + coo.Y + ")";
        }

        /*
        public static float TriangleArea(Vector2f a, Vector2f b, Vector2f c)
        {
            float A = Distance(a,b);
        }
        public static bool IsPointInRectangle(Vector2f point, Vector2f a, Vector2f b, Vector2f c, Vector2f d) 
        {
            Rectangle rec = new Rectangle(;
        }*/
    }
}
