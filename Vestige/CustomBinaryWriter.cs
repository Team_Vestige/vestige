﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace Vestige
{
    class CustomBinaryWriter : BinaryWriter
    {
        public void Write(Vector2i value)
        {
            Write(value.X);
            Write(value.Y);
        }

        public void Write(Map.Zone value)
        {
           Write(value.Id);
        }
    }
}
