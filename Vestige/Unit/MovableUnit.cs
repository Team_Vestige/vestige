﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SFML.Graphics;
using SFML.System;
using System.IO;
using OpenTK.Graphics.OpenGL;
using PrimitiveType = SFML.Graphics.PrimitiveType;

namespace Vestige.Unit
{
    class MovableUnit : AdvancedUnit
    {
        public static readonly float[] Hitboxes = { 0.5f, 2f };
        public readonly int HitBoxId;
        public float _speed;
        List<Vector2d> _followingPath;
        List<Vector2d> _oldFollowingPath;//using for debug
        public MovableUnit(Game game, UnitType id, Vector2f position, float maxHealth, Team owner, float speed, int hitBoxId, float itemRange, float viewRange)
            : base(game, id, position, maxHealth, owner, itemRange, viewRange)
        {
            HitBoxId = hitBoxId;
            _speed = speed;
            _followingPath = new List<Vector2d> { (Vector2d)position };
            _oldFollowingPath = new List<Vector2d> { (Vector2d)position };
            game.Update += Game_Update;
        }

        public MovableUnit(BinaryReader r, Game game, Map.Chunk chunk) : base(r, game, chunk)
        {
            _followingPath = new List<Vector2d>();
            HitBoxId = r.ReadInt32(); //1 int
            _speed = r.ReadSingle(); //2 float
            Console.WriteLine(_speed);
            int length = r.ReadInt32(); //3 int
            for (int i = 0; i < length; i++)
            {
                _followingPath.Add(new Vector2d(r.ReadDouble(), r.ReadDouble()));
            }
            _oldFollowingPath = new List<Vector2d> { (Vector2d)Position };

        }

        public override void WriteTo(BinaryWriter w)
        {
            w.Write(0);
            State = State.Waiting;
            w.Write((int)Type.MovableUnit);
            WriteContent(w);
            w.Write(HitBoxId);                             // 1 int
            w.Write(_speed);                                // 2 float
            w.Write(_followingPath.Count);                  // 3 int
            for (int i = 0; i < _followingPath.Count; i++)
            {
                w.Write(_followingPath[i].X);               // 4 double
                w.Write(_followingPath[i].Y);               // 5 double 
            }
        }

        void Game_Update(object sender, Time e)
        {
            if (Health > 0)
            {
                if (State.
                    CanMove())
                {
                    Follow(e);
                }
                AvoidCollision();
            }
        }

        protected override void Die()
        {
            base.Die();
            Game.Update -= Game_Update;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (Game.Map[i, j].ZoneData == null) continue;
                    foreach (var unit in Game.Map[i, j].ChunkData.Units)
                    {
                        if (unit is MovableUnit mUnit
                            && unit.Team == Team.Alien
                            && Common.Distance(Position, mUnit.Position) < 15
                            )
                        {
                            mUnit.GoTo((Vector2d)Position);
                        }
                    }
                }
            }
        }
        void ChangeChunk(Map.Chunk newChunk)
        {
            Debug.Assert(Health > 0);
            if (newChunk != Chunk)
            {
                Chunk.ChunkData.RemoveUnit(this);
                newChunk.ChunkData.AddUnit(this);
                Chunk = newChunk;

                for (int i = -1; i < 2; i++)
                {
                    for (int j = -1; j < 2; j++)
                    {
                        var coo = Chunk.Position + new Vector2i(i, j);
                        if (Game.Map[coo].ZoneData == null)
                        {
                            Game.Map[coo].GenerateAroundAndActivateZone(Game);
                        }
                    }
                }
                for (int i = -1; i < 2; i++)
                {
                    for (int j = -1; j < 2; j++)
                    {
                        var coo = Chunk.Position + new Vector2i(i, j);
                        Game.Map[coo].ZoneData.UpdateZones();
                        
                    }
                }
            }

        }

        void AvoidCollision()
        {
            var collidingUnit = new List<Unit>();
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    Vector2i pos = new Vector2i(x, y);
                    if (Game.Map[pos + Chunk.Position].ChunkData != null)
                    {
                        collidingUnit.AddRange(Game.Map[pos + Chunk.Position].ChunkData.Units.Where(unit => unit != this && unit is MovableUnit mUnit && Common.Distance(Position, unit.Position) < Hitboxes[mUnit.HitBoxId] + Hitboxes[HitBoxId]));
                    }
                }
            }
            // ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
            foreach (MovableUnit mUnit in collidingUnit)
            {
                float distanceAb = Common.Distance(mUnit.Position, Position);
                float minDistanceAb = Hitboxes[mUnit.HitBoxId] + Hitboxes[HitBoxId];
                float distanceToMove = minDistanceAb - distanceAb;

                float ratioToMove = (distanceToMove / distanceAb) / 2;
                if (distanceAb == 0)
                {
                    ratioToMove = 1;
                }
                Vector2f vectorAb = mUnit.Position - Position;
                if (mUnit.State.CanMove())
                {
                    mUnit.Move((Vector2d)mUnit.Position + vectorAb * ratioToMove);
                }
                if (mUnit._followingPath.Count > 0)
                {
                    mUnit.GoTo(mUnit._followingPath.First());
                }
            }
        }
        /// <summary>
        /// Move the unit to a direction indicated by point with a certain distance
        /// </summary>
        /// <param name="destination">Point to go</param>
        /// <param name="distanceToTravel">Distance to travel</param>
        /// <returns>Remaining distance to travel after the movement</returns>
        float SmoothMove(Vector2d destination, float distanceToTravel)
        {
            Debug.Assert(Health > 0);
            var distanceMaximumToTravel = Common.Distance(Position, destination);
            if (distanceMaximumToTravel.Equals(0))
            {
                return distanceToTravel;
            }

            var ratioToMove = distanceToTravel / distanceMaximumToTravel;
            if (ratioToMove > 1)
            {
                float movedDistance = Common.Distance(Position, destination);
                Move(destination);
                return distanceToTravel - movedDistance;
            }

            var displacementVector = (destination - (Vector2d)Position);
            Vector2f positionToGo = Position + (Vector2f)displacementVector * ratioToMove;
            Move((Vector2d)positionToGo);
            return 0;
        }

        void Follow(Time time)
        {
            Debug.Assert(Health > 0);
            float distanceToTravel = time.AsSeconds() * (_speed / 1) * 0.1f;
            if (_followingPath == null || _followingPath.Count == 0 || _speed.Equals(0))
            {
                State = State.Waiting;
                return;
            }



            Vector2d checkPoint = _followingPath.Last();
            distanceToTravel = SmoothMove(checkPoint, distanceToTravel);

            while (distanceToTravel > 0 && _followingPath.Count > 1)
            {
                _followingPath.Remove(checkPoint);
                checkPoint = _followingPath.Last();
                distanceToTravel = SmoothMove(checkPoint, distanceToTravel);
            }

            if (_followingPath.Count == 1 && Common.Distance(Position, _followingPath.Last()) < 1)
            {
                _followingPath.Remove(checkPoint);
                StopUnitAround();
            }
            if (distanceToTravel > 0f)
            {
                _followingPath.Remove(checkPoint);
                StopUnitAround();
            }     
       
        }

        public void GoTo(Vector2d absoluteCoordinate)
        {
            try
            {
                _followingPath = Map.PathFind.PathFinding((Vector2d) Position, absoluteCoordinate, HitBoxId, Game.Map);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            _oldFollowingPath = new List<Vector2d>(_followingPath);
        }

        public void AttackMove(Vector2d absoluteCoordinate)
        {
            State = State.MovingAttacking;
            if (Game.Map.GetZoneOfPoint(absoluteCoordinate, HitBoxId) !=
                Game.Map.GetZoneOfPoint((Vector2d) Position, HitBoxId))
            {
                bool found = false;
                for (int i = -1; i < 2; i++)
                {
                    for (int j = -1; j < 2; j++)
                    {
                        var coo = new Vector2d(i, j);
                        if (Game.Map.GetZoneOfPoint(absoluteCoordinate + coo, HitBoxId) ==
                            Game.Map.GetZoneOfPoint((Vector2d) Position , HitBoxId))
                        {
                            absoluteCoordinate += coo;
                            found = true;
                            break;
                        }
                    }
                    if (found) break;
                }
                if (!found) return;
            }
            try
            {
                _followingPath = Map.PathFind.PathFinding((Vector2d) Position, absoluteCoordinate, HitBoxId, Game.Map);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            _oldFollowingPath = new List<Vector2d>(_followingPath);
        }
        void Move(Vector2d coo)
        {
            Debug.Assert(Health > 0);
            Map.Chunk chunk = Game.Map.GetChunkOfPoint(coo);
            Vector2f nextTileFloat = chunk.GetRelativeCoordinate(coo);
            Vector2i nextTile = new Vector2i((int)Math.Floor(nextTileFloat.X), (int)Math.Floor(nextTileFloat.Y));
            Vector2f currentTileFloat = Chunk.GetRelativeCoordinate(Position);
            Vector2i currentTile = new Vector2i((int)Math.Floor(currentTileFloat.X), (int)Math.Floor(currentTileFloat.Y));
            if (chunk.ZoneData == null) return;
            if (chunk.ZoneData[nextTile.X, nextTile.Y, HitBoxId] !=
                Chunk.ZoneData[currentTile.X, currentTile.Y, HitBoxId])
            {
                if (_followingPath.Count > 0)
                {
                    GoTo(_followingPath.First());
                }
                return;
            }
            ChangeChunk(Game.Map.GetChunkOfPoint(coo));
            Position = (Vector2f)coo;
        }
        public override void Draw(RenderTarget target, RenderStates states)
        {
            Debug.Assert(Health > 0);
            if (GameDebug.Instance.Advanced && GameDebug.Instance.Enabled)
            {
                Vector2d last = new Vector2d();
                foreach (var point in _oldFollowingPath)
                {
                    if (point == _oldFollowingPath.First())
                    {
                        last = point;
                        continue;
                    }

                    foreach (Vector2l dePoint in Map.PathFind.GetPointsOnLines((long)Math.Floor(last.X), (long)Math.Floor(last.Y), (int)Math.Floor(point.X), (int)Math.Floor(point.Y)))
                    {
                        target.Draw(new RectangleShape()
                        {
                            FillColor = Color.Blue,
                            Size = new Vector2f(0.5f, 0.5f),
                            Position = new Vector2f(dePoint.X, dePoint.Y)
                        });

                    }


                    target.Draw(new VertexArray(PrimitiveType.Lines, 2)
                    {
                        [0] = new Vertex((Vector2f)last, Color.Green),
                        [1] = new Vertex(new Vector2f((int)Math.Floor(point.X), (int)Math.Floor(point.Y)),
                                Color.Cyan)
                    });
                    target.Draw(new RectangleShape()
                    {
                        FillColor = Color.Green,
                        Size = new Vector2f(1f, 1f),
                        Position = (Vector2f)new Vector2i((int)Math.Floor(point.X), (int)Math.Floor(point.Y))
                    });
                    last = point;
                }
                foreach (Vector2d point in _oldFollowingPath)
                {
                    target.Draw(new RectangleShape()
                    {
                        FillColor = Color.Red,
                        Size = new Vector2f(0.5f, 0.5f),
                        Position = (Vector2f)point
                    });
                }
            }
            target.Draw(UnitShape);
            target.Draw(LostHealthShape);
            target.Draw(HealthBarShape);
        }
        public List<Unit> StopUnitAround()
        {
            List<Unit> unitsList = new List<Unit>();
            // quand une unite arrive a followingpath areter tout les units qui on le même Followingpath
            for (var x=-1; x<2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    var chunk = Game.Map[Chunk.Position + new Vector2i(x, y)];
                    if (chunk?.ChunkData == null) continue;
                    foreach (var unit in chunk.ChunkData.Units)
                    {
                        if (unit is MovableUnit mUnit
                            && mUnit._followingPath.Count > 0)
                        {
                            if (Common.Distance((Vector2f) mUnit._followingPath.Last(),Position) <= 1.2)
                            {
                                if (Common.Distance(unit.Position, Position) <= 2)
                                {
                                    unit.State = State.Waiting;
                                    mUnit._followingPath = new List<Vector2d>();
                                }
                            }
                        }
                    }
                }      
            }    
            return unitsList;
        }
    }
}
