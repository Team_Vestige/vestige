﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige.Unit
{
    class AutoTurret : Building
    {
        public AutoTurret(Game game, Vector2l position)
            : base(game, UnitType.AutoTurret, position, new[,] {{true}}, Team.Human, 3, 3, TileType.AutoTurret, TileType.Path)
        {
            new AmmoWeapon(this, 5f, 1.2f, Time.FromSeconds(0.17f), 10f);
        }
    }
}
