﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace Vestige.Unit
{
    class ProducerBuilding: Building
    {
        readonly Dictionary<Item, int> _itemToAdd;
        readonly Time _timeToAdd;
        Time _time;

        public ProducerBuilding(Game game, UnitType id, Vector2l position, bool[,] hitbox, Team team, float itemRange, float viewRange, TileType tileType, Dictionary<Item, int> itemToAdd, Time timeToAdd, TileType deathTile) : base(game, id, position, hitbox, team, itemRange, viewRange, tileType, deathTile)
        {
            _itemToAdd = itemToAdd;
            _timeToAdd = timeToAdd;
            game.Update += GameUpdate;
            _time = new Time();
        }

        void GameUpdate(object sender, Time e)
        {
            _time += e;
            if (_time > _timeToAdd)
            {
                _time -= _timeToAdd;
                foreach (var items in _itemToAdd)
                {
                    AddInInventory(items.Key, items.Value);
                }
            }
            
        }
    }
}
