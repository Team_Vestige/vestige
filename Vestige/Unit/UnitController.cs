﻿using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using Vestige.Unit;

namespace Vestige
{
    class UnitController : Drawable
    {
        readonly Game _game;
        public UnitController(Game game)
        {
            _game = game;
            SelectedUnit = new List<Unit.Unit>();
            game.GameEvents.MouseButtonPressed += Window_MouseButtonPressed;
        }

        void Window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            if (e.Button != Mouse.Button.Right)
            {
                return;
            }
            foreach (var unit in SelectedUnit)
            {
                if(unit is MovableUnit movableUnit)
                {
                    movableUnit.State = State.Moving;
                    movableUnit.GoTo(Map.Map.WindowPositionToMapPositon((Vector2f)Mouse.GetPosition(_game.Window), _game.CameraView, _game.Window));
                }
            }
        }

        public List<Unit.Unit> SelectedUnit { get; }

        public void Draw(RenderTarget target, RenderStates states)
        {
            foreach (Unit.Unit unit in SelectedUnit)
            {
                if (unit is AdvancedUnit aUnit)
                {
                    aUnit.SelectionShape.Draw(target, states);
                }
            }
        }
    }
}
