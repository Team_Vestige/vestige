﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige.Unit
{
    class FakeRealWall : Building
    {
        public FakeRealWall(Game game,Vector2l position) : base(game, UnitType.Wall, position, new[,]{{true}}, Team.Alien, 0, 3, TileType.Wall, TileType.Path)
        {

        }
    }
}
