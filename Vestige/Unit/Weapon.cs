﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SFML.System;
using SFML.Audio;

namespace Vestige.Unit
{
    internal class Weapon
    {
        internal AdvancedUnit _unit;
        float _range;
        float _damage;
        internal float _aggroRange;
        Time _cooldown;
        Time _timeSinceLastAttack;
        //Sound _sound;

        public List<AdvancedUnit> UnitsInRange { get; } = new List<AdvancedUnit>();

        public Weapon(AdvancedUnit unit, float range, float damage, Time cooldown, float aggroRange)
        {
            _aggroRange = aggroRange;
            _unit = unit;
            _range = range;
            _damage = damage;
            _cooldown = cooldown;
            unit.Game.Update += Game_Update;
            _timeSinceLastAttack = Time.Zero;
        }

        void Game_Update(object sender, Time e)
        {
            if (_unit.Health <= 0)
            {
                _unit.Game.Update -= Game_Update;
                return;
            }
            UnitInRange();
            if (_timeSinceLastAttack <= _cooldown)
            {
                _timeSinceLastAttack += e;
            }
            if (_unit.State.CanAttack())
            {
                TryAttack();
            }

            if (_unit.State.CanAttack())
            {
                TryAggro();
            }
        }

        public void TryAggro()
        {
            var aggro = new List<AdvancedUnit>();
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    Vector2i pos = _unit.Chunk.Position + new Vector2i(x, y);
                    if (_unit.Game.Map[pos].ChunkData != null)
                    {
                        aggro.AddRange(_unit.Game.Map[pos].ChunkData.Units.OfType<AdvancedUnit>()
                            .Where(unit =>
                                _unit.CanAttack(unit)
                                && unit is AdvancedUnit advancedUnit
                                && Common.DistancePow2(_unit.Position, unit.Position) < _aggroRange * _aggroRange
                                ));
                    }
                }
            }

            if (aggro.Count == 0)
            {
                return;
            }
            Unit minUnit = aggro.First();
            float minDistance = Common.DistancePow2(minUnit.Position, _unit.Position);
            foreach (var unit in aggro)
            {
                float testDistance = Common.DistancePow2(unit.Position, _unit.Position);
                
                if (!(testDistance < minDistance)) continue;
                minUnit = unit;
                minDistance = testDistance;
            }

            if (_unit is MovableUnit mUnit)
            {
                mUnit.AttackMove((Vector2d)minUnit.Position);
            }
        }

        public virtual bool TryAttack()
        {
            if (_timeSinceLastAttack <= _cooldown || UnitsInRange.Count == 0) return false;
            _timeSinceLastAttack -= _cooldown;

            float minDistance = UnitsInRange.Min(unit => Common.DistancePow2(unit.Position, _unit.Position));
            AdvancedUnit minUnit =
                // ReSharper disable once CompareOfFloatsByEqualityOperator CEST MAL HEIN ? :D
                UnitsInRange.First(unit => minDistance == Common.DistancePow2(unit.Position, _unit.Position));
            Debug.Assert(minUnit != null);
            
            _unit.State = State.Attacking;
            minUnit.Damage(_damage);
            return true;
        }

        void UnitInRange()
        {
            UnitsInRange.Clear();
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    Vector2i pos = _unit.Chunk.Position + new Vector2i(x, y);
                    if (_unit.Game.Map[pos].ChunkData != null)
                    {
                        UnitsInRange.AddRange(_unit.Game.Map[pos].ChunkData.Units.OfType<AdvancedUnit>().Where(unit => Common.DistancePow2(_unit.Position, unit.Position) < _range * _range && unit != _unit && _unit.CanAttack(unit)));
                    }
                }
            }
        }
    }
}
