﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

namespace Vestige.Unit
{
    class AdvancedUnit : Unit
    {
        public CircleShape SelectionShape { get; }
        public float ViewRange;
        public List<Tuple<int, GUI.ActionButton.Action>> Actions;
        public AdvancedUnit(Game game, UnitType id, Vector2f position, float maxHealth, Team owner, float itemRange, float viewRange)
            : base(game, id, position, maxHealth, owner)
        {
            Actions = new List<Tuple<int, GUI.ActionButton.Action>>();
            ViewRange = viewRange;
            Inventory = new Dictionary<Item, int>();
            ItemMaxRange = itemRange;
            MaxHealth = maxHealth;
            Health = MaxHealth;

            HealthBarShape = new RectangleShape
            {
                Size = new Vector2f(0.8f, 0.1f),
                FillColor = new Color(0, 200, 0),
                Origin = new Vector2f(-0.1f, 0.1f),
                Position = Position
            };
            LostHealthShape = new RectangleShape
            {
                Size = new Vector2f(0.8f, 0.1f),
                FillColor = new Color(200, 0, 0),
                Origin = new Vector2f(-0.1f, 0.1f),
                Position = Position
            };
            SelectionShape = new CircleShape
            {
                Position = Position,
                Radius = 0.490f,
                OutlineThickness = 0.05f,
                OutlineColor = new Color(0, 200, 20, 150),
                FillColor = Color.Transparent
            };
        }

        public AdvancedUnit(BinaryReader r, Game game, Map.Chunk chunk) : base(r, game, chunk)
        {
            int length = r.ReadInt32(); // 5 int
            Inventory = new Dictionary<Item, int>();
            for (int i = 0; i < length; i++)
            {
                Inventory.Add((Item)r.ReadInt32(), r.ReadInt32()); // intX2
            }
            ItemMaxRange = r.ReadSingle(); // float
            MaxHealth = r.ReadSingle(); // float
            Health = r.ReadSingle(); // float

            HealthBarShape = new RectangleShape
            {
                Size = new Vector2f(0.8f, 0.1f),
                FillColor = new Color(0, 200, 0),
                Origin = new Vector2f(-0.1f, 0.1f),
                Position = Position
            };
            LostHealthShape = new RectangleShape
            {
                Size = new Vector2f(0.8f, 0.1f),
                FillColor = new Color(200, 0, 0),
                Origin = new Vector2f(-0.1f, 0.1f),
                Position = Position
            };
            SelectionShape = new CircleShape
            {
                Position = Position,
                Radius = 0.490f,
                OutlineThickness = 0.05f,
                OutlineColor = new Color(0, 200, 20, 150),
                FillColor = Color.Transparent
            };
        }

        public override void WriteTo(BinaryWriter w)
        {
            w.Write(0);
            w.Write((int)Type.AdvancedUnit);
            base.WriteContent(w);
            WriteContent(w); // write base unit 1-2-3-4
        }

        protected override void WriteContent(BinaryWriter w)
        {
            base.WriteContent(w);
            Inventory = new Dictionary<Item, int>();
            w.Write(Inventory.Count); // write int 5
            foreach (var value in Inventory)
            {
                w.Write((int)value.Key); // write int 
                w.Write(value.Value);  // write int
            }
            w.Write(ItemMaxRange); // write float
            w.Write(MaxHealth); // write float
            w.Write(Health); // write float
        }

        public Dictionary<Item, int> Inventory { get; set; }
        public float ItemMaxRange { get; set; }
        event EventHandler GotDamaged;
        public event EventHandler Died;
        public event EventHandler InventoryUpdate;
        public RectangleShape HealthBarShape { get; }
        protected RectangleShape LostHealthShape { get; }
        public float MaxHealth { get; set; }

        public float Health { get;  set; }
        Animation _animation;
        public AnimatedSprite animationHumanDie;
        public override Vector2f Position
        {
            get => base.Position;
            protected set
            {
                base.Position = value;
                HealthBarShape.Position = value;
                LostHealthShape.Position = value;
                SelectionShape.Position = value;
            }
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            Debug.Assert(Health > 0);
            base.Draw(target, states);
            target.Draw(LostHealthShape);
            target.Draw(HealthBarShape);
            target.Draw(SelectionShape);
        }

        public bool CanAttack(Unit unit)
        {
            Debug.Assert(Health > 0);
            //if (unit.Team == Team.Neutral) return false; //Nobody can attack neutral unit
            return unit.Team != Team;
        }

        public void Damage(float damage)
        {
            if (damage <= 0) throw new ArgumentException();
            Health -= damage;
            if (Health <= 0)
            {
                Die();
            }
            HealthBarShape.Size = new Vector2f(0.8f * Health / MaxHealth, 0.1f);
            GotDamaged?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void Die()
        {
            Debug.Assert(Health <= 0);
            Time time = Time.FromSeconds(1);
            int[] die = new int[9];
            die[0] = 29;
            die[1] = 30;
            die[2] = 31;
            die[3] = 32;
            die[4] = 33;
            die[5] = 34;
            die[6] = 35;
            die[7] = 36;
            die[8] = 37;
            Animation humanDie = new Animation(Game, time, false, die);
            animationHumanDie = new AnimatedSprite(humanDie, new RectangleShape
            {
                Position = UnitShape.Position,
                Size = new Vector2f(1, 1),
                Texture = TextureArray.TileSet
            });
            humanDie.EndAnimation += HumanDie_EndAnimation;
            Game.AnimatedSprite.Add(animationHumanDie);

            Game.UnitController.SelectedUnit.Remove(this); //If the unit die you must remove all the referenced instance to it.
            Chunk.ChunkData.RemoveUnit(this);
            Died?.Invoke(this, EventArgs.Empty);
        }

        private void HumanDie_EndAnimation(object sender, EventArgs e)
        {
            Game.AnimatedSprite.Remove(animationHumanDie);
        }

        /// <summary>
        /// Add a given number in the inventory
        /// </summary>
        /// <param name="itm"></param>
        /// <param name="nbr"></param>
        public void AddInInventory(Item itm, int nbr = 1)
        {
            if (Inventory.ContainsKey(itm))
            {
                Inventory[itm] += nbr;
                InventoryUpdate?.Invoke(this, EventArgs.Empty);
                return;
            }
            Inventory.Add(itm, nbr);
            InventoryUpdate?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Return the number of the given Item in the inventory
        /// </summary>
        /// <param name="itm"></param>
        /// <returns></returns>
        public int HowManyInInventory(Item itm)
        {
            if (Inventory.ContainsKey(itm))
            {
                return Inventory[itm];
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Remove a given number of an item from the instancied unit, if you give no number remove all of it
        /// </summary>
        /// <param name="itm"> Item to remove</param>
        /// <param name="nbr"> Number of given item to remove</param>
        public void RemoveFromInventory(Item itm, int nbr = 0)
        {
            if (nbr == 0)
            {
                Inventory.Remove(itm);
            }
            else
            {
                if (nbr > Inventory[itm])
                {
                    Inventory.Remove(itm);
                }
                else
                {
                    Inventory[itm] = Inventory[itm] - nbr;
                }
            }
            InventoryUpdate?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Transfer an given number of item to another unit
        /// </summary>
        /// <param name="u"> cibled unit</param>
        /// <param name="itm"></param>
        /// <param name="nbr"></param>
        public void TransferItemto(AdvancedUnit u, Item itm, int nbr = 0)
        {
            RemoveFromInventory(itm, nbr);
            u.AddInInventory(itm, nbr);
        }

        //public Dictionary<Item,int> DropItem(Item itm, int nbr = 0)
        //{
        //    Dictionary<Item, int> DroppedItem = new Dictionary<Item, int>();
        //    DroppedItem.Add(itm, nbr);
        //    RemoveFromInventory(itm, nbr);         
        //}

        public bool CanCraft(Recipe recette)
        {
            foreach (var item in recette.NeededItems)
            {
                if (!Inventory.ContainsKey(item.Key)) return false;
                if (Inventory[item.Key] < item.Value) return false;
            }
            return true;
        }

        public void CraftRecipe(Recipe recette)
        {
            if (CanCraft(recette))
            {
                foreach (var item in recette.NeededItems)
                {
                    RemoveFromInventory(item.Key, item.Value);
                }
                foreach (var ingredients in recette.ResultItems)
                {
                    AddInInventory(ingredients.Key, ingredients.Value);
                }
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            HealthBarShape?.Dispose();
            LostHealthShape?.Dispose();
            SelectionShape?.Dispose();
        }

    }
}

