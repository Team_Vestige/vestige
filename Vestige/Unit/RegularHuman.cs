﻿using System;
using System.Collections.Generic;
using SFML.System;
using Vestige.GUI;

namespace Vestige.Unit
{
    class RegularHuman : MovableUnit
    {
        public RegularHuman(Game game, Vector2f position) : base(game, UnitType.Human, position, 100f, Team.Human, 30f, 0, 3f, 4)
        {
            game.Humans.Add(this); 
            Actions = new List<Tuple<int, ActionButton.Action>>
            {
                new Tuple<int, ActionButton.Action>(5, CreateWallAction),
                new Tuple<int, ActionButton.Action>(2, AttackWallAction),
                new Tuple<int, ActionButton.Action>(9,CreateStoneExtractor),
                new Tuple<int, ActionButton.Action>(4, CreateTurretAction),
                new Tuple<int, ActionButton.Action>(6, CreateCraftTableAction)
            };
            new Weapon(this, 1.5f, 5, Time.FromSeconds(0.3f), 4);
        }
        void WallAttacker(Vector2l pos)
        {
            new FakeRealWall(Game, pos);
        }

        void AttackWallAction(AdvancedUnit unit)
        {
            new DrawBuilding(Game, TileType.None, WallAttacker, TileType.Wall);
        }
        void CreateStoneExtractor(AdvancedUnit unit)
        {
            if (Inventory.ContainsKey(Item.StoneExtractor))
            {
                new DrawBuilding(Game, TileType.StoneProducer, StoneProducerCreator, TileType.StoneOre);
                RemoveFromInventory(Item.StoneExtractor, 1);
            }
        }
        void CreateWallAction(AdvancedUnit unit)
        {
            if (Inventory.ContainsKey(Item.Wall))
            {
                new DrawBuilding(Game, TileType.HumanWall, WallCreator, TileType.Path);
                RemoveFromInventory(Item.Wall, 1);
            }
        }
        void CreateCraftTableAction(AdvancedUnit unit)
        {
            if (Inventory.ContainsKey(Item.CraftingTable))
            {
                new DrawBuilding(Game, TileType.CraftTable, WallCreator, TileType.Path);
                RemoveFromInventory(Item.CraftingTable, 1);
            }
        }
        void CreateTurretAction(AdvancedUnit unit)
        {
            if (Inventory.ContainsKey(Item.Turret))
            {
                new DrawBuilding(Game, TileType.AutoTurret, TurretCreator, TileType.Path);
                RemoveFromInventory(Item.Turret, 1);
            }
        }
        void TurretCreator(Vector2l pos)
        {
            new AutoTurret(Game, pos);
        }

        void StoneProducerCreator(Vector2l pos)
        {
            new StoneProducer(Game, pos);
        }
        void WallCreator(Vector2l pos)
        {
            new Wall(Game, pos);
        }
    }
}
