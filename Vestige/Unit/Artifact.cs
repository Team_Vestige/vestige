﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige.Unit
{
    class Artifact 
    {
        public enum ArtifactType
        {
            Defense,
            Velocity
        };

        MovableUnit _unit;
        float _percentage;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unit"> Unit to equip to</param>
        /// <param name="percentage"> Float between 0,01 and 1, given percentage of buff for the unit</param>
        /// <param name="type">Type of the Artifact, Defense or Velocity</param>
        public Artifact(MovableUnit unit, float percentage, ArtifactType type)
        {
            _unit = unit;
            _percentage = percentage;
            if (unit.HowManyInInventory(Item.Artifact) == 0)
            { 

                if (type == ArtifactType.Defense)
                {
                    _unit.Health = unit.Health * (_percentage + 1);
                    _unit.MaxHealth = unit.MaxHealth * (_percentage + 1);
                    _unit.AddInInventory(Item.Artifact);

                }
                else if (type == ArtifactType.Velocity)
                {
                    _unit._speed = unit._speed * (_percentage + 1);
                    _unit.AddInInventory(Item.Artifact);

                }
            }
        }

        public void unequipArtifact(MovableUnit unit, ArtifactType type)
        {
            if (unit.HowManyInInventory(Item.Artifact) == 1)
            {
                if (type == ArtifactType.Defense)
                {
                    _unit.Health = unit.Health / (_percentage + 1);
                    _unit.MaxHealth = unit.MaxHealth / (_percentage + 1);
                    _unit.RemoveFromInventory(Item.Artifact);

                }
                else if (type == ArtifactType.Velocity)
                {
                    _unit._speed = unit._speed / (_percentage + 1);
                    _unit.RemoveFromInventory(Item.Artifact);

                }
                
            }

        }


    }
}
