﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using SFML.System;

namespace Vestige.Unit
{
    internal class AmmoWeapon : Weapon
    {

        public AmmoWeapon(AdvancedUnit unit, float range, float damage, Time cooldown, float aggroRange)
            : base(unit, range, damage, cooldown, aggroRange)
        {
           
        }
        
        public bool CanShoot()
        {
            foreach (KeyValuePair<Item, int> item in _unit.Inventory)
            {
                if (item.Key == Item.Ammo)
                {
                    int ammo = item.Value;
                    return ammo > 0;
                }
            }
                return false;
        }

        public override bool TryAttack()
        {
            if (CanShoot())
            {
                if (base.TryAttack())
                {
                    foreach (KeyValuePair<Item, int> item in _unit.Inventory)
                    {
                        if (item.Key == Item.Ammo)
                        {
                            _unit.RemoveFromInventory(item.Key, 1);
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
