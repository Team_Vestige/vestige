﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige.Unit
{
    class Wall : Building
    {
        public Wall(Game game,Vector2l position) : base(game, UnitType.Wall, position, new[,]{{true}}, Team.Human, 0, 3, TileType.HumanWall,TileType.Path)
        {

        }
    }
}
