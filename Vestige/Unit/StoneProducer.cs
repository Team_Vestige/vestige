﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace Vestige.Unit
{
    class StoneProducer : ProducerBuilding
    {
        public StoneProducer(Game game, Vector2l position) : base(game, UnitType.StoneProducer, position, new[,]{{true}}, Team.Human, 3, 4, TileType.StoneProducer, new Dictionary<Item, int> {{Item.Stone, 1}}, Time.FromSeconds(5), TileType.StoneOre)
        {
        }
    }
}
