﻿using SFML.System;
using System;
using System.IO;
using SFML.Graphics;

namespace Vestige.Unit
{
    class Building : AdvancedUnit
    {
        bool[,] _hitbox;
        readonly TileType _deathTile;
        protected Game _game;
        public Building (Game game, UnitType id, Vector2l position, bool[,] hitbox, Team team, float itemRange, float viewRange, TileType tileType, TileType deathTile)
            :base(game, id , (Vector2f)position, 250, team, itemRange, viewRange)
        {
            _hitbox = hitbox ?? throw new ArgumentNullException(nameof(hitbox));
            _deathTile = deathTile;
            _game = game ?? throw new ArgumentNullException(nameof(game));
            for (int x = 0; x < _hitbox.GetLength(0); x++)
            {
                for (int y = 0; y < _hitbox.GetLength(1); y++)
                {
                    if (_hitbox[x, y])
                    {
                        Vector2l coo = (Vector2l) Position + new Vector2l(x, y);
                        Map.Chunk chunk = _game.Map.GetChunkOfPoint(coo);
                        Vector2i tileCoo = chunk.GetRelativeCoordinate(coo);
                        chunk[tileCoo] = Tile.Create(tileType, 0);
                    }
                }
            }
        }

        public Building(BinaryReader r,Game game, Map.Chunk chunk) : base(r,game,chunk)
        {
            int width = r.ReadInt32();
            int heigth = r.ReadInt32();
            _hitbox = new bool[width, heigth];

            for (int i = 0; i < width; i++)
            {
                for (int p = 0; i < heigth; i++)
                {
                    _hitbox[i, p] = r.ReadBoolean();
                }
            }
        }

        protected override void Die()
        {
            for (int x = 0; x < _hitbox.GetLength(0); x++)
            {
                for (int y = 0; y < _hitbox.GetLength(1); y++)
                {
                    if (_hitbox[x, y])
                    {
                        Vector2l coo = (Vector2l)Position + new Vector2l(x, y);
                        Map.Chunk chunk = _game.Map.GetChunkOfPoint(coo);
                        Vector2i tileCoo = chunk.GetRelativeCoordinate(coo);
                        chunk[tileCoo] = Tile.Create(_deathTile, 0);
                    }
                }
            }
            base.Die();
        }

        public override void WriteTo(BinaryWriter w)
        {
            w.Write(0);
            w.Write((int)Type.Building);
            base.WriteContent(w);
            w.Write(_hitbox.GetLength(0));
            w.Write(_hitbox.GetLength(1));
            for (int i = 0; i < _hitbox.GetLength(0); i++)
            {
                for (int p = 0; i < _hitbox.GetLength(1); i++)
                {
                    w.Write(_hitbox[i,p]);
                }       
            }
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(LostHealthShape);
            target.Draw(HealthBarShape);
        }
    }
}
