﻿using System;
using System.Diagnostics;
using SFML.Graphics;
using SFML.System;
using System.IO;

namespace Vestige.Unit
{
    public enum State
    {
        Waiting,
        Moving,
        MovingAttacking,
        Attacking,
        HoldPosition
    }
    public enum Team
    {
        Neutral,
        Human,
        Alien
    }
    abstract class Unit : Drawable, IDisposable
    {
        const int BinaryVersion = 0;
        protected enum Type
        {
            AdvancedUnit,
            Building,
            MovableUnit
        }
        public enum UnitType
        {
            AutoTurret,
            Wall,
            Alien,
            Human,
            CraftTable,
            StoneProducer
        }
        public readonly Game Game;
        public Team Team { get; }
        public State State { get; set; } = State.Waiting; 
        protected RectangleShape UnitShape { get; }
        public Map.Chunk Chunk;
        public virtual Vector2f Position
        {
            get => UnitShape.Position;
            protected set => UnitShape.Position = value;

        }
        public UnitType Id { get; }

        /// <summary>
        /// TO REDO
        /// </summary>
        /// <param name="game">Instance of the game where is the unit</param>
        /// <param name="id">Id of the unit, no usage for now</param>
        /// <param name="position">Position where the unit is created</param>
        /// <param name="maxHealth">MaxHealth of the unit</param>
        /// <param name="owner"></param>
        protected Unit(Game game, UnitType id, Vector2f position, float maxHealth, Team owner)
        {
            if (maxHealth <= 0) throw new ArgumentOutOfRangeException(nameof(maxHealth) + " should be more than 0");
            Game = game;
            Chunk = game.Map[Map.Map.PositionOfChunk((Vector2d)position)];
            Chunk.ChunkData.AddUnit(this);
            Id = id;
            Team = owner;
            
            UnitShape = new RectangleShape 
            {
                Position = position,
                Size = new Vector2f(1, 1),
                Texture = TextureArray.TileSet,
                TextureRect = TextureArray.GetTextureCoord(Id.GetUnitId()).IntRect()
            };
            Debug.Assert(game.Map[Map.Map.PositionOfChunk((Vector2d)Position)] == Chunk);
        }

       protected Unit(BinaryReader r, Game game, Map.Chunk chunk)
        {
            Game = game;
            Id =(UnitType) r.ReadByte();                                          // 1 readByte
            Team = (Team) r.ReadInt32();                                // 2 readInt
            var position = new Vector2f(r.ReadSingle(), r.ReadSingle());// 3-4 float
            Chunk = chunk; 
            UnitShape = new RectangleShape
            {
                Position = position,
                Size = new Vector2f(1, 1),
                Texture = TextureArray.TileSet,
                TextureRect = TextureArray.GetTextureCoord(Id.GetUnitId()).IntRect()
            };
        }

        public abstract void WriteTo(BinaryWriter w);

        protected virtual void WriteContent(BinaryWriter w)
        {
            w.Write(Id.GetUnitId());            // 1 byte
            w.Write((int)Team);     // 2 int
            w.Write(Position.X);    // 3 float
            w.Write(Position.Y);    // 4 float
        }

        public static Unit Read(BinaryReader r, Game game, Map.Chunk chunk)
        {
            if(r.ReadInt32() != BinaryVersion) throw new InvalidDataException("bad version");
            switch ((Type) r.ReadInt32())
            {
                case Type.AdvancedUnit:
                    return new AdvancedUnit(r, game, chunk);
                case Type.Building:
                    return new Building(r, game, chunk);
                case Type.MovableUnit:
                    return new MovableUnit(r, game, chunk);
                default:
                    throw new InvalidDataException("Corrupted file");
            }
        }


        public virtual void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(UnitShape);
        }

        public virtual void Dispose()
        {
            UnitShape?.Dispose();
        }
    }
}
