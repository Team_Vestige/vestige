﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using Vestige.GUI;
using Vestige.Map;

namespace Vestige.Unit
{
    class CraftTable : Building
    {
        
        public CraftTable(Game game, Vector2l position) : base(game, UnitType.CraftTable, position, new[,] { { true } }, Team.Human, 3, 3, TileType.CraftTable, TileType.Path)
        {
            Actions = new List<Tuple<int, ActionButton.Action>>
            {
                new Tuple<int, ActionButton.Action>(5, CraftWall),
                new Tuple<int, ActionButton.Action>(55, CraftTurret),
                new Tuple<int, ActionButton.Action>(166,CraftAmmo),
                new Tuple<int, ActionButton.Action>(6, CraftCraftTable),
                new Tuple<int, ActionButton.Action>(9, CraftOreProducer),
            };
        }

        
        void CraftOreProducer(AdvancedUnit unit)
        {
            unit.CraftRecipe(_game.RecipesList.Recipes[(int)RecipesList.RecipeType.CraftOreProducer]);
        }

        void CraftCraftTable(AdvancedUnit unit)
        {
            unit.CraftRecipe(_game.RecipesList.Recipes[(int)RecipesList.RecipeType.CraftCraftTable]);
        }
        void CraftAmmo(AdvancedUnit unit)
        {
            unit.CraftRecipe(_game.RecipesList.Recipes[(int)RecipesList.RecipeType.CraftAmmo]);
        }
        void CraftTurret(AdvancedUnit unit)
        {
            unit.CraftRecipe(_game.RecipesList.Recipes[(int)RecipesList.RecipeType.CraftTurret]);
        }
        void CraftWall(AdvancedUnit unit)
        {
            unit.CraftRecipe(_game.RecipesList.Recipes[(int)RecipesList.RecipeType.CraftWall]);
        }
    }
}
