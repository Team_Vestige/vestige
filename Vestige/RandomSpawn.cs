﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using Vestige.Map;
using Vestige.Unit;

namespace Vestige
{
    class RandomSpawn
    {
        Game _game;
        Random _random;

        public RandomSpawn(Game game)
        {
            _game = game;
            _random = new Random();
        }
        public void AlienSpawn()
        {
            foreach (var chunk in _game.Map)
            {
                int randomNumber = _random.Next(1, 10);
                if (chunk.Value.ChunkData.Units.FirstOrDefault(unit => unit.Team == Unit.Team.Human) != null) continue;
                if (chunk.Value.ChunkData.Units.Count > 6) continue;
                if (chunk.Value.ZoneData == null || randomNumber != 5) continue;
                var pos = new Vector2i(_random.Next(0, 15), _random.Next(0, 15));
                var k = 0;
                while (chunk.Value.ZoneData[pos, 1] == null)
                {
                    k++;
                    pos = new Vector2i(_random.Next(0, 15), _random.Next(0, 15));
                    if (k == 5) break;
                }
                if (k != 5)
                {
                    var alien = new Unit.MovableUnit(_game, Unit.Unit.UnitType.Alien, (Vector2f)pos + Common.MultiplyVector(chunk.Value.Position, 16) + new Vector2f((float)_random.NextDouble(), (float)_random.NextDouble()), 100f, Unit.Team.Alien, 30f, 0, 3f, 3);
                    new Unit.Weapon(alien, 2, 10f, Time.FromSeconds(0.4f), 5);
                }
            }
        }

        public static void ChanceOfHumanSpawn(Chunk chunk, Random random, Game game)
        {
            if (chunk.ChunkData.Units.FirstOrDefault(unit => unit.Team == Team.Human) != null) return;
                //int randomNumber = _random.Next(1, 10);
                //var pos = new Vector2i(_random.Next(0, 15), _random.Next(0, 15));
                //if(randomNumber == 5)
                // new Unit.MovableUnit(_game, 0, new Vector2f(pos.X, pos.Y), 100f, Unit.Team.Human, 100f, 0, 0.5f, 4);
                int randomNumber = random.Next(1, 5);
                var pos = new Vector2i(random.Next(0, 15), random.Next(0, 15));
                var k = 0;
                while (chunk.ZoneData[pos, 1] == null)
                {
                    k++;
                    pos = new Vector2i(random.Next(0, 15), random.Next(0, 15));
                }

            //if(chunk.ChunkData.Units.Contains())
            //int[] num = new int[chunk.ChunkData.Units.Count]
            if(randomNumber == 4)
            new Unit.MovableUnit(game, 0, (Vector2f)pos + Common.MultiplyVector(chunk.Position, 16) + new Vector2f((float)random.NextDouble(), (float)random.NextDouble()), 100f, Unit.Team.Human, 30f, 0, 3f, 3);
        }

    }
}
