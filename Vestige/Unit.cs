﻿using System.Collections.Generic;
using SFML.Graphics;
using SFML.System;

namespace Vestige
{
    class Unit : Drawable
    {
        readonly RectangleShape _shape;
        protected readonly Game Game;

        /// <summary>
        /// TO REDO
        /// </summary>
        /// <param name="game"></param>
        /// <param name="id"></param>
        /// <param name="position"></param>
        protected Unit(Game game, byte id, Vector2f position)
        {
            Game = game;
            Chunk = Game.Map.ChunkOfPoint((Vector2d) position);
            Chunk.AddUnit(this);
            GetId = id;

            _shape = new RectangleShape
            {
                Position = position,
                Size = new Vector2f(1, 1),
                Texture = new Texture("resources/4.png")//TODO use tileset

            };
            SelectionShape = new CircleShape
            {
                Position = Position,
                Radius = 0.490f,
                OutlineThickness = 0.05f,
                OutlineColor = new Color(0,200,20, 75),
                FillColor = Color.Transparent
            };

        }
        public CircleShape SelectionShape { get; }

        protected Chunk Chunk { get; set; }

        public Vector2f Position {
            get => _shape.Position;
            protected set
            {
                _shape.Position = value;
                SelectionShape.Position = _shape.Position;
            }
        }

        public Dictionary<Item, int> Inventory { get; set; }

        public byte GetId { get; }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(_shape);
        }
    }
}
