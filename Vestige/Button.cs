﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;

namespace Vestige
{
    class Button : Drawable
    {
        public event EventHandler OnOverEnter;  //When Mouse On Button
        public event EventHandler OnOverExit;   //When Mouse Not On Button
        public event EventHandler OnClick;  //When Mouse Click Button
        public event EventHandler OnClickRelease;   //When Mouse Release Click Button
        bool _overing;

        readonly RectangleShape _button;
        readonly RenderWindow _window;
        static List<Button> _list = new List<Button>();

        static bool _firstInstance = true;
        public Button(RenderWindow window)
        {
            if (_firstInstance) //TODO check if it's bad design
            {
                _firstInstance = false;
                window.MouseMoved += AllButtonCheckOver;
                window.MouseButtonPressed += AllButtonClickPressed;
                window.MouseButtonReleased += AllButtonClickReleased;
            }
           
            _window = window;
            _button = new RectangleShape();
            _list.Add(this);
        }
        public virtual Vector2f Position
        {
            get => _button.Position;
            set => _button.Position = value;
        }
        public Vector2f Size
        {
            get => _button.Size;
            set => _button.Size = value;
        }
        public Texture Texture
        {
            get => _button.Texture;
            set => _button.Texture = value;
        }

        public virtual Color FillColor
        {
            get => _button.FillColor;
            set => _button.FillColor = value;
        }

        static void AllButtonClickPressed(object sender, MouseButtonEventArgs e)
        {
            foreach (Button x in _list)
            {
                x.MouseClickPressed();
            }
        }
        static void AllButtonClickReleased(object sender, MouseButtonEventArgs e)
        {
            foreach (Button x in _list)
            {
                x.MouseClickReleased();
            }
        }
        static void AllButtonCheckOver(object sender, MouseMoveEventArgs e)
        {
            foreach (Button x in _list)
            {
                x.CheckCursorOverButton();
            }
        }

        /// <summary>
        /// Return true if the point is on axes aligned rectangle.
        /// </summary>
        /// <param name="a">top left rectangle</param>
        /// <param name="b">buttom right</param>
        /// <param name="c">mouse position</param>
        /// <returns>return true if "c" is in rectangle between "a" and "b"</returns>
        static bool CursorInRect(Vector2f a, Vector2f b, Vector2f c)
        {
            if (b.X < a.X || b.Y < a.Y)
            {
                throw new ArgumentException("BAD RECTANGLE");
            }
            return c.X > a.X && c.X < b.X && c.Y > a.Y && c.Y < b.Y;
        }

        void MouseClickPressed()
        {
            Vector2f a = _button.Position;
            Vector2f b = a + _button.Size;
            Vector2f c = (Vector2f)Mouse.GetPosition(_window);
            if (CursorInRect(a, b, c))
            {
                OnClick?.Invoke(this, EventArgs.Empty);     
            }
        }

        void MouseClickReleased()
        {
            Vector2f a = _button.Position;
            Vector2f b = a + _button.Size;
            Vector2f c = (Vector2f)Mouse.GetPosition(_window);
            if (CursorInRect(a, b, c))
            {
                OnClickRelease?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Call event when mouse Enter/Exit, Click/Release rectangle 
        /// </summary>
        void CheckCursorOverButton()
        {
            Vector2f a = _button.Position;
            Vector2f b = a + _button.Size;
            Vector2f c = (Vector2f)Mouse.GetPosition(_window);
            if (CursorInRect(a, b, c))
            {
                if (_overing) return;
                OnOverEnter?.Invoke(this, EventArgs.Empty);
                _overing = true;
            }
            else
            {
                if (!_overing) return;
                OnOverExit?.Invoke(this, EventArgs.Empty);
                _overing = false;
            }
        }

        public virtual void Draw(RenderTarget target, RenderStates states)
        {
            _button.Draw(target, states);
        }
    }
}