﻿namespace Vestige.GUI
{
    class ItemButton : SubButtonText
    {
        Unit.AdvancedUnit _unit;
        readonly Item _item;
        public ItemButton(GUIMananager guiMananager, InventoryContainer parent, Unit.AdvancedUnit unit, Item item) : base(guiMananager, parent, unit.HowManyInInventory(item).ToString())
        {
            _unit = unit;
            _item = item;
            Texture = TextureArray.TileSet;
            TextureRect = TextureArray.GetTextureCoord(_item.GetItemId()).IntRect();
        }

        protected override void MoveSubButton(WidgetsOrganizedContainer container)
        {
            if (!(container is InventoryContainer iContainer) || Common.Distance(_unit.Position, iContainer.Unit.Position) >
                _unit.ItemMaxRange)
            {
                Position = Parent.Position;
                return;
            }
            base.MoveSubButton(container);
            if (iContainer.Unit.Inventory.ContainsKey(_item))
            {
                iContainer.Unit.Inventory[_item] += _unit.Inventory[_item];
                _unit = iContainer.Unit;
                return;
            }
            iContainer.Unit.Inventory.Add(_item, _unit.Inventory[_item]);
            _unit.Inventory.Remove(_item);
            _unit = iContainer.Unit;
        }

    }
}
