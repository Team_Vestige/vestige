﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Window;

namespace Vestige.GUI
{
    class ActionButton : Button
    {
        public delegate void Action(Unit.AdvancedUnit unit);
        public ActionButton(GUIMananager guiMananager, Action action, Unit.AdvancedUnit unit) : base(guiMananager, 0, true)
        {
            OnClick += (sender, args) => action(unit);
        }

     
    }
}
