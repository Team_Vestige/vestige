﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Vestige.GUI
{
    class UnitButton : Button
    {
        public Unit.AdvancedUnit Unit;
        RectangleShape healthUnit;
        Hud _hud;
        public UnitButton(Hud hud, Unit.AdvancedUnit unit) : base(hud.GUIMananager, 0, true)
        {
            _hud = hud;
            Texture = TextureArray.TileSet;
            TextureRect = TextureArray.GetTextureCoord(unit.Id.GetUnitId()).IntRect();
            Unit = unit;
            healthUnit = new RectangleShape();//TODO later
        }

        public override Vector2f Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                healthUnit.Position = value;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            healthUnit.Dispose();
        }

        public override Vector2f Origin
        {
            get => base.Origin;
            set
            {
                base.Origin = value;
                healthUnit.Origin = value;
            }
        }

        public override Vector2f Size
        {
            get => base.Size;
            set
            {
                base.Size = value;
                healthUnit.Size = value;
            }
        }

        public override bool MouseButtonPressed(MouseButtonEventArgs e)
        {
            if (MouseInWidget())
            {
                _hud.InventoryOpener.TryOpen(Unit);
            }
            return base.MouseButtonPressed(e);
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            base.Draw(target, states);
            //healthUnit.Draw(target, states);
        }
    }
}
