﻿using System;
using System.Diagnostics;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Vestige.GUI
{
    class Widget : Drawable, IDisposable
    {
        public GUIMananager GUIMananager;
        public RectangleShape Shape { get; set; }
        public Widget(GUIMananager guiMananager, int layer)
        {
            Shape = new RectangleShape();
            GUIMananager = guiMananager;
            guiMananager.Layers[layer].Add(this);
        }
        public virtual Vector2f Size
        {
            get => Shape.Size;
            set
            {
                if (Shape.CPointer != IntPtr.Zero)
                {
                    Shape.Size = value;
                }
            }
        }

        public virtual Vector2f Position
        {
            get => Shape.Position;
            set => Shape.Position = value;
        }
        public virtual Vector2f Origin
        {
            get => Shape.Origin;
            set => Shape.Origin = value;
        }

        public Color FillColor
        {
            get => Shape.FillColor;
            set => Shape.FillColor = value;
        }

        public IntRect TextureRect
        {
            get => Shape.TextureRect;
            set => Shape.TextureRect = value;
        }
        public Texture Texture
        {
            get => Shape.Texture;
            set => Shape.Texture = value;
        }

        public Color OutlineColor
        {
            get => Shape.OutlineColor;
            set => Shape.OutlineColor = value;
        }

        public float OutlineThickness
        {
            get => Shape.OutlineThickness;
            set => Shape.OutlineThickness = value;
        }

        /// <summary>
        /// Triggered if the mouse button is clicked.  
        /// If the value returned is true, the GUIManager will trigger the next layer
        /// </summary>
        /// <returns></returns>
        public virtual bool MouseButtonPressed(MouseButtonEventArgs e)
        {
            return false;
        }

        /// <summary>
        /// Triggered if the mouse button is released.  
        /// If the value returned is true, the GUIManager will trigger the next layer
        /// </summary>
        /// <returns></returns>
        public virtual bool MouseButtonReleased(MouseButtonEventArgs e)
        {
            return false;
        }

        /// <summary>
        /// Triggered if the widget is moved.  
        /// If the value returned is true, the GUIManager will trigger the next layer
        /// </summary>
        /// <returns></returns>
        public virtual bool MouseMoved(MouseMoveEventArgs e)
        {
            return false;
        }

        public bool MouseInWidget()
        {
            Vector2f a = Position - Origin;
            Vector2f b = a + Size;
            Vector2f c = (Vector2f)Mouse.GetPosition(GUIMananager.Window);

            return Common.CursorInRect(a, b, c);
        }

        public bool Remove()
        {
            bool removed = false;
            foreach (var layer in GUIMananager.Layers)
            {
                if (layer.Remove(this))
                {
                    removed = true;
                }
            }
            Debug.Assert(removed);
            Dispose();
            return true;
        }

        public virtual void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(Shape);
        }

        public virtual void Dispose()
        {
            Shape?.Dispose();
        }
    }
}
