﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;

namespace Vestige.GUI
{
    class WidgetsOrganizedContainer : WidgetCollection
    {
        readonly Vector2f _widgetSpacement;
        readonly bool _columnOverflow;
        readonly int _sideLimit;
        Vector2i _lastWidgetPos;

        /// <summary>
        /// Manage multiple widget and set an origin for each widget based on widgetSpacement
        /// </summary>
        /// <param name="layer"></param>
        /// <param name="widgetSpacement">Spacement between two widget</param>
        /// <param name="columnOverflow">If there is too much widget, does will add more column ? If false it add more line.</param>
        /// <param name="sideLimit"></param>
        /// <param name="guiMananager"></param>
        public WidgetsOrganizedContainer(GUIMananager guiMananager, int layer, Vector2f widgetSpacement, bool columnOverflow, int sideLimit) : base(guiMananager, layer)
        {
            _widgetSpacement = widgetSpacement;
            _columnOverflow = columnOverflow;
            _sideLimit = sideLimit;
            if (_columnOverflow)
            {
                Size = -new Vector2f(_widgetSpacement.X, _widgetSpacement.Y * _sideLimit);
            }
            else
            {
                Size = -new Vector2f(_widgetSpacement.X * _sideLimit, _widgetSpacement.Y);
            }
        }


        public override Vector2f Position
        {
            get => base.Position;
            set
            {
                foreach (var elem in this)
                {
                    elem.Position = value;
                }
                base.Position = value;
            }
        }

        public override void Add(Widget item)
        {
            if (item == null) throw new ArgumentException("Adding a null value to the collection");
            if (Count == 0)
            {
                if (_columnOverflow)
                {
                    Size = -new Vector2f(_widgetSpacement.X, _widgetSpacement.Y * _sideLimit);
                }
                else
                {
                    Size = -new Vector2f(_widgetSpacement.X * _sideLimit, _widgetSpacement.Y);
                }
            }
            base.Add(item);
            item.Origin = Common.MultiplyVector(_lastWidgetPos, _widgetSpacement) + Origin;
            item.Position = Position;
            if (_columnOverflow)
            {
                _lastWidgetPos.Y++;
                if (_lastWidgetPos.Y >= _sideLimit)
                {
                    _lastWidgetPos.Y = 0;
                    _lastWidgetPos.X++;
                    Size -= new Vector2f(_widgetSpacement.X, 0);
                }
            }
            else
            {
                _lastWidgetPos.X++;
                if (_lastWidgetPos.X >= _sideLimit)
                {
                    _lastWidgetPos.X = 0;
                    _lastWidgetPos.Y++;
                    Size -= new Vector2f(0, _widgetSpacement.Y);
                }
            }

        }

        public override bool Remove(Widget item)
        {
            bool output = base.Remove(item);
            RealignWidgets();
            return output;
        }

        public override void Clear()
        {
            if (_columnOverflow)
            {
                Size = -new Vector2f(_widgetSpacement.X, _widgetSpacement.Y * _sideLimit);
            }
            else
            {
                Size = -new Vector2f(_widgetSpacement.X * _sideLimit, _widgetSpacement.Y);
            }
            base.Clear();
            _lastWidgetPos = new Vector2i();
        }

        public void DeleteChilds()
        {
            foreach (var elem in this)
            {
                elem.Remove();
            }
            Clear();
        }


        public void RealignWidgets()
        {
            var copy = new List<Widget>(this);  //Now we will reinject all the element
            Clear();
            if(copy.Count>0) Size = new Vector2f();
            foreach (var elem in copy)              //To have all the widget realigned
            {
                Add(elem);
            }
        }
    }
}
