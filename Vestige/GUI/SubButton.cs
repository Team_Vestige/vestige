﻿using System.Linq;
using SFML.System;
using SFML.Window;

namespace Vestige.GUI
{
    class SubButton : Button
    {
        bool _clicked;
        Vector2f _lastMousePos;
        public WidgetsOrganizedContainer Parent;
        public SubButton(GUIMananager guiMananager, WidgetsOrganizedContainer parent) : base(guiMananager, 0, true)
        {
            Parent = parent;
        }

        public override bool MouseButtonPressed(MouseButtonEventArgs e)
        {
            if (MouseInWidget())
            {
                _clicked = true;
                _lastMousePos = (Vector2f)Mouse.GetPosition(GUIMananager.Window);
                return true;
            }
            _clicked = false;
            return false;
        }

        public override bool MouseButtonReleased(MouseButtonEventArgs e)
        {
            if (_clicked)
            {
                _clicked = false;
                foreach (var item in GUIMananager.Layers.Reverse())
                {
                    foreach (var widget in item)
                    {
                        if (widget.MouseInWidget() && widget is WidgetsOrganizedContainer container)
                        {
                            Parent.Remove(this);
                            Parent = container;
                            container.Add(this);
                            return true;
                        }
                    }
                }
                Position = Parent.Position;
            }
            return false;

        }

        public override bool MouseMoved(MouseMoveEventArgs e)
        {
            if (_clicked)
            {
                Vector2f newPos = (Vector2f)Mouse.GetPosition(GUIMananager.Window);
                Position -= _lastMousePos - newPos;
                _lastMousePos = newPos;
                return true;
            }
            return false;
        }
    }
}

