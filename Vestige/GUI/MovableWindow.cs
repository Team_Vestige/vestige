﻿using SFML.System;
using SFML.Window;

namespace Vestige.GUI
{
    class MovableWindow : Window
    {
        Vector2f _lastMousePos;
        bool _clicked;
        public MovableWindow(GUIMananager guiMananager, Widget childWidget, string windowName) : base(guiMananager, childWidget, windowName)
        {
        }

        public override Vector2f Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                Widget.Position = value;
            }
        }

        public override bool MouseButtonPressed(MouseButtonEventArgs e)
        {
            if (MouseInWidget())
            {
                _clicked = true;
                _lastMousePos = (Vector2f)Mouse.GetPosition(GUIMananager.Window);
                return true;
            }
            _clicked = false;
            return false;
        }

        public override bool MouseButtonReleased(MouseButtonEventArgs e)
        {
            _clicked = false;
            return false;
        }

        public override bool MouseMoved(MouseMoveEventArgs e)
        {
            if (_clicked)
            {
                Vector2f newPos = (Vector2f)Mouse.GetPosition(GUIMananager.Window);
                Position -= _lastMousePos - newPos;
                _lastMousePos = newPos;
                return true;
            }
            return false;
        }
    }
}
