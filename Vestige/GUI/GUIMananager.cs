﻿using System;
using System.Collections.Generic;
using System.Linq;
using SFML.Graphics;
using SFML.Window;

namespace Vestige.GUI
{
    class GUIMananager : Drawable, IDisposable
    {
        public readonly List<Widget>[] Layers;
        public Game Game;
        public RenderWindow Window;
        public Hud Hud;
        public GUIMananager(RenderWindow window)
        {
            Layers = new List<Widget>[3];
            Layers[0] = new List<Widget>();
            Layers[1] = new List<Widget>();
            Layers[2] = new List<Widget>();
            Window = window;
            window.MouseButtonPressed += Window_MouseButtonPressed;
            window.MouseButtonReleased += Window_MouseButtonReleased;
            window.MouseMoved += Window_MouseMoved;
        }
        public GUIMananager(Game game)
        {
            Window = game.Window;
            Game = game;
            Layers = new List<Widget>[3];
            Layers[0] = new List<Widget>();
            Layers[1] = new List<Widget>();
            Layers[2] = new List<Widget>();
            game.GameEvents.MouseButtonPressed += Window_MouseButtonPressed;
            game.GameEvents.MouseButtonReleased += Window_MouseButtonReleased;
            game.GameEvents.MouseMoved += Window_MouseMoved;

            Hud = new Hud(this);
            new RubberBand(Hud);
        }

        public void Unsuscribe()
        {
            Window.MouseButtonPressed -= Window_MouseButtonPressed;
            Window.MouseButtonReleased -= Window_MouseButtonReleased;
            Window.MouseMoved -= Window_MouseMoved;
        }

        void Window_MouseMoved(object sender, MouseMoveEventArgs e)
        {
            foreach (var layer in Layers)
            {
                foreach (var widget in layer)
                {
                    if (widget.MouseMoved(e))
                    {
                        //trigger = true;
                    }
                }
                //if (trigger) break; 
            }
        }

        void Window_MouseButtonReleased(object sender, MouseButtonEventArgs e)
        {
            //bool trigger = false;
            foreach (var layer in Layers)
            {
                foreach (var widget in layer)
                {
                    if (widget.MouseButtonReleased(e))
                    {
                        //trigger = true;
                    }
                }
                //if (trigger) break;
            }

        }

        void Window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            bool trigger = false;
            foreach (var layer in Layers)
            {
                foreach (var widget in layer)
                {
                    if (widget.MouseButtonPressed(e))
                    {
                        trigger = true;
                        break;
                    }
                }
                if (trigger) break;
            }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            foreach (var layer in Layers.Reverse())
            {
                foreach (var widget in layer)
                {
                    target.Draw(widget);
                }
            }
        }

        public void Dispose()
        {
            foreach (var layer in Layers)
            {
                foreach (var elem in layer)
                {
                    elem?.Dispose();
                }
            }
        }
    }
}
