﻿using System;
using System.Collections.Generic;
using SFML.Graphics;
using SFML.System;

namespace Vestige.GUI
{
    class Window : ButtonText
    {
        protected Widget ChildWidget;

        public virtual Widget Widget
        {
            get { return ChildWidget; }
            set
            {
                ChildWidget = value;
                ChildWidget.Position = Position;
                ChildWidget.Origin += new Vector2f(0, -40);
            }
        }

        public Window(GUIMananager guiMananager, Widget childWidget, string windowName) : base(guiMananager, 1, windowName, Fonts.Basic, true)
        {
            ChildWidget = childWidget;
        }

        public override Vector2f Position
        {
            get => Shape.Position;
            set
            {
                base.Position = value;
                if (ChildWidget != null)
                {
                    Widget.Position = value;
                }
               
            }
        }
    }
}
