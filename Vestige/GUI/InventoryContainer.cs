﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace Vestige.GUI
{
    class InventoryContainer : WidgetsOrganizedContainer
    {
        internal Unit.AdvancedUnit Unit;
        protected internal EventHandler Resizing;
        public InventoryContainer(GUIMananager guiMananager, int layer, Vector2f widgetSpacement, bool columnOverflow, int sideLimit, Unit.AdvancedUnit unit) : base(guiMananager, layer, widgetSpacement, columnOverflow, sideLimit)
        {
            Unit = unit;
            foreach (var elem in unit.Inventory)
            {
                Add(new ItemButton(GUIMananager, this, unit, elem.Key) { Size = new Vector2f(25, 25) });
            }
            unit.InventoryUpdate += Unit_InventoryUpdate;
        }

        void Unit_InventoryUpdate(object sender, EventArgs e)
        {
            DeleteChilds();
            foreach (var elem in Unit.Inventory)
            {
                Add(new ItemButton(GUIMananager, this, Unit, elem.Key) { Size = new Vector2f(25, 25) });
            }
            RealignWidgets();
        }

        public override Vector2f Size
        {
            get => base.Size;
            set
            {
                base.Size = value;
                
                    Resizing?.Invoke(this, EventArgs.Empty);
                
            }
        }
    }
}
