﻿using System.Collections.Generic;
using System.Linq;

namespace Vestige.GUI
{
    class InventoryOpener
    {
        readonly List<InventoryWindow> _opened;
        readonly GUIMananager _guiMananager;
        public InventoryOpener(GUIMananager guiMananager)
        {
            _opened = new List<InventoryWindow>();
            _guiMananager = guiMananager;
        }


        public void TryOpen(Unit.AdvancedUnit unit)
        {

            InventoryWindow opened = _opened.Where((window => window.Unit == unit)).FirstOrDefault();
            if (opened == null)
            {
                _opened.Add(new InventoryWindow(_guiMananager, unit));
            }
            else
            {
                opened.Position = _guiMananager.Window.DefaultView.Center;
            }
        }


        public bool Remove(InventoryWindow elem)
        {
            elem.Remove();
            return _opened.Remove(elem);
        }

        public bool Remove(Unit.Unit unit)
        {
            
            InventoryWindow elem = _opened.Find(window => window.Unit == unit);
            return Remove(elem);
        }
        

    }
}
