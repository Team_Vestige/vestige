﻿using System.Collections.Generic;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Vestige.GUI
{
    class RubberBand : Button
    {
        Vector2f _firstpos;
        Vector2f _secondpos;
        bool _isclicked;
        readonly Hud _hud;
        public RubberBand(Hud hud): base(hud.GUIMananager, 2, true)
        {

            FillColor = new Color(150, 0, 0, 120);
            OutlineColor = new Color(200, 0, 0, 200);
            OutlineThickness = 3;
            _hud = hud;
        }

        public override bool MouseMoved(MouseMoveEventArgs e)
        {
            if (!_isclicked) return false;
            _secondpos = (Vector2f)Mouse.GetPosition(GUIMananager.Game.Window);
            var firstReversed = _firstpos;
            var seconReversed = _secondpos;
            if (_firstpos.X > _secondpos.X)
            {
                firstReversed.X = _secondpos.X;
                seconReversed.X = _firstpos.X;
            }
            if (_firstpos.Y > _secondpos.Y)
            {
                firstReversed.Y = _secondpos.Y;
                seconReversed.Y = _firstpos.Y;
            }
            SelectUnitInRectangle(firstReversed, seconReversed);
            return false;
        }

        /// <summary>
        /// Put the unit thate are in the rectangle in the selected unit of UnitController
        /// </summary>
        /// <param name="firstPos"></param>
        /// <param name="secondPos"></param>
        void SelectUnitInRectangle(Vector2f firstPos, Vector2f secondPos)
        {
            Vector2d mapSecondPos = Map.Map.WindowPositionToMapPositon(secondPos, GUIMananager.Game.CameraView, GUIMananager.Game.Window);
            Vector2d mapFirstPos = Map.Map.WindowPositionToMapPositon(firstPos, GUIMananager.Game.CameraView, GUIMananager.Game.Window);
            var chunks = ChunkSelection(mapFirstPos, mapSecondPos, GUIMananager.Game);
            GUIMananager.Game.UnitController.SelectedUnit.Clear();
            GUIMananager.Hud.SelectedUnitsContainer.DeleteChilds();
            GUIMananager.Hud.ActionUnitsContainer.DeleteChilds();
            foreach (var chunk in chunks)
            {
                foreach(Unit.Unit unit in chunk.Units)
                {
                    if (unit is Unit.AdvancedUnit aUnit && unit.Position.X >= mapFirstPos.X && unit.Position.X < mapSecondPos.X && unit.Position.Y >= mapFirstPos.Y && unit.Position.Y < mapSecondPos.Y)
                    {
                        GUIMananager.Game.UnitController.SelectedUnit.Add(unit);
                        GUIMananager.Hud.SelectedUnitsContainer.Add(new UnitButton(_hud, aUnit){Size = new Vector2f(25,25)});
                        foreach (var action in aUnit.Actions)
                        {
                            GUIMananager.Hud.ActionUnitsContainer.Add(new ActionButton(_hud.GUIMananager, action.Item2, aUnit){Size = new Vector2f(25,25), Texture = TextureArray.TileSet, TextureRect = TextureArray.GetTextureCoord(action.Item1).IntRect()});
                        }
                    }
                }
            }
        }

        static IEnumerable<Map.ChunkData> ChunkSelection(Vector2d firstchunk, Vector2d lastchunk, Game game)
        {
            var selectedchunks = new List<Map.ChunkData>();
            Vector2i first = Map.Map.PositionOfChunk(firstchunk);
            Vector2i last = Map.Map.PositionOfChunk(lastchunk);
            for (int i = first.X; i <= last.X; i++)
            {
                for (int y = first.Y; y <= last.Y; y++)
                {
                    var candidate = new Vector2i(i, y);

                    if (game.Map.ChunkExist(candidate) && game.Map[candidate].ChunkData != null)
                    {
                        selectedchunks.Add(game.Map[candidate].ChunkData);
                    }
                }
            }
            return selectedchunks;
        }
        public override bool MouseButtonReleased(MouseButtonEventArgs e)
        {
            if(e.Button == Mouse.Button.Left)
            {
                _isclicked = false;
            }
            return false;
        }

        public override bool MouseButtonPressed(MouseButtonEventArgs e)
        {
            if (e.Button != Mouse.Button.Left) return false;
            _isclicked = true;
            _firstpos = (Vector2f)Mouse.GetPosition(GUIMananager.Game.Window);
            _secondpos = _firstpos;
            return false;
        }
        public override void Draw(RenderTarget target, RenderStates states)
        {
            if (!_isclicked) return;
            Shape.Position = _firstpos;
            Shape.Size = _secondpos - _firstpos;
            Shape.Draw(target, states);
        }
    }
}
