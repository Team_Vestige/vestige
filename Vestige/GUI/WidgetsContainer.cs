﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace Vestige.GUI
{
    class WidgetsContainer : Widget, ICollection<Widget>
    {
        readonly Vector2f _widgetSpacement;
        readonly bool _columnOverflow;
        readonly int _sideLimit;
        Vector2i _lastWidgetPos;
        List<Widget> _widgets;
        /// <summary>
        /// Manage multiple widget and set an origin for each widget based on widgetSpacement
        /// </summary>
        /// <param name="guiMananager"></param>
        /// <param name="layer"></param>
        /// <param name="widgetSpacement">Spacement between two widget</param>
        /// <param name="columnOverflow">If there is too much widget, does will add more column ? If false it add more line.</param>
        /// <param name="sideLimit"></param>
        public WidgetsContainer(GUIMananager guiMananager, int layer, Vector2f widgetSpacement, bool columnOverflow, int sideLimit) : base(guiMananager, layer)
        {
            _widgetSpacement = widgetSpacement;
            _columnOverflow = columnOverflow;
            _sideLimit = sideLimit;
            _widgets = new List<Widget>();
        }

        public override Vector2f Position
        {
            get => base.Position;
            set
            {
                foreach (var elem in _widgets)
                {
                    elem.Position = value;
                }
                base.Position = value;
            }
        }

        public IEnumerator<Widget> GetEnumerator() => _widgets.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Widget item)
        {
            if(item == null) throw new ArgumentException("Adding a null value to the collection");
            _widgets.Add(item);
            item.Origin = Common.MultiplyVector(_lastWidgetPos, _widgetSpacement);
            item.Position = Position;
            if (_columnOverflow)
            {
                _lastWidgetPos.Y++;
                if (_lastWidgetPos.Y > _sideLimit)
                {
                    _lastWidgetPos.Y = 0;
                    _lastWidgetPos.X++;
                }
            }
            else
            {
                _lastWidgetPos.X++;
                if (_lastWidgetPos.X > _sideLimit)
                {
                    _lastWidgetPos.X = 0;
                    _lastWidgetPos.Y++;
                }
            }
            
        }

        public void Clear()
        {
            _widgets.Clear();
            _lastWidgetPos = new Vector2i();
        }

        public bool Contains(Widget item)
        {
            return _widgets.Contains(item);
        }

        public void CopyTo(Widget[] array, int arrayIndex)
        {
            _widgets.CopyTo(array, arrayIndex);
        }

        public void RealignWidgets()
        {
            var copy = new List<Widget>(_widgets);  //Now we will reinject all the element
            Clear();
            foreach (var elem in copy)              //To have all the widget realigned
            {
                Add(elem);
            }
        }

        public bool Remove(Widget item)
        {
            bool outputValue = _widgets.Remove(item);
            return outputValue;
        }

        public int Count => _widgets.Count;
        public bool IsReadOnly => ((ICollection<Widget>) _widgets).IsReadOnly;
    }
}
