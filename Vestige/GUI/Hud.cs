﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using Vestige.Unit;

namespace Vestige.GUI
{
    class Hud : Drawable
    {
        readonly Window _miniMap;
        readonly Window _inventory;
        readonly Window _selectedUnitsWindow;
        readonly Window _action;
        readonly List<Text> _texts = new List<Text>();
        public readonly WidgetsOrganizedContainer SelectedUnitsContainer;
        public readonly WidgetsOrganizedContainer ActionUnitsContainer;
        public InventoryOpener InventoryOpener;
        public GUIMananager GUIMananager;
        public Hud(GUIMananager guiMananager)
        {
            GUIMananager = guiMananager;
            InventoryOpener = new InventoryOpener(guiMananager);
            float heightWindow = guiMananager.Game.Window.DefaultView.Size.Y;
            float widthWindow = guiMananager.Game.Window.DefaultView.Size.X;
            Color colorHud = new Color(110, 110, 110, 175);

            _miniMap = new Window(guiMananager, null, "Map")
            {
                Size = new Vector2f(widthWindow / 7, (float)(heightWindow / 4.5)),
                Position = new Vector2f(widthWindow / 100, (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };

            _inventory = new Window(guiMananager, null, "Inventory")
            {
                Size = new Vector2f(widthWindow / 3, (float)(heightWindow / 4.5)),
                Position = new Vector2f(widthWindow / 6, (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };

            SelectedUnitsContainer = new WidgetsOrganizedContainer(guiMananager, 2, new Vector2f(-35, -35), false, 5)
            {
                Origin = new Vector2f(0, -40)
            };

            _selectedUnitsWindow = new Window(guiMananager, SelectedUnitsContainer, "Stats")
            {
                Size = new Vector2f(widthWindow / 4, (float)(heightWindow / 4.5)),
                Position = new Vector2f((float)(widthWindow / 1.95), (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };

            
            ActionUnitsContainer = new WidgetsOrganizedContainer(guiMananager, 2, new Vector2f(-35, -35), false, 5)
            {
                Origin = new Vector2f(0, -40)
            };
            _action = new Window(guiMananager, ActionUnitsContainer, "Actions")
            {
                Size = new Vector2f((float)(widthWindow / 4.7), (float)(heightWindow / 4.5)),
                Position = new Vector2f((float)(widthWindow / 1.29), (float)(heightWindow / 1.3)),
                FillColor = colorHud
            };
        }

        public void Draw(RenderTarget target, RenderStates states)
        {

            _miniMap.Draw(target, states);
            _inventory.Draw(target, states);
            _selectedUnitsWindow.Draw(target, states);
            _action.Draw(target, states);
            foreach (var text in _texts)
            {
                text.Draw(target, states);
            }

        }
    }
}