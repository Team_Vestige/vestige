﻿using System.Linq;
using SFML.System;
using SFML.Window;

namespace Vestige.GUI
{
    class SubButtonText : ButtonText
    {
        bool _clicked;
        Vector2f _lastMousePos;
        public WidgetsOrganizedContainer Parent;
        public SubButtonText(GUIMananager guiMananager, WidgetsOrganizedContainer parent, string text) : base(guiMananager, 0, text, Fonts.Basic, true)
        {
            Parent = parent;
        }

        public override bool MouseButtonPressed(MouseButtonEventArgs e)
        {
            if (MouseInWidget())
            {
                _clicked = true;
                _lastMousePos = (Vector2f)Mouse.GetPosition(GUIMananager.Window);
                return true;
            }
            _clicked = false;
            return false;
        }

        protected virtual void MoveSubButton(WidgetsOrganizedContainer container)
        {
            Parent.Remove(this);
            Parent = container;
            container.Add(this);
            
        }

        public override bool MouseButtonReleased(MouseButtonEventArgs e)
        {
            if (_clicked)
            {
                _clicked = false;
                foreach (var item in GUIMananager.Layers.Reverse())
                {
                    foreach (var widget in item)
                    {
                        if (widget.MouseInWidget() && widget is WidgetsOrganizedContainer container)
                        {
                            if (widget == Parent) continue;
                            MoveSubButton(container);
                            return true;
                        }
                    }
                }
                Position = Parent.Position;
            }
            return false;

        }

        public override bool MouseMoved(MouseMoveEventArgs e)
        {
            if (_clicked)
            {
                Vector2f newPos = (Vector2f)Mouse.GetPosition(GUIMananager.Window);
                Position -= _lastMousePos - newPos;
                _lastMousePos = newPos;
                return true;
            }
            return false;
        }
    }
}

