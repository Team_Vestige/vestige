﻿using SFML.Graphics;
using SFML.System;

namespace Vestige.GUI
{
    class ButtonText : Button
    {
        public readonly Text Text;
        public ButtonText(GUIMananager guiMananager, int layer, string text, Font font, bool blockPropagation) : base(guiMananager, layer, blockPropagation)
        {
            Text = new Text(text, font);
        }
        public ButtonText(GUIMananager guiMananager, string text, int layer,Font font, Vector2f size, bool blockPropagation) : base(guiMananager, layer, blockPropagation)
        {
            Text = new Text(text, font);
            Size = size;
        }
        public override Vector2f Position
        {
            get => base.Position;
            set {
                base.Position = value;
                Text.Position = value;
            }
        }

        public uint CharacterSize
        {
            get => Text.CharacterSize;
            set => Text.CharacterSize = value;
        }

        public Font Font
        {
            get => Text.Font;
            set => Text.Font = value;
        }
        public Vector2f Scale
        {
            get => Text.Scale;
            set => Text.Scale = value;
        }

        public Color Color
        {
            get => Text.Color;
            set => Text.Color = value;
        }

        public override Vector2f Origin
        {
            get => base.Origin;
            set
            {
                base.Origin = value;
                Text.Origin = value;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            Text?.Dispose();
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            base.Draw(target, states);
            Text.Draw(target, states);
        }
    }
}
