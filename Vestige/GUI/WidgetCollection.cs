﻿using System;
using System.Collections;
using System.Collections.Generic;
using SFML.Graphics;
using SFML.System;

namespace Vestige.GUI
{
    class WidgetCollection: Widget, ICollection<Widget>
    {
        List<Widget> _widgets;

        public WidgetCollection(GUIMananager guiMananager, int layer): base(guiMananager, layer)
        {
            _widgets = new List<Widget>();
        }
        public override Vector2f Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                foreach (var elem in _widgets)
                {
                    elem.Position = value;
                }
            }
        }

        public override Vector2f Origin
        {
            get => base.Origin;
            set
            {
                var diff = Origin - value;
                base.Origin = value;
                foreach (var elem in _widgets)
                {
                    elem.Origin -= diff;
                }
            }

        }

        public IEnumerator<Widget> GetEnumerator() => _widgets.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual void Add(Widget item)
        {
            if (item == null) throw new ArgumentException("Adding a null value to the collection");
            _widgets.Add(item);
        }

        public virtual void Clear()
        {
            _widgets.Clear();
        }

        public bool Contains(Widget item)
        {
            return _widgets.Contains(item);
        }

        public void CopyTo(Widget[] array, int arrayIndex)
        {
            _widgets.CopyTo(array, arrayIndex);
        }

        public virtual bool Remove(Widget item)
        {
            return _widgets.Remove(item);
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
        }

        public int Count => _widgets.Count;
        public bool IsReadOnly => ((ICollection<Widget>)_widgets).IsReadOnly;
    }
}
