﻿using SFML.Window;
using System;

namespace Vestige.GUI
{
    class Button : Widget
    {
        public event EventHandler OnOverEnter;  //When Mouse On Button
        public event EventHandler OnOverExit;   //When Mouse Not On Button
        public event EventHandler OnClick;  //When Mouse Click Button
        public event EventHandler OnClickRelease;   //When Mouse Release Click Button
        bool _overing;
        readonly bool _blockPropagation;

        public Button(GUIMananager guiMananager, int layer, bool blockPropagation) : base(guiMananager, layer)
        {
            GUIMananager = guiMananager;
            _blockPropagation = blockPropagation;
        }
        
        public override bool MouseButtonPressed(MouseButtonEventArgs e)
        {
            if (MouseInWidget())
            {
                OnClick?.Invoke(this, e);
                if (_blockPropagation) return true;
            }
           return false;
        }

        public override bool MouseButtonReleased(MouseButtonEventArgs e)
        {
            if (MouseInWidget())
            {
                OnClickRelease?.Invoke(this, e);
                return _blockPropagation;
            }
            return false;
        }

        public override bool MouseMoved(MouseMoveEventArgs e)
        {
            if (MouseInWidget())
            {
                if (_overing) return false;
                OnOverEnter?.Invoke(this, e);
                _overing = true;
                return false;
            }
            if (!_overing)return false;
            OnOverExit?.Invoke(this, e);
            _overing = false;
            return false;
        }
    }
}