﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace Vestige.GUI
{
    class InventoryWindow : MovableWindow
    {
        public readonly Unit.AdvancedUnit Unit;
        Button _buttonRemove;
        public InventoryWindow(GUIMananager guiMananager, Unit.AdvancedUnit unit) : base(guiMananager, new InventoryContainer(guiMananager, 1, new Vector2f(-35, -35), false, 5, unit), "Inventory - " + unit.Id)
        {
            _buttonRemove = new Button(guiMananager, 0, true)
            {
                Size = new Vector2f(10, 10),
                FillColor = Color.Red
            };
            _buttonRemove.OnClick += Button_OnClick;
            InventoryContainer woc = (InventoryContainer)Widget;

            Unit = unit;
            Position = guiMananager.Window.DefaultView.Center;
            Size = Widget.Size + new Vector2f(50, 50);
            Widget.Origin = new Vector2f(-25, -50);
            FillColor = new Color(125, 125, 125, 125);

            woc.Resizing += Resizing;
            Position = Position;

        }

        public override Vector2f Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                _buttonRemove.Position = new Vector2f(Position.X + Size.X - 20, Position.Y);
            }
        }

        void Button_OnClick(object sender, EventArgs e)
        {
            if (ChildWidget is InventoryContainer a)
            {
                a.DeleteChilds();
            }
            _buttonRemove.Remove();
            GUIMananager.Hud.InventoryOpener.Remove(this);
        }

        void Resizing(object sender, EventArgs eventArgs)
        {
            try
            {


                Size = Widget.Size + new Vector2f(50, 50);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
