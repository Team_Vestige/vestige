﻿using Vestige.Unit;

namespace Vestige
{
    static class Extensions
    {
        public static bool IsBlocking(this TileType type)
        {
            return type != TileType.Path && type != TileType.Stone;
        }

        public static bool CanMove(this State type)
        {
            return type != State.Attacking && type != State.HoldPosition;
        }

        public static bool CanAttack(this State type)
        {
            return type == State.Waiting || type == State.Attacking || type == State.MovingAttacking;
        }


        //Units Sprites starts at 60
        public static int GetUnitId(this Unit.Unit.UnitType type)
        {
            return (int)type + 60;
        }

        //NeededItems Sprites starts at 160
        public static int GetItemId(this Item type)
        {
            return (int)type + 160;
        }
    }
    
}
