﻿using SFML.System;
using System;

namespace Vestige
{
    // ReSharper disable once InconsistentNaming
    public struct Vector2d
    {
        public double X;
        public double Y;
        public Vector2d(double x, double y)
        {
            X = x;
            Y = y;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var k = (Vector2d)obj;
            return Math.Abs(k.X - X) < float.Epsilon && Math.Abs(k.Y - Y) < float.Epsilon;

        }
        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator - overload ; returns the opposite of a vector
        /// </summary>
        /// <param name="v">Vector to negate</param>
        /// <returns>-v</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2d operator -(Vector2d v)
        {
            return new Vector2d(-v.X, -v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator - overload ; subtracts two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 - v2</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2d operator -(Vector2d v1, Vector2d v2)
        {
            return new Vector2d(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static Vector2d operator -(Vector2f v1, Vector2d v2)
        {
            return new Vector2d(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static Vector2d operator -(Vector2d v1, Vector2f v2)
        {
            return new Vector2d(v1.X - v2.X, v1.Y - v2.Y);
        }
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator + overload ; add two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 + v2</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2d operator +(Vector2d v1, Vector2d v2)
        {
            return new Vector2d(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Vector2d operator +(Vector2f v1, Vector2d v2)
        {
            return new Vector2d(v1.X + v2.X, v1.Y + v2.Y);
        }
        public static Vector2d operator +(Vector2d v1, Vector2f v2)
        {
            return new Vector2d(v1.X + v2.X, v1.Y + v2.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator * overload ; multiply a vector by a scalar value
        /// </summary>
        /// <param name="v">Vector</param>
        /// <param name="x">Scalar value</param>
        /// <returns>v * x</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2d operator *(Vector2d v, double x)
        {
            return new Vector2d(v.X * x, v.Y * x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator * overload ; multiply a scalar value by a vector
        /// </summary>
        /// <param name="x">Scalar value</param>
        /// <param name="v">Vector</param>
        /// <returns>x * v</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2d operator *(double x, Vector2d v)
        {
            return new Vector2d(v.X * x, v.Y * x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator / overload ; divide a vector by a scalar value
        /// </summary>
        /// <param name="v">Vector</param>
        /// <param name="x">Scalar value</param>
        /// <returns>v / x</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2d operator /(Vector2d v, double x)
        {
            return new Vector2d(v.X / x, v.Y / x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator == overload ; check vector equality
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 == v2</returns>
        ////////////////////////////////////////////////////////////
        public static bool operator ==(Vector2d v1, Vector2d v2)
        {
            return v1.Equals(v2);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Operator != overload ; check vector inequality
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>v1 != v2</returns>
        ////////////////////////////////////////////////////////////
        public static bool operator !=(Vector2d v1, Vector2d v2)
        {
            return !v1.Equals(v2);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Provide a string describing the object
        /// </summary>
        /// <returns>String description of the object</returns>
        ////////////////////////////////////////////////////////////
        public override string ToString()
        {
            return "[Vector2d]" +
                   " X(" + X + ")" +
                   " Y(" + Y + ")";
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2f(Vector2d v)
        {
            return new Vector2f((float)v.X,(float) v.Y);
        }
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        /*public static explicit operator Vector2l(Vector2d v)
        {
            return new Vector2l((long)Math.Floor(v.X), (long) Math.Floor(v.Y));
        }*/

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2d(Vector2f v)
        {
            return new Vector2d(v.X, v.Y);
        }
        public static explicit operator Vector2d(Vector2u v)
        {
            return new Vector2d(v.X, v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2d(Vector2i v)
        {
            return new Vector2d((double)v.X, (double)v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2d(Vector2l v)
        {
            return new Vector2d((double)v.X, (double)v.Y);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Explicit casting to another vector type
        /// </summary>
        /// <param name="v">Vector being casted</param>
        /// <returns>Casting result</returns>
        ////////////////////////////////////////////////////////////
        public static explicit operator Vector2i(Vector2d v)
        {
            return new Vector2i((int)v.X, (int)v.Y);
        }
    }
}
