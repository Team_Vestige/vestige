using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML.Audio;
using System.Threading;

namespace Vestige
{
    class Game : IDisposable
    {
        public List<AnimatedSprite> AnimatedSprite;
        public readonly RenderWindow Window;
        public readonly View CameraView;
        public readonly Map.Map Map;
        readonly GameDebug _debug;
        public readonly UnitController UnitController;
        public MenuType GameState;
        public event EventHandler<Time> Update;
        readonly Clock _frameTime;
        public Time DayTime;
        public int NumberOfDay;
        public List<Unit.AdvancedUnit> Humans;
        Sound _sound;
        public readonly GUI.GUIMananager GUIMananager;
        public readonly GameEvents GameEvents;
        readonly View _guiView;
        readonly Map.ChunkManager _chunkManager;
        DayAndNight _dayAndNight;
        public readonly Time TimeInAday = Time.FromSeconds(180);
        RectangleShape _gameOver;
        RandomSpawn _spawn;
        public Random rand = new Random();


        public RecipesList RecipesList;
        public DrawBuilding DrawBuilding;

        Game(RenderWindow window, bool newGame)
        {
            AnimatedSprite = new List<AnimatedSprite>();
            #region Events subscriding
            GameEvents = new GameEvents(window);
            new Camera().Attach(this);
            window.Closed += Quit;
            Update += Game_Update;
            #endregion
            #region Variable Initialiation
            Window = window;
            GameState = MenuType.Game;
            _frameTime = new Clock();
            _debug = new GameDebug(this);
            DayTime = new Time();
            RecipesList = new RecipesList();
            #endregion
            #region View Initialiation
            /**
             * View Init
             **/
            CameraView = new View(new FloatRect(new Vector2f(0, 0),
                new Vector2f(VideoMode.FullscreenModes[1].Width, VideoMode.FullscreenModes[1].Height)));
            CameraView.Zoom(0.1f);
            CameraView.Center = new Vector2f(25, 21);
            _guiView = Window.DefaultView;
            #endregion
            #region Map Initialisation
            /**
             * Map Init
             **/
            Map = new Map.Map(this, 12); //Init Map
            _chunkManager = new Map.ChunkManager(this);


            #endregion
            #region GUI Initialiation
            /**
             * GUI Init
             **/
            GUIMananager = new GUI.GUIMananager(this);
            _dayAndNight = new DayAndNight(this);
            #endregion
            UnitController = new UnitController(this);
            if (newGame)
            {
                GamePlayInit();
            }
        }

        void GamePlayInit()
        {
            /**
             * GamePlay Init
             **/
            _chunkManager.PopulateSpawn();
            List<Unit.AdvancedUnit> toDelete = new List<Unit.AdvancedUnit>();
            foreach (var chunk in Map)
            {
                foreach (var unit in chunk.Value.ChunkData.Units)
                {
                    if (unit.Team == Unit.Team.Human && unit is Unit.AdvancedUnit aUnit)
                    {
                        toDelete.Add(aUnit);
                    }
                }
            }
            foreach (var advancedUnit in toDelete)
            {
                advancedUnit.Damage(float.MaxValue);
            }
            DayTime = new Time();
            Update += Game_Update;
            Vector2l coo = new Vector2l(26,8);
            Map.Chunk chunky = Map.GetChunkOfPoint(coo);
            chunky[chunky.GetRelativeCoordinate(coo)] = Tile.Create(TileType.StoneOre, 0);

            new Unit.Wall(this, new Vector2l(25, 17));
            new Unit.Wall(this, new Vector2l(26, 17));

            var tourelle = new Unit.AutoTurret(this, new Vector2l(25, 15));
            tourelle.AddInInventory(Item.Ammo, 83);
            tourelle = new Unit.AutoTurret(this, new Vector2l(26, 15));
            tourelle.AddInInventory(Item.Ammo, 83);
            Humans = new List<Unit.AdvancedUnit>();
            new Unit.RegularHuman(this, new Vector2f(21, 8));
            new Unit.RegularHuman(this, new Vector2f(21, 8));
            new Unit.RegularHuman(this, new Vector2f(22, 8));
            new Unit.RegularHuman(this, new Vector2f(23, 8));
            new Unit.RegularHuman(this, new Vector2f(24, 8));
            var toto = new Unit.CraftTable(this, new Vector2l(27, 10));
            toto.AddInInventory(Item.Stone, 25);
            foreach (var unit in Humans)
            {
                unit.Died += Unit_Died;
            }

            

            
            ThreadPool.QueueUserWorkItem(threadContext =>
            {
                SoundBuffer buffer = new SoundBuffer("resources/sound.ogg");
                _sound = new Sound(buffer);
                if (GameState == MenuType.Game) _sound.Play();
            });
            _spawn = new RandomSpawn(this);

        }

        void Quit(object sender, EventArgs args)
        {
            GameState = MenuType.Quit;
        }

        void Unit_Died(object sender, EventArgs e)
        {
            Humans.Remove((Unit.AdvancedUnit)sender);
            if (Humans.Count == 0)
            {
                Console.WriteLine("You loose");
                Texture img = new Texture("resources/gameover.jpg");
                _gameOver = new RectangleShape()
                {
                    Texture = img,
                    Size = new Vector2f(Window.Size.X,Window.Size.Y),
                    FillColor = Color.White
                };
            }
        }

        void Game_Update(object sender, Time e)
        {



            DayTime += e;
            if (DayTime.AsSeconds() < 40 || DayTime.AsSeconds() > 120)
                if ((int)Math.Floor(DayTime.AsSeconds()) % 25 == 0) _spawn.AlienSpawn();
            if (!(DayTime > TimeInAday)) return;
            NumberOfDay++;
            DayTime -= TimeInAday;
        }

        void GameLoop()
        {
            /**
            * Input management
            **/

            Window.DispatchEvents();
            Update?.Invoke(this, _frameTime.Restart());

            /**
             * Drawing 
             **/
            Window.Clear();
            Window.SetView(CameraView);
            Window.Draw(Map);
            Window.Draw(UnitController);
            foreach (var item in AnimatedSprite)
            {
                Window.Draw(item);
            }
           if(DrawBuilding!=null) Window.Draw(DrawBuilding);
            if (_debug.Enabled) _debug.DrawCamera();

            Window.SetView(_guiView);
            //View for hud
            //Game Over

            if (Humans.Count == 0 && AnimatedSprite.Count==0)
                Window.Draw(_gameOver);
            Window.Draw(_dayAndNight);
            if (_debug.Enabled) Window.Draw(_debug);
            Window.Draw(GUIMananager);
            //Game Over
            Window.Display();
        }



        public static MenuType RunGame(RenderWindow window, bool newGame)
        {
            Game game = new Game(window, newGame);
            while (game.GameState == MenuType.Game)
            {
                game.GameLoop();
            }
            game._chunkManager.RemoveAllChunk();
            game.Dispose();
            return game.GameState;
        }


        public void Dispose()
        {
            _chunkManager.StopProcess();
            CameraView?.Dispose();
            _debug?.Dispose();
            _frameTime?.Dispose();
            _sound?.Dispose();
            GUIMananager?.Dispose();
            _guiView?.Dispose();
            Map?.Dispose();
            GameDebug.Instance = null;
            Window.Closed -= Quit;
            GameEvents.Unsuscribe();
        }
    }
}
            //Game Over

            