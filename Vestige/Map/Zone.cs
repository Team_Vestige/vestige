﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Vestige.Map
{
    class Zone
    {
#if DEBUG
        static readonly Random ColorGenerator = new Random();
        public readonly Color Color;
#endif
        public readonly int Id;
        public Zone(Map map)
        {
            Id = map.GetNewZoneId(this);
#if DEBUG
            var bytesColor = new byte[3];
            ColorGenerator.NextBytes(bytesColor);
            Color = new Color(bytesColor[0], bytesColor[1], bytesColor[2]);
#endif
        }
        public Zone(int id)
        {
            Id = id;
#if DEBUG
            var bytesColor = new byte[3];
            ColorGenerator.NextBytes(bytesColor);
            Color = new Color(bytesColor[0], bytesColor[1], bytesColor[2]);
#endif
        }
    }
}
