﻿using System;
using System.Collections.Generic;
using SFML.System;
using SFML.Graphics;
using SFML.Window;

namespace Vestige.Map
{
    class OldMap : Drawable, IDisposable
    {
        readonly Dictionary<Vector2i, ChunkData> _map;
        /// <summary>
        /// Map of the game, contain chunks that contains the tile of the terrain.
        /// </summary>
        readonly Game _game;

        public int ZoneId;
        public Map(Game game)
        {
            ZoneId = 0;
            _game = game;
            _map = new Dictionary<Vector2i, ChunkData>();
        }

        
        /// <summary>
        /// Enumerator of the chunk of the map
        /// </summary>
        /// <returns>Enumerator</returns>
        public IEnumerator<KeyValuePair<Vector2i, ChunkData>> GetEnumerator()
        {
            return _map.GetEnumerator();
        }

        /// <summary>
        /// Remove chunk from the given position
        /// </summary>
        /// <param name="coordinate"></param>
        public void RemoveChunk(Vector2i coordinate)
        {
            _map.Remove(coordinate);
        }
        public Zone GetZoneOfPoint(Vector2d point, int unitHitboxId)
        {
            if (!(GetChunkOfPoint(point) is ZoneData chunk)) return null;
            Vector2f pointOnChunk = chunk.GetRelativeCoordinate(point);
            Vector2i tileOnChunk = new Vector2i((int) Math.Floor(pointOnChunk.X), (int) Math.Floor(pointOnChunk.Y));
            return chunk.Zones[unitHitboxId][tileOnChunk.X, tileOnChunk.Y];

        }
        public Zone GetZoneOfPoint(Vector2l point, int unitHitboxId)
        {
            if (!(GetChunkOfPoint(point) is ZoneData chunk)) return null;
            Vector2i pointOnChunk = chunk.GetRelativeCoordinate(point);
            return chunk.Zones[unitHitboxId][pointOnChunk.X, pointOnChunk.Y];
        }

        
       
        
        
        /// <summary>
        /// Return the chunk where is the point.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public ChunkData PreChunkOfPoint(Vector2l point)
        {
            return this[PositionOfChunk(point)];
        }
        /// <summary>
        /// Return the chunk where is the point.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public ChunkData PreChunkOfPoint(Vector2d point)
        {
            return this[PositionOfChunk(point)];
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            Vector2d topLeft = (Vector2d)_game.CameraView.Center - _game.CameraView.Size;
            Vector2i topLeftChunk = PositionOfChunk(topLeft);
            Vector2d botRight = (Vector2d)_game.CameraView.Center + _game.CameraView.Size;
            Vector2i botRightChunk = PositionOfChunk(botRight);
            for (int x = topLeftChunk.X; x < botRightChunk.X + 1; x++) //Parse all PreChunk present on screen + 1 outside to generate chunk offscreen
            {
                for (int y = topLeftChunk.Y; y < botRightChunk.Y + 1; y++)
                {
                    Vector2i coo = new Vector2i(x,y);
                    if (ChunkExist(coo) && _map[coo] is ZoneData chunk)
                    {
                        _map[coo].TileVertexs.Draw(target, states);
                        if (Keyboard.IsKeyPressed(Keyboard.Key.A))
                        {
                            for (int i = 0; i < ChunkData.TileSize; i++)
                            {
                                for (int j = 0; j < ChunkData.TileSize; j++)
                                {
                                    if (chunk.Zones[0][i, j] != null)
                                    {
                                        new RectangleShape() { Size = new Vector2f(1, 1), Position = (Vector2f)coo * ChunkData.TileSize + new Vector2f(i, j), FillColor = chunk.Zones[0][i, j].Color }.Draw(target, states);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            _game.UnitController.Draw(target, states);
            for (int x = topLeftChunk.X; x < botRightChunk.X + 1; x++) //Parse all PreChunk present on screen + 1 outside to generate chunk offscreen
            {
                for (int y = topLeftChunk.Y; y < botRightChunk.Y + 1; y++)
                {
                    Vector2i coo = new Vector2i(x, y);
                    if (ChunkExist(coo) && _map[coo] is ZoneData chunk)
                    {
                       chunk.Draw(target, states);
                    }
                }
            }
        }
        public void Dispose()
        {
            foreach (var elem in _map)
            {
                elem.Value?.Dispose();
            }
        }
    }
}
