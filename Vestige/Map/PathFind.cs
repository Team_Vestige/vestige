﻿using System;
using System.Collections.Generic;
using System.Linq;
using SFML.System;

namespace Vestige.Map
{
    static class PathFind
    {
        class PathPoint
        {
            public readonly float Cost;
            public readonly Vector2d Coo;
            public PathPoint(SimplePoint point, float cost)
            {
                Cost = cost;
                Coo = point.Coo;
            }
        }
        public struct SimplePoint : IEquatable<SimplePoint>
        {
            public Vector2d Coo;
            public readonly float Distance;
            public SimplePoint(Vector2d coo, Vector2d end)
            {
                Coo = coo;
                Distance = (float)Common.DistancePow2(coo, end);
            }
            public bool Equals(SimplePoint otherPoint) => Math.Abs(otherPoint.Coo.X - Coo.X) < float.Epsilon && Math.Abs(otherPoint.Coo.Y - Coo.Y) < float.Epsilon;
        }
        static void AddProximityPoints(Vector2d start, Vector2d arrival, ICollection<SimplePoint> toProcess, Map.TileDataManager<PathPoint> dataMap)
        {
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if (Math.Abs(x) + Math.Abs(y) != 1) continue;
                    var candidate = new SimplePoint(new Vector2d(Math.Floor(start.X), Math.Floor(start.Y)) + new Vector2d(x, y), new Vector2d(Math.Floor(arrival.X), Math.Floor(arrival.Y)));
                    if (!toProcess.Contains(candidate) && dataMap[Common.FloorVector2l(start + new Vector2d(x, y))] == null)
                    {
                        toProcess.Add(candidate);
                    }
                }
            }
        }

        static float SmallestCostNearPoint(Vector2l coo, Map.TileDataManager<PathPoint> dataMap)
        {
            float mini = float.MaxValue;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (dataMap[coo + new Vector2l(i, j)] == null || Math.Abs(i) + Math.Abs(j) != 1) continue;
                    var temp = dataMap[coo + new Vector2l(i, j)].Cost + 1;
                    if (temp < mini)
                    {
                        mini = temp;
                    }
                }
            }

            return mini;
        }

        static void ProcessMinimalPoint(ICollection<SimplePoint> toProcess, Map.TileDataManager<PathPoint> dataMap, int unitHitboxId, Map map, Vector2d end)
        {
            SimplePoint mini = toProcess.First();
            foreach (SimplePoint point in toProcess)
            {
                if (mini.Distance > point.Distance)
                {
                    mini = point;
                }
            }

            Chunk chunkCandidate = map[Map.PositionOfChunk(mini.Coo)];
            Vector2f relativCoo = chunkCandidate.GetRelativeCoordinate(mini.Coo);
            if (chunkCandidate.ChunkData != null && //Is a chunk
                chunkCandidate.ZoneData.UnitHoldingSize((Vector2i)relativCoo) >= Unit.MovableUnit.Hitboxes[unitHitboxId])
            {
                dataMap[Common.FloorVector2l(mini.Coo)] = new PathPoint(mini, SmallestCostNearPoint(Common.FloorVector2l(mini.Coo), dataMap));
                AddProximityPoints(mini.Coo, end, toProcess, dataMap);
            }
            toProcess.Remove(mini);
        }

        static PathPoint FindPreviousPoint(PathPoint point, Map.TileDataManager<PathPoint> dataMap)
        {
            PathPoint output = null;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (Math.Abs(i) + Math.Abs(j) != 1) continue;
                    var testPoint = dataMap[new Vector2l((long)Math.Floor(point.Coo.X), (long)Math.Floor(point.Coo.Y)) + new Vector2l(i, j)];
                    if (testPoint == null || !(testPoint.Cost < point.Cost)) continue;
                    if (output == null)
                    {
                        output = testPoint;
                    }
                    else if (output.Cost > testPoint.Cost)
                    {
                        output = testPoint;
                    }
                }
            }
            if (output == null)
            {
                for (int i = -1; i < 2; i++)
                {
                    for (int j = -1; j < 2; j++)
                    {
                        if (Math.Abs(i) + Math.Abs(j) != 1) continue;
                        var testPoint = dataMap[new Vector2l((long)Math.Floor(point.Coo.X), (long)Math.Floor(point.Coo.Y)) + new Vector2l(i, j)];
                        if (testPoint == null || !(testPoint.Cost < point.Cost)) continue;
                        if (output == null)
                        {
                            output = testPoint;
                        }
                        else if (output.Cost > testPoint.Cost)
                        {
                            output = testPoint;
                        }
                    }
                }
                throw new Exception("Didnt found way back");
            }
            return output;
        }
       
        static IEnumerable<Vector2l> LineY(long x0, long y0, long x1, long y1)
        {
            var output = new List<Vector2l>();
            if (x0 < x1)
            {
                long temp = x0;
                x0 = x1;
                x1 = temp;
                temp = y0;
                y0 = y1;
                y1 = temp;
            }
            double slope = Common.Slope(x0, y0, x1, y1);
            double total = Math.Abs(slope);
            while (x0 != x1 || y0 != y1)
            {
                if (total >= 1)
                {
                    if (total-Math.Floor(total)  < 0.0001f)
                    {
                        output.Add(new Vector2l(x0-1, y0));
                    }
                    y0 -= Math.Sign(slope);
                    output.Add(new Vector2l(x0, y0));
                    total -= 1;
                    continue;
                }
                total += Math.Abs(slope);
                x0 -= 1;
                output.Add(new Vector2l(x0, y0));
            }
            return output;
        }

        static IEnumerable<Vector2l> LineX(long x0, long y0, long x1, long y1)
        {
            var output = new List<Vector2l>();
            if (x0 > x1)
            {
                long temp = x0;
                x0 = x1;
                x1 = temp;

                temp = y0;
                y0 = y1;
                y1 = temp;
            }
            double slope = Common.Slope(x0, y0, x1, y1);
            double total = Math.Abs(slope);
            output.Add(new Vector2l(x0, y0));
            while (x0 != x1 || y0 != y1)
            {
                if (total >= 1)
                {
                    if (total - Math.Floor(total) < 0.0001f)
                    {
                        output.Add(new Vector2l(x0 + 1, y0));
                    }
                    y0 += Math.Sign(slope);
                    output.Add(new Vector2l(x0, y0));
                    total -= 1;
                    continue;
                }
                total += Math.Abs(slope);
                x0 += 1;
                output.Add(new Vector2l(x0, y0));
            }
            return output;
        }
        
        public static IEnumerable<Vector2l> GetPointsOnLines(long x, long y, long x1, long y1)
        {
            if (x == x1) //Straight line management
            {
                var output = new List<Vector2l> {new Vector2l(x, y)};
                while (y != y1)
                {
                    y += Math.Sign(y1 - y);
                    output.Add(new Vector2l(x, y));
                }
                output.Add(new Vector2l(x1, y1));
                return output;
            }
            if (y == y1)
            {
                var output = new List<Vector2l> { new Vector2l(x, y) };
                while (x != x1)
                {
                    x += Math.Sign(x1 - x);
                    output.Add(new Vector2l(x, y));
                }
                output.Add(new Vector2l(x1, y1));
                return output;
            }
            var output1 = LineX(x, y, x1, y1);
            var output2 = LineY(x1, y1, x, y);
            return output1.Union(output2);
        }

        

        static bool DirectPathPossible(Map map, int hitBoxId, Vector2d pointA, Vector2d pointB)
        {
            Chunk firstZoneChunk = map[Map.PositionOfChunk(pointA)];
            var firstZoneRelative = firstZoneChunk.GetRelativeCoordinate(pointA);
            Zone firstZone = firstZoneChunk.ZoneData[(int)Math.Floor(firstZoneRelative.X), (int)Math.Floor(firstZoneRelative.Y), hitBoxId]; //map.GetZoneOfPoint(pointA, hitBoxId);
            var pointsOnPath = GetPointsOnLines((long)Math.Floor(pointA.X), (long)Math.Floor(pointA.Y), (long)Math.Floor(pointB.X), (long)Math.Floor(pointB.Y));
            return pointsOnPath.All(tilePos => map.GetZoneOfPoint(tilePos, hitBoxId) == firstZone);
        }

        static List<Vector2d> Optimize(Map map, int hitboxId, List<Vector2d> checkPoints, Vector2d checkPoint)
        {
            if (checkPoint == checkPoints.First() || checkPoint == checkPoints.Last()) return checkPoints;
            bool found = false;
            Vector2d previous = new Vector2d();
            
            foreach (var point in checkPoints)
            {
                
                if (found)
                {
                    var next = point;
                    if (DirectPathPossible(map, hitboxId, previous, next))
                    {
                        checkPoints.Remove(checkPoint);
                        return checkPoints;
                    }
                    return checkPoints;
                    
                }
                if (point == checkPoint)
                {
                    found = true;
                } 
                if (!found) previous = point;
            }

            throw new InvalidOperationException("We shouldnt be there");
        }

        static List<Vector2d> OptimizeAll(Map map, int hitboxId, List<Vector2d> checkPoints)
        {
            bool optimized;
            var optimizing = new List<Vector2d>(checkPoints);
            do
            {
                optimized = false;
                foreach (Vector2d point in checkPoints)
                {
                    optimizing = Optimize(map, hitboxId, optimizing, point);
                    if (optimizing.Count != checkPoints.Count)
                    {
                        optimized = true;
                        break;
                    }
                }
                checkPoints = new List<Vector2d>(optimizing);
            } while (optimized);
            return checkPoints;
        }
        public static List<Vector2d> PathFinding(Vector2d start, Vector2d end, int hitBoxId, Map map)
        {
            //Check if the pathfind is possible
            if (map.GetChunkOfPoint(start) == null || map.GetChunkOfPoint(end) == null)
            {
                Console.WriteLine("Path not in a chunk");
                return new List<Vector2d>();
            }
            //Check if the pathfind is in the same area
            Chunk startingChunk = map.GetChunkOfPoint(start);
            Chunk endingChunk = map.GetChunkOfPoint(end);
            if (endingChunk.ZoneData == null)
            {
                Console.WriteLine("Path not in a chunk");
                return new List<Vector2d>();
            }
            Vector2i startingTile = startingChunk.GetRelativeCoordinate(Common.FloorVector2l(start));
            Zone startingZone = startingChunk.ZoneData[startingTile.X, startingTile.Y, hitBoxId];
            Vector2i endingTile = endingChunk.GetRelativeCoordinate(Common.FloorVector2l(end));
            Zone endingZone = endingChunk.ZoneData[endingTile.X, endingTile.Y, hitBoxId];
            if (endingZone != startingZone)
            {
                return new List<Vector2d>();
            }
            //Starting the pathfinding
            var dataMap = new Map.TileDataManager<PathPoint>
            {
                [(long)Math.Floor(start.X), (long)Math.Floor(start.Y)] = new PathPoint(new SimplePoint(start, end), 0)
            };
            var toProcess = new List<SimplePoint>();
            AddProximityPoints(start, end, toProcess, dataMap);

            while (toProcess.Count > 0 && dataMap[Common.FloorVector2l(end)] == null)
            {
                ProcessMinimalPoint(toProcess, dataMap, hitBoxId, map, end);
            }
            if (toProcess.Count == 0)
            {
                return new List<Vector2d>();
            }
            PathPoint previousPoint = dataMap[Common.FloorVector2l(end)];
            var output = new List<Vector2d>
            {
                end
            };
            while (previousPoint != dataMap[Common.FloorVector2l(start)])
            {
                output.Add(previousPoint.Coo);
                previousPoint = FindPreviousPoint(previousPoint, dataMap);
            }
            output.Add(start);
            output = OptimizeAll(map, hitBoxId, output);
            

            return output;
        }
    }
}
