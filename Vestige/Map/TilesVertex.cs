﻿using System;
using SFML.System;
using SFML.Graphics;


namespace Vestige.Map
{
    
    class TilesVertex : Drawable, IDisposable
    {
        const uint VertexArraySize = ChunkData.TileSize * ChunkData.TileSize * 4;
        readonly VertexArray _vertexArray;
        static readonly Vector2f[] Direction = {new Vector2f(0, 0), new Vector2f(1, 0), new Vector2f(1, 1), new Vector2f(0, 1)};
        public TilesVertex(ChunkData chunk, Vector2i screenPosition)
        {
            _vertexArray = new VertexArray(PrimitiveType.Quads, VertexArraySize);
            for (var x = 0; x < ChunkData.TileSize; x++) //TODO Is Double loop faster or getting one number to two coordinate ?
            {
                for (var y = 0; y < ChunkData.TileSize; y++)
                {
                    var tile = TextureArray.GetTextureCoord((int)chunk[(x << ChunkData.TileBits) | y].Type);
                    AddTileVertices((Vector2f)(new Vector2i(x, y)+ screenPosition), tile);
                }
            }
        }
        /// <summary>
        /// Add the 4 vertices of a tile to the vertex array
        /// </summary>
        /// <param name="position">Position of the tile</param>
        /// <param name="textureCoords">Coordinates of the texture</param>
        void AddTileVertices(Vector2f position, QuadCoords textureCoords )
        {
            _vertexArray.Append(
                new Vertex(Direction[0] + position,   //Coords
                    textureCoords.TopLeft + new Vector2f(0.0075f, 0.0075f)));

            _vertexArray.Append(
                new Vertex(Direction[1] + position,
                   textureCoords.TopRight + new Vector2f(-0.0075f, 0.0075f)));

            _vertexArray.Append(
                new Vertex(Direction[2] + position,
                    textureCoords.BottomRight + new Vector2f(-0.0075f, -0.0075f)));

            _vertexArray.Append(
                new Vertex(Direction[3] + position,
                    textureCoords.BottomLeft + new Vector2f(0.0075f, -0.0075f)));
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Texture = TextureArray.TileSet;
            target.Draw(_vertexArray, states);
        }

        public void Dispose()
        {
            _vertexArray?.Dispose();
        }
    }
}
