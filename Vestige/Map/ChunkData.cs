﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Vestige.Map
{
    class ChunkData : IDisposable
    {
        protected const int BinaryVersion = 0;
        public const int TileBits = 4;
        public const int TileSize = 1 << TileBits;
        public const int TileMask = TileSize - 1;
        readonly Tile[] _tiles;
        TilesVertex _tileVertexs;
        Chunk _chunk;
        Game _game;
        /// <summary>
        /// PreChunk is all the preChunk data, but not preprocessed for the pathfinder
        /// </summary>
        /// <param name="tiles">Tiles of the PreChunk</param>
        public ChunkData(TileType[,] tiles, Chunk chunk, Map map,Game game)
        {
            _game = game;
            Position = chunk.Position;
            Map = map;
            _chunk = chunk;
            Units = new List<Unit.Unit>();
            // crée l'unité en fonction de son type 
            _tiles = new Tile[TileSize * TileSize];
            for (int x = 0; x < tiles.GetLength(0); x++)
            {
                for (int y = 0; y < tiles.GetLength(1); y++)
                {
                    _tiles[(x << TileBits) | y] = Tile.Create(tiles[x, y], 0);
                }
            }
            UpdateTileVertex();
            CheckInstance();
        }

        #region Unit
        public List<Unit.Unit> Units { get; set; }

        public void AddUnit(Unit.Unit unit)
        {
            Units.Add(unit);
        }
        public void RemoveUnit(Unit.Unit unit)
        {
            Debug.Assert(Units.Contains(unit));
            Units.Remove(unit);
        }

        #endregion



        protected ChunkData(ChunkData preChunk, Chunk chunk)
        {
            _chunk = chunk;
            Position = preChunk.Position;
            Map = preChunk.Map;
            Position = preChunk.Position;
            _tiles = preChunk._tiles;
            TileVertexs = preChunk.TileVertexs;
            CheckInstance();
            Units = preChunk.Units;
        }

        void UpdateTileVertex()
        {
            TileVertexs = new TilesVertex(this, Position * TileSize);
        }
        #region Serial
        protected ChunkData(BinaryReader r, Game game, Chunk chunk)
        {
            _chunk = chunk;
            int version = r.ReadInt32();
            if (version != BinaryVersion) throw new InvalidDataException("readed version "+version+" should be "+BinaryVersion);
            Map = game.Map;
            Position = new Vector2i(r.ReadInt32(), r.ReadInt32());
            _tiles = new Tile[TileSize * TileSize];
            for (int i = 0; i < TileSize * TileSize; i++)
            {
                _tiles[i] = Tile.Create((TileType)r.ReadInt32(), r.ReadByte());
            }
            TileVertexs = new TilesVertex(this, Position * TileSize);
            CheckInstance();

            Units = new List<Unit.Unit>();
            int numberOfUnits = r.ReadInt32();
            for (int i = 0; i < numberOfUnits; i++)
            {
                Units.Add(Unit.Unit.Read(r, game, _chunk));
            }

            var value = r.ReadString();
            if (value != "endPreChunk")
            {
                throw new InvalidDataException("invalid sentinel, readed" + value + " pos:" + r.BaseStream.Position +
                                               " length" + r.BaseStream.Length);
            }
        }

        void WriteTo(BinaryWriter w)
        {
            w.Write(0);
            w.Write(Position.X);
            w.Write(Position.Y);
            foreach (var tile in _tiles)
            {
                w.Write((int)tile.Type);
                w.Write(tile.Damage);
            }
            w.Write(Units.Count);
            foreach (var unit in Units)
            {
                unit.WriteTo(w);
            }
            w.Write("endChunk");
        }



        public void Save(string filePath)
        {
            using (var stream = File.OpenWrite(filePath))
            using (var w = new BinaryWriter(stream))
            {
                WriteTo(w);
            }
        }

        public static ChunkData Load(string filePath, Game game, Chunk chunk)
        {
            using (var stream = File.OpenRead(filePath))
            using (var r = new BinaryReader(stream))
            {
                return new ChunkData(r, game, chunk);
            }
        }
        #endregion
        void CheckInstance()
        {
            if (Map == null) throw new Exception("Map is null");
            if (TileVertexs == null) throw new Exception("TileVertexs is null");
            if (Position == null) throw new Exception("Position is null");
            if (_tiles == null) throw new Exception("Tiles is null");

        }
        public Vector2i Position { get; }

        protected Map Map { get; }

        /// <summary>
        /// Return the TileType at a given coo
        /// </summary>
        /// <param name="x"></param>
        /// <returns>A TileType</returns>
        public Tile this[int x]
        {
            get => _tiles[x];
            set
            {
                _tiles[x] = value;
                UpdateTileVertex();
            }
        }

        Tile this[int x, int y]
        {
            get
            {
                CheckCoords(x, y);
                return this[(x << TileBits) | y];
            }
            set
            {
                CheckCoords(x, y);
                this[(x << TileBits) | y] = value;
            }
        }

        Tile this[Vector2i coo]
        {
            get => this[coo.X, coo.Y];
            set => this[coo.X, coo.Y] = value;
        }

        /// <summary>
        /// Check if the local coordinates are in the preChunk.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        static void CheckCoords(int x, int y)
        {
            if (y >= TileSize) throw new ArgumentOutOfRangeException();
            if (x < 0) throw new ArgumentOutOfRangeException(nameof(x));
            if (y < 0) throw new ArgumentOutOfRangeException(nameof(y));
        }

        /// <summary>
        /// Get the vertex array of the preChunk.
        /// </summary>
        public TilesVertex TileVertexs
        {
            get => _tileVertexs;
            private set => _tileVertexs = value;
        }



        

        
        public static ChunkData LoadPreChunkOrGenerate(Chunk chunk, Game game, Perlin perlin)
        {
            string fileName = Common.Vector2iToFileName(chunk.Position);
            return File.Exists(fileName) ? Load(fileName, game, chunk) : Generate(chunk, game.Map,game);
        }
        public static ChunkData Generate(Chunk chunk, Map map,Game game)
        {
            var chunkMap = new TileType[TileSize, TileSize];
            for (int y = 0; y < TileSize; y++)
            {
                for (int x = 0; x < TileSize; x++)
                {
                    float bruit = map.Perlin.ParametablePerlinNoise((x + chunk.Position.X * TileSize), (y + chunk.Position.Y * TileSize), 0.1f, Perlin.InterpolantType.Wide);
                    if (bruit > 0.5)
                    {
                        chunkMap[x, y] = TileType.Wall;
                    }
                    else
                    {
                        chunkMap[x, y] = TileType.Path;
                    }
                }
            }
            return new ChunkData(chunkMap, chunk, map, game);
        }


        public virtual void Dispose()
        {
            _tileVertexs?.Dispose();
            foreach (var unit in Units)
            {
                unit?.Dispose();
            }
        }
    }
}
