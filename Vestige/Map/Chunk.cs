﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SFML.System;

namespace Vestige.Map
{

    class Chunk
    {
        
        public ChunkData ChunkData;
        public ZoneData ZoneData;
        public Vector2i Position;
        public Chunk(Vector2i position)
        {
            Position = position;
        }

        #region Getter/Setter

        public Tile this[int x]
        {
            get => ChunkData[x];
            set
            {
                ChunkData[x] = value;
                ZoneData.UpdateZones();
            }
        }
        public Tile this[int x, int y]
        {
            get => this[(x << ChunkData.TileBits) | y];
            set => this[(x << ChunkData.TileBits) | y] = value;
        }

        public Tile this[Vector2i coo]
        {
            get => this[coo.X, coo.Y];
            set => this[coo.X, coo.Y] = value;
        }
        public IEnumerable<Zone> ContainZones(int unitHoldingSize)
        {

            return ZoneData.Zones.Select(zones => zones[unitHoldingSize]).Distinct();
        }
        #endregion
        #region Serialization
        public void LoadOrGenerateChunkData(Game game)
        {
            string fileName = Common.Vector2iToFileName(Position);
            ChunkData = File.Exists(fileName) ? ChunkData.Load(fileName, game, this) : ChunkData.Generate(this,game.Map,game);
        }

        public bool TryLoadChunk(Game game)
        {
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    var coo = Position + new Vector2i(i, j);
                    if (game.Map[coo] == null) continue;
                    if (game.Map[coo].ChunkData != null) continue;
                    if (!game.Map[coo].TryLoadChunkData(game)) return false;
                }
            }
            ZoneData = new ZoneData(game.Map, this);
            ZoneData.UpdateZones();
            ZoneData.UpdateZones();
            //RandomSpawn.ChanceOfHumanSpawn(this, game.rand, game);
            return true;
        }

        bool TryLoadChunkData(Game game)
        {
            string fileName = Common.Vector2iToFileName(Position);
            if (File.Exists(fileName))
            {
                ChunkData = ChunkData.Load(fileName, game, this);
                return true;
            }
            return false;
        }



        public void GenerateAroundAndActivateZone(Game game)
        {
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    var candidatePos = new Vector2i(x, y) + Position;
                    if (game.Map[candidatePos] == null)
                    {
                        game.Map.AddChunk(new Chunk(candidatePos));
                    }

                    var candidate = game.Map[candidatePos];
                    if (candidate.ChunkData == null)
                    {
                        candidate.LoadOrGenerateChunkData(game);
                    }
                }
            }
            ZoneData = new ZoneData(game.Map, this);

            ZoneData.UpdateZones();
            ZoneData.UpdateZones();
            RandomSpawn.ChanceOfHumanSpawn(this, game.rand, game);
            // RandomSpawn.ChanceOfHumanSpawn(this, game.rand, game);
        }
        #endregion


        #region PositionFunctions
        Vector2f PositionInPixels()
        {
            return (Vector2f)Position * ChunkData.TileSize;
        }

        public Vector2f GetAbsoluteCoordinate(Vector2f relativeCoordinate)
        {
            return relativeCoordinate + PositionInPixels();
        }
        public Vector2l GetAbsoluteCoordinate(Vector2i relativeCoordinate)
        {
            return (Vector2l)relativeCoordinate + (Vector2l)PositionInPixels();
        }

        public Vector2f GetRelativeCoordinate(Vector2f absoluteCoordinate)
        {
            Vector2f output = absoluteCoordinate - PositionInPixels();
            if (output.X >= 16)
            {
                output.X -= 16;
            }
            if (output.Y >= 16)
            {
                output.Y -= 16;
            }
            return output;
        }

        public Vector2i GetRelativeCoordinate(Vector2l absoluteCoordinate)
        {
            Vector2i output = (absoluteCoordinate - (Vector2l)PositionInPixels()).Vector2i();
            if (output.X >= 16)
            {
                output.X -= 16;
            }
            if (output.Y >= 16)
            {
                output.Y -= 16;
            }
            return output;
        }

        public Vector2f GetRelativeCoordinate(Vector2d absoluteCoordinate)
        {
            Vector2f output = (Vector2f)(absoluteCoordinate - PositionInPixels());//TODO REDO

            if (output.X >= 16)
            {
                output.X -= 16;
            }
            if (output.Y >= 16)
            {
                output.Y -= 16;
            }
            return output;
        }


        #endregion
    }
}
