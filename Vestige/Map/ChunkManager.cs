﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SFML.Window;

namespace Vestige.Map
{
    class ChunkManager
    {
        readonly Game _game;
        Task _chunkAsyncGenerator;
        bool _running;
        public ChunkManager(Game game)
        {
            _game = game;
            game.Update += Game_Update;
            LoadAroundCamera();
            RemoveChunkOutOfCamera();
            _running = true;
        }

        public void StopProcess()
        {
            _running = false;
            //_chunkAsyncGenerator.Wait();
        }

        void Game_Update(object sender, Time e)
        {
            if (_chunkAsyncGenerator == null || _chunkAsyncGenerator.IsCompleted)
            {
                _chunkAsyncGenerator = Task.Run(() => { LoadAroundCamera(); RemoveChunkOutOfCamera(); });
                
            }
        }

        public void PopulateSpawn()
        {
            for (int x = -5; x <= 5; x++) 
            {
                for (int y = -5; y <= 5; y++)
                {
                    Vector2i coo = new Vector2i(x, y);
                    var chunk = new Chunk(coo);
                    _game.Map.AddChunk(chunk);
                    if(chunk.ChunkData == null)chunk.LoadOrGenerateChunkData(_game);
                }
            }
            for (int x = -5; x <= 5; x++)
            {
                for (int y = -5; y <= 5; y++)
                {
                    _game.Map[new Vector2i(x, y)].GenerateAroundAndActivateZone(_game);
                    _game.Map[new Vector2i(x,y)].ZoneData.UpdateZones();
                }
            }
        }

        void LoadAroundCamera()
        {
            if (!_running) return;
            Vector2i topLeftChunk = FindTopLeft(_game.CameraView);
            Vector2i bottomRightChunk = FindBotRight(_game.CameraView);
            Vector2i middleChunk = FindMiddle(_game.CameraView);

            var toLoad = new Dictionary<Vector2i, float>();
            for (int x = topLeftChunk.X; x <= bottomRightChunk.X; x++) //Parse all PreChunk present on screen + 1 outside to generate chunk offscreen
            {
                for (int y = topLeftChunk.Y; y <= bottomRightChunk.Y; y++)
                {
                    Vector2i coo = new Vector2i(x, y);
                    if (_game.Map[coo] != null && _game.Map[coo].ChunkData == null)
                    {
                        toLoad.Add(coo, Common.DistancePow2(coo, middleChunk));
                    }
                }
            }
            var items = from pair in toLoad
                        orderby pair.Value
                        select pair;
            toLoad.Values.ToList().Sort();
            int i = 0;
            foreach (var pair in items)
            {
                if (!_running) return;
                i++;
                if (i == 10)
                {
                    break;
                }
                _game.Map[pair.Key]?.TryLoadChunk(_game);
              

            }
        }

        public void RemoveAllChunk()
        {
            _running = false;
            _chunkAsyncGenerator.Wait();
            /*foreach (var chunk in _game.Map)
            {
                chunk.Value.Save(Common.Vector2iToFileName(chunk.Key));
                _game.Map.RemoveChunk(chunk.Key);
            }*/ //WHYYYY
        }
        void RemoveChunkOutOfCamera()
        {
            /*
            View view = _game.CameraView;
            var listOfChunkOutOfScreen = new List<Vector2i>();
            Vector2i topLeftChunk = FindTopLeft(view);
            Vector2i botRightChunk = FindBotRight(view);


            foreach (var values in _game.Map)
            {
                if (values.Key.X > botRightChunk.X || values.Key.X < topLeftChunk.X || values.Key.Y < topLeftChunk.Y || values.Key.Y > botRightChunk.Y)
                {
                    listOfChunkOutOfScreen.Add(values.Key);
                }
            }
            
            foreach (Vector2i value in listOfChunkOutOfScreen)
            {
                _game.Map[value].Save(Common.Vector2iToFileName(value));
                _game.Map.RemoveChunk(value);
            }*/
        }
        static Vector2i FindTopLeft(View view)
        {
            Vector2f topLeft = view.Center - view.Size / 2;
            Vector2i topLeftChunk = Map.PositionOfChunk((Vector2d)topLeft);
            return topLeftChunk;
        }
        static Vector2i FindBotRight(View view)
        {
            Vector2f botRight = view.Center + view.Size / 2;
            Vector2i botRightChunk = Map.PositionOfChunk((Vector2d)botRight);
            return botRightChunk;
        }
        static Vector2i FindMiddle(View view)
        {
            return Map.PositionOfChunk((Vector2d)view.Center);
        }
    }
}

