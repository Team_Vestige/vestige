﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Vestige.Map
{
    class Map : Drawable, IDisposable
    {
        public readonly Perlin Perlin;
        public readonly List<Zone> Zones;
        Dictionary<Vector2i, Chunk> _chunks;
        Game _game;
        public Map(Game game, int seed)
        {
            _game = game;
            Perlin = new Perlin(seed);
            Zones = new List<Zone>();
            _chunks = new Dictionary<Vector2i, Chunk>();//Todo serialize that later
        }

        public Map(CustomBinaryReader r, Game game)
        {
            _game = game;
            Zones = new List<Zone>();
            int count = r.ReadInt32();
            for (var i = 0; i < count; count++)
            {
                Zones.Add(new Zone(i));
            }

            if (r.ReadString() != "MapEnd") throw new InvalidDataException("No sentinel");
        }

        #region GetterSetter


        /// <summary>
        /// Enumerator of the chunk of the map
        /// </summary>
        /// <returns>Enumerator</returns>
        public IEnumerator<KeyValuePair<Vector2i, Chunk>> GetEnumerator()
        {
            return _chunks.GetEnumerator();
        }
        #endregion
        /// <summary>
        /// Chunks of the map.
        /// </summary>
        /// <param name="x">Axe X of the chunk</param>
        /// <param name="y">Axe Y of the chunk</param>
        /// <returns>An instancied PreChunk or null if it doesn't exist.</returns>
        public Chunk this[int x, int y]
        {
            get
            {
                _chunks.TryGetValue(new Vector2i(x, y), out Chunk c);
                return c;
            }
            set => _chunks[new Vector2i(x, y)] = value;
        }

        public Chunk this[Vector2i coo]
        {
            get => this[coo.X, coo.Y];
            set => this[coo.X, coo.Y] = value;
        }



        public int GetNewZoneId(Zone zone)
        {
            Zones.Add(zone);
            return Zones.Count;
        }
        public void WriteContent(CustomBinaryWriter w)
        {
            w.Write(Zones.Count);
            foreach (var item in Zones)
            {
                w.Write(item);
            }
            w.Write("MapEnd");

        }
        /// <summary>
        /// Add a PreChunk to the map at a coordinate.
        /// </summary>
        /// <param name="chunk">PreChunk to add, this PreChunk shouldn't be already in the map</param>
        public void AddChunk(Chunk chunk)
        {
            if (_chunks.ContainsKey(chunk.Position))
            {
                throw new ArgumentException("A chunk already exist at these coordinate.");
            }
            if (_chunks.ContainsValue(chunk))
            {
                throw new ArgumentException("This chunk already exist in this map.");
            }
            _chunks.Add(chunk.Position, chunk);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            Vector2d topLeft = (Vector2d)_game.CameraView.Center - _game.CameraView.Size;
            Vector2i topLeftChunk = PositionOfChunk(topLeft);
            Vector2d botRight = (Vector2d)_game.CameraView.Center + _game.CameraView.Size;
            Vector2i botRightChunk = PositionOfChunk(botRight);
            for (int x = topLeftChunk.X; x < botRightChunk.X + 1; x++)
            {
                for (int y = topLeftChunk.Y; y < botRightChunk.Y + 1; y++)
                {
                    Vector2i coo = new Vector2i(x, y);
                    if (_chunks.ContainsKey(coo) && _chunks[coo].ChunkData!=null && _chunks[coo].ZoneData != null)
                    {
                        _chunks[coo].ChunkData.TileVertexs.Draw(target, states);
#if DEBUG
                        if (Keyboard.IsKeyPressed(Keyboard.Key.A))
                        {
                            for (int i = 0; i < ChunkData.TileSize; i++)
                            {
                                for (int j = 0; j < ChunkData.TileSize; j++)
                                {
                                    if (_chunks[coo].ZoneData[i, j, 0] != null)
                                    {
                                        new RectangleShape() { Size = new Vector2f(1, 1), Position = (Vector2f)coo * ChunkData.TileSize + new Vector2f(i, j), FillColor = _chunks[coo].ZoneData[i, j, 0].Color }.Draw(target, states);
                                    }
                                }
                            }
                        }
#endif
                    }
                }
            }
            for (int x = topLeftChunk.X; x < botRightChunk.X + 1; x++)
            {
                for (int y = topLeftChunk.Y; y < botRightChunk.Y + 1; y++)
                {
                    Vector2i coo = new Vector2i(x, y);
                    if (_chunks.ContainsKey(coo))
                    {
                        foreach (Unit.Unit unit in _chunks[coo].ChunkData.Units)
                        {
                            unit.Draw(target, states);
                        }
                    }
                }
            }
        }

#region PositionFunctions
        public bool ChunkExist(Vector2i chunkCoo)
        {
            return _chunks.ContainsKey(chunkCoo);
        }
        public Chunk GetChunkOfPoint(Vector2l coo) => this[PositionOfChunk(coo)];
        public Chunk GetChunkOfPoint(Vector2d coo) => this[PositionOfChunk(coo)];
        public void SetZoneOfPoint(Vector2d pointA, int hitBoxId, Zone zone)
        {
            Chunk firstZoneChunk = this[Map.PositionOfChunk(pointA)];
            var firstZoneRelative = firstZoneChunk.GetRelativeCoordinate(pointA);
            firstZoneChunk.ZoneData[(int)Math.Floor(firstZoneRelative.X), (int)Math.Floor(firstZoneRelative.Y), hitBoxId] = zone; //map.GetZoneOfPoint(pointA, hitBoxId);
        }
        public Zone GetZoneOfPoint(Vector2d pointA, int hitBoxId)
        {
            Chunk firstZoneChunk = this[Map.PositionOfChunk(pointA)];
            var firstZoneRelative = firstZoneChunk.GetRelativeCoordinate(pointA);
            return firstZoneChunk.ZoneData[(int)Math.Floor(firstZoneRelative.X), (int)Math.Floor(firstZoneRelative.Y), hitBoxId];
        }
        public Zone GetZoneOfPoint(Vector2l pointA, int hitBoxId)
        {
            Chunk firstZoneChunk = this[Map.PositionOfChunk(pointA)];
            var firstZoneRelative = firstZoneChunk.GetRelativeCoordinate(pointA);
            return firstZoneChunk.ZoneData[firstZoneRelative.X, firstZoneRelative.Y, hitBoxId];
        }
        public static Vector2d WindowPositionToMapPositon(Vector2f point, View view, RenderWindow window)
        {
            return view.Center - view.Size / 2 + Common.MultiplyVector(view.Size, Common.Divide(point, window.Size));
        }
        /// <summary>
        ///  Return the Vector2i of the PreChunk where is the point. 
        /// </summary>
        /// <param name="point">A point in a PreChunk</param>
        /// <returns>Coordinate of the PreChunk</returns>
        public static Vector2i PositionOfChunk(Vector2d point)
        {
            return new Vector2i((int)Math.Floor(point.X) >> ChunkData.TileBits, (int)Math.Floor(point.Y) >> ChunkData.TileBits);
        }

        /// <summary>
        ///  Return the Vector2i of the PreChunk where is the point. 
        /// </summary>
        /// <param name="point">A point in a PreChunk</param>
        /// <returns>Coordinate of the PreChunk</returns>
        public static Vector2i PositionOfChunk(Vector2l point)
        {
            return new Vector2i((int)(point.X >> ChunkData.TileBits), (int)(point.Y >> ChunkData.TileBits));
        }
#endregion


        /// <summary>
        /// Store data on a tile on the map.
        /// </summary>
        /// <typeparam name="T">Any objet to store data on the tile.</typeparam>
        public class TileDataManager<T> where T : class
        {
            readonly Dictionary<Vector2l, T> _values;
            public TileDataManager()
            {
                _values = new Dictionary<Vector2l, T>();
            }

            public IEnumerator<T> GetEnumerator()
            {
                return _values.Values.GetEnumerator();
            }

            public T this[long x, long y]
            {
                get
                {
                    _values.TryGetValue(new Vector2l(x, y), out T c);
                    return c;
                }
                set
                {
                    Vector2l candidate = new Vector2l(x, y);
                    if (!_values.ContainsKey(candidate))
                    {
                        _values.Add(candidate, value);
                        return;
                    }
                    _values[candidate] = value;
                }
            }
            public T this[Vector2l point]
            {
                get
                {
                    _values.TryGetValue(new Vector2l(point.X, point.Y), out T c);
                    return c;
                }
                set
                {
                    Vector2l candidate = new Vector2l(point.X, point.Y);
                    if (!_values.ContainsKey(candidate))
                    {
                        _values.Add(candidate, value);
                        return;
                    }
                    _values[candidate] = value;
                }
            }
        }
        public void Dispose()
        {
            foreach (var elem in _chunks)
            {
                elem.Value.ChunkData?.Dispose();
            }
        }
    }
}

