﻿using System;
using System.Collections.Generic;
using SFML.System;
using System.Linq;

namespace Vestige.Map
{
    class ZoneData
    {
        float[,] _unitHoldingSize;
        readonly Chunk _chunk;
        public List<Zone[]> Zones { get; private set; }
        Map _map;

        /// <summary>
        /// PreChunk of tiles
        /// </summary>
        public ZoneData(Map map, Chunk chunk)
        {
            _map = map;
            _chunk = chunk;
            UpdateZones();
        }

        #region TileSize
        /// <summary>
        /// Take a 46*46 byte array and return the byte array of middle the chunk
        /// </summary>
        /// <param name="input">46*46 byte array</param>
        /// <returns>byte array of middle the chunk</returns>
        static float[,] SelectMiddleChunk(float[,] input)
        {
            var output = new float[ChunkData.TileSize, ChunkData.TileSize];
            for (int y = 0; y < ChunkData.TileSize; y++)
            {
                for (int x = 0; x < ChunkData.TileSize; x++)
                {
                    output[x, y] = input[x + ChunkData.TileSize, y + ChunkData.TileSize];
                }
            }
            return output;
        }
        /// <summary>
        /// Calculate the minimum distance between a 'tile' and an unit
        /// </summary>
        /// <param name="tile"></param>
        /// <param name="tileHoldingUnit"></param>
        /// <returns></returns>
        static float GetMinimalDistancePow2(Vector2i tile, Vector2i tileHoldingUnit)
        {

            if (tile.X == tileHoldingUnit.X)//Check if they are on the same line and return the result if they are
            {
                return Math.Abs(tile.Y - tileHoldingUnit.Y) * Math.Abs(tile.Y - tileHoldingUnit.Y);
            }
            if (tile.Y == tileHoldingUnit.Y)
            {
                return Math.Abs(tile.X - tileHoldingUnit.X) * Math.Abs(tile.X - tileHoldingUnit.X);
            }

            float min = float.MaxValue;

            Vector2i tileCasted = tile;

            for (int y = 0; y <= 1; y += 1)
            {
                for (int x = 0; x <= 1; x += 1)
                {
                    Vector2i temp = new Vector2i(x, y);
                    if (temp + tileCasted == tileHoldingUnit) continue;
                    float candidate = Common.DistancePow2(temp + tileCasted, tileHoldingUnit);
                    if (min > candidate)
                    {
                        min = candidate;
                    }
                }
            }
            return min;
        }
        public float UnitHoldingSize(Vector2i coo) => _unitHoldingSize[coo.X, coo.Y];
        /// <summary>
        /// Calculate the distance with the closest blocking tile
        /// </summary>
        /// <param name="blocking"></param>
        /// <param name="tileHoldingUnit"></param>
        /// <returns></returns>
        static float GetUnitHoldingSizePow2(IEnumerable<Vector2i> blocking, Vector2i tileHoldingUnit)
        {
            float min = blocking.Select(tile => GetMinimalDistancePow2(tile, tileHoldingUnit)).Concat(new[] { float.MaxValue }).Min();
            return (float)Math.Sqrt(min);
        }

        /// <summary>
        /// Fill the size holding of a tile near the blocking tiles.
        /// </summary>
        /// <param name="tiles"></param>
        /// <param name="output"></param>
        static void FastFillSize(Tile[,] tiles, float[,] output)
        {
            for (int y = ChunkData.TileSize; y < ChunkData.TileSize * 2; y++)
            {
                for (int x = ChunkData.TileSize; x < ChunkData.TileSize * 2; x++)
                {
                    if (!tiles[x, y].Type.IsBlocking()) continue;
                    for (int j = -1; j < 2; j++)
                    {
                        for (int i = -1; i < 2; i++)
                        {
                            if (Math.Abs(i) + Math.Abs(j) != 2 && //Not the tiles in diagonals
                                x + i >= 0 && x + i < tiles.GetLength(0) && y + j >= 0 && y + j < tiles.GetLength(1)//is in the array
                                && !tiles[x + i, y + j].Type.IsBlocking() && Math.Abs(output[x + i, y + j]) < float.Epsilon)//Is not a blocking tile or not already setted
                            {
                                output[x + i, y + j] = 1;
                            }
                        }
                    }
                }
            }
            const float diagonalValue = 1.41421354f;
            for (int y = ChunkData.TileSize; y < ChunkData.TileSize * 2; y++)
            {
                for (int x = ChunkData.TileSize; x < ChunkData.TileSize * 2; x++)
                {
                    if (!tiles[x, y].Type.IsBlocking()) continue;
                    for (int j = -1; j < 2; j++)
                    {
                        for (int i = -1; i < 2; i++)
                        {
                            if (Math.Abs(i) + Math.Abs(j) == 2 &&               //Are in diagonals
                                x + i >= 0 && x + i < tiles.GetLength(0) && y + j >= 0 && y + j < tiles.GetLength(1)//is in the array
                                && !tiles[x + i, y + j].Type.IsBlocking() &&    //Is not a blocking tile
                                Math.Abs(output[x + i, y + j]) < float.Epsilon) //not already setted
                            {
                                output[x + i, y + j] = diagonalValue;
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Take a 2d array of tiles and calculate the max size each tile can hold
        /// Please mind that you need nearest chunk, because obstacle of these chunk must be taken in account for the unit holding size.
        /// If you notice problem with huge units you need to take the 5*5 nearest chunks.
        /// </summary>
        /// <returns>An 2d array of bytes</returns>
        float[,] FillSizeValuePow2()
        {
            var tiles = NearTiles();
            var output = new float[tiles.GetLength(0), tiles.GetLength(1)];
            var blocking = new List<Vector2i>();
            //Indexing all the walls
            for (int y = 0; y < tiles.GetLength(0); y++)
            {
                for (int x = 0; x < tiles.GetLength(0); x++)
                {
                    if (!tiles[x, y].Type.IsBlocking()) continue;
                    bool isSurrounded = true;
                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            if (Math.Abs(x) + Math.Abs(y) != 1) continue;
                            if (x + i < 16 || x + i >= 32 || y + j < 16 || y + j >= 32) continue;
                            if (!tiles[x + i, y + j].Type.IsBlocking())
                            {
                                isSurrounded = false;
                            }
                        }
                    }
                    if (!isSurrounded)
                    {
                        blocking.Add(new Vector2i(x, y));
                    }
                }
            }
            FastFillSize(tiles, output);
            for (int y = ChunkData.TileSize; y < ChunkData.TileSize * 2; y++)
            {
                for (int x = ChunkData.TileSize; x < ChunkData.TileSize * 2; x++)
                {
                    if (!tiles[x, y].Type.IsBlocking() && Math.Abs(output[x, y]) < float.Epsilon)
                    {
                        output[x, y] = GetUnitHoldingSizePow2(blocking, new Vector2i(x, y));
                    }
                }
            }
            return SelectMiddleChunk(output);
        }
        Tile[,] NearTiles()
        {
            var output = new Tile[ChunkData.TileSize * 3, ChunkData.TileSize * 3];
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    for (int j = 0; j < ChunkData.TileSize; j++)
                    {
                        for (int i = 0; i < ChunkData.TileSize; i++)
                        {
                            output[x * ChunkData.TileSize + i, y * ChunkData.TileSize + j]
                                = _map[new Vector2i(x - 1, y - 1) + _chunk.Position][i, j];
                        }
                    }
                }
            }
            return output;
        }

        #endregion
        #region GetterSetter
        /// <summary>
        /// Return the TileType at a given coo
        /// </summary>
        /// <param name="x"></param>
        /// <param name="unitHoldingSize"></param>
        /// <returns>A TileType</returns>
        public Zone this[int x, int unitHoldingSize]
        {
            get => Zones[unitHoldingSize][x];
            set => Zones[unitHoldingSize][x] = value;
        }

        public Zone this[int x, int y, int unitHoldingSize]
        {
            get
            {
                CheckCoords(x, y);
                return this[(x << ChunkData.TileBits) | y, unitHoldingSize];
            }
            set
            {
                CheckCoords(x, y);
                this[(x << ChunkData.TileBits) | y, unitHoldingSize] = value;
            }
        }
        static void CheckCoords(int x, int y)
        {
            if (y >= ChunkData.TileSize) throw new ArgumentOutOfRangeException();
            if (x < 0 || x>=16) throw new ArgumentOutOfRangeException(nameof(x));
            if (y < 0 || y>=16) throw new ArgumentOutOfRangeException(nameof(y));
        }


        #endregion
        #region Zone
        public Zone this[Vector2i coo, int unitHoldingSize]
        {
            get => this[coo.X, coo.Y, unitHoldingSize];
            set
            {
                if (_chunk.Position == new Vector2i(1, 1) && coo.X == 9 && coo.Y == 0)
                {

                }
                this[coo.X, coo.Y, unitHoldingSize] = value;
            }
        }

        /// <summary>
        /// Check if the local coordinates are in the preChunk.
        /// </summary>
       


        void FillZone(Vector2l absolutePos, Zone zone, int unitSizeIndex, bool ignoreFirstTile = false)
        {
            if(zone == null) throw new FuckingException();
            var newChunkPos = Map.PositionOfChunk(absolutePos);
            Chunk chunkToModify = _map[newChunkPos];
            ZoneData zoneToModify = chunkToModify?.ZoneData;
            if (zoneToModify == null) return;
            var relativeCoo = chunkToModify.GetRelativeCoordinate(absolutePos);
            if (zoneToModify._unitHoldingSize[relativeCoo.X, relativeCoo.Y] < Unit.MovableUnit.Hitboxes[unitSizeIndex]) return;
            if (zoneToModify[relativeCoo, unitSizeIndex] == zone && !ignoreFirstTile) return;
            zoneToModify[relativeCoo, unitSizeIndex] = zone;
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if(Math.Abs(x)+Math.Abs(y) != 1) continue;
                    FillZone(absolutePos + new Vector2l(x, y), zone, unitSizeIndex);
                }
            }
        }
        public void UpdateZones()
        {
            _unitHoldingSize = FillSizeValuePow2();
            Zones?.Clear();
            Zones = new List<Zone[]>();
            for (int i = 0; i < Unit.MovableUnit.Hitboxes.Length; i++)
            {
                Zones.Add(new Zone[ChunkData.TileSize * ChunkData.TileSize]);
                ZonifyChunk(i);
            }
        }

        /// <summary>
        /// Create zone within the chunk
        /// </summary>
        /// <returns></returns>
        void ZonifyChunk(int unitSizeIndex)
        {

            // Fill all wall with max value
            for (int y = -1; y < ChunkData.TileSize + 1; y++)
            {
                for (int x = -1; x < ChunkData.TileSize + 1; x++)
                {
                    var absoluteCoo = _chunk.GetAbsoluteCoordinate(new Vector2i(x, y));
                    var chunk = _map[Map.PositionOfChunk(absoluteCoo)];
                    var relativeCoo = chunk.GetRelativeCoordinate(absoluteCoo);
                    if (chunk.ZoneData?[relativeCoo, unitSizeIndex] == null || chunk.ZoneData._unitHoldingSize[relativeCoo.X, relativeCoo.Y]<Unit.MovableUnit.Hitboxes[unitSizeIndex]) continue;
                    FillZone(absoluteCoo, chunk.ZoneData[relativeCoo, unitSizeIndex], unitSizeIndex, true);
                }
            }
            // Fill the array with a zone num
            for (int y = 0; y < ChunkData.TileSize; y++)
            {
                for (int x = 0; x < ChunkData.TileSize; x++)
                {
                    if (this[x, y, unitSizeIndex] != null) continue;
                    if (_unitHoldingSize[x, y] < Unit.MovableUnit.Hitboxes[unitSizeIndex]) continue;
                    FillZone(_chunk.GetAbsoluteCoordinate(new Vector2i(x, y)), new Zone(_map), unitSizeIndex);
                }
            }
        }


        #endregion
    }
}