﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Vestige.Map
{
    class Perlin
    {
        const int Size = 256;
        const int Mask = Size - 1;
        static readonly int[] Perm;
        static Perlin()
        {
            Perm = new int[Size];
            for(int i = 0; i< Size;i++)
            {
                Perm[i] = i;
            }
        }


        readonly float[] _gradsX = new float[Size];
        readonly float[] _gradsY = new float[Size];
        public Perlin(int seed)
        {
            Random rand = new Random(seed);
            for (int index = 0; index < Size; ++index)
            {
                int other = rand.Next() % (index + 1);
                if (index > other)
                {
                    Perm[index] = Perm[other];
                }

                Perm[other] = index;
                _gradsX[index] = (float)Math.Cos(2.0f * Math.PI * index / Size);
                _gradsY[index] = (float)Math.Sin(2.0f * Math.PI * index / Size);
            }
        }

        static decimal Cubic(decimal t)
        {
            t = Math.Abs(t);
            return t >= 1.0m ? 0.0m : 1.0m -
                (3.0m - 2.0m * t) * t * t;
        }

        static decimal Wide(decimal t)
        {
            t = 1.0m - 0.25m * t * t;
            return t < 0.0m ? 0.0m :
                (4.0m * t - 3.0m) * t * t * t * t;
        }

        static decimal Quintic(decimal t)
        {
            t = Math.Abs(t);
            return ((6.0m * t - 15.0m) * t + 10.0m) * t * t * t;
        }

        static decimal Surflet(decimal x, decimal y, float gradX, float gradY, InterpolantType interpolant)
        {
            switch(interpolant)
            {
                case InterpolantType.Cubic:
                    return Cubic(x) * Cubic(y) * ((decimal)gradX * x + (decimal)gradY * y);
                case InterpolantType.Wide:
                    return Wide(x) * Wide(y) * ((decimal)gradX * x + (decimal)gradY * y);
                case InterpolantType.Quintic:
                    return Quintic(x) * Quintic(y) * ((decimal)gradX * x + (decimal)gradY * y);
                default:
                    throw new NotImplementedException();
            }
            
        }
        public float ParametablePerlinNoise(decimal x, decimal y,float frequency, InterpolantType interpolant = InterpolantType.Cubic)
        {
            float total = 0;
            const float amplitude = 1;
            total += 1f * Noise(x * (decimal)frequency, y * (decimal)frequency, interpolant) * amplitude;
            total += 0.5f * Noise(x * (decimal)frequency*1.75m,  y * (decimal)frequency *1.75m, interpolant) * amplitude;
            return total/1.5f;
        }

        float Noise(decimal x, decimal y, InterpolantType interpolant)
        {
            float result = 0.0f;
            int cellX = (int)Math.Floor(x);
            int cellY = (int)Math.Floor(y);
            for (int gridY = cellY; gridY <= cellY + 1; ++gridY)
            {
                for (int gridX = cellX; gridX <= cellX + 1; ++gridX)
                {
                    int hash = Perm[(Perm[gridX & Mask] + gridY) & Mask];
                    result += (float)Surflet(x - gridX, y - gridY,
                                       _gradsX[hash], _gradsY[hash], interpolant);
                }
            }
            return (result+1f)/2f;
        }

        public enum InterpolantType
        {
            Cubic,
            Wide,
            Quintic 
        }
    }
}
