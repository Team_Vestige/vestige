﻿using SFML.Graphics;
using SFML.Window;
using System;
using OpenTK;
using OpenTK.Graphics;
using Vestige.GUI;

namespace Vestige
{
    public enum MenuType
    {
        Game,
        Menu,
        Load,
        Option,
        Quit
    }

    static class Program
    {
        static void Main()
        {
            MenuType choice = MenuType.Menu;
            Console.WriteLine("FullScreen Mode ? y/n");

            var answere = Console.ReadLine()?.ToLower();
            bool windowed = !(answere == "yes" || answere == "y");
            //Change true for window, false for fullscreen
            var window = windowed ? new RenderWindow(VideoMode.FullscreenModes[0], "Vestige", Styles.Titlebar | Styles.Close) : new RenderWindow(VideoMode.FullscreenModes[0], "Vestige", Styles.Fullscreen);
            window.SetActive(true);
            Console.WriteLine("Vendor: " + GL.GetString(GL.PlatformInfo.Renderer));
            while (choice != MenuType.Quit)
            {
                choice = ComputeMainMenuChoice(choice, window);
            }
            window.Close();
        }

        static MenuType ComputeMainMenuChoice(MenuType choice, RenderWindow window)
        {
            MenuType newChoice;
            switch (choice)
            {
                case MenuType.Game:
                    newChoice = Game.RunGame(window, true);
                    break;
                case MenuType.Menu:
                    newChoice = Menu.RunMenu(new GUIMananager(window));
                    break;
                case MenuType.Load:
                    newChoice = Game.RunGame(window, false);
                    break;
                case MenuType.Option:
                    throw new NotImplementedException("todo");
                default:
                    throw new NotImplementedException("todo");
            }

            return newChoice;
        }


    }
}
