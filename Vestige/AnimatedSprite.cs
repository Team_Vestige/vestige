﻿using SFML.Graphics;

namespace Vestige
{
    class AnimatedSprite : Drawable
    {
        Animation _animation;
        public Shape Shape;

        public AnimatedSprite(Animation animation, Shape shape)
        {
            _animation = animation;
            Shape = shape;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            Shape.TextureRect = _animation.GetIntRect();
            target.Draw(Shape);
        }
    }
}