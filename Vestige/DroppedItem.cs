﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige.Unit
{
    class DroppedItem : Unit
    {

        Item Item { get; set; }

        public DroppedItem(Game game, UnitType id, Vector2f position, float maxHealth, Team owner,Item item) : base(game,id,position,maxHealth,owner)
        {
            Item = item;
        }

        public void GatherItem(AdvancedUnit unit)
        {
            if (unit.ItemMaxRange < Common.Distance(unit.Position, Position))
            {
                unit.AddInInventory(Item);
                Chunk.ChunkData.RemoveUnit(this);
            }
        }

        public override void WriteTo(BinaryWriter w)
        {
            throw new NotImplementedException();
        }
    }
}
