﻿using SFML.Graphics;

namespace InfiniteMap
{
    static class Fonts
    {
        public static readonly Font Basic = new Font("resources/sansation.ttf");
        public static readonly Font InfiniteMap = new Font("resources/CLARKE_black.otf");
        public static readonly Font InfiniteMapOblic = new Font("resources/CLARKE_black-oblique.otf");
    }
}
       