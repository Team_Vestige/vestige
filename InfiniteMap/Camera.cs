﻿using SFML.System;
using SFML.Window;
using System;

namespace InfiniteMap
{
    class Camera
    {
        bool _up;
        bool _left;
        bool _right;
        bool _down;
        bool _zoomIn;
        bool _zoomOut;
        Game _game;
        public void Attach(Game game)
        {
            _game = game;
            game.GameEvents.KeyPressed += Window_KeyPressed;
            game.GameEvents.KeyReleased += Window_KeyReleased;
            game.Update += Game_Update;
        }

        void Game_Update(object sender, Time frameTime)
        {
            const float cameraSpeed = 10f;
            float cameraMovement = frameTime.AsSeconds()*cameraSpeed;
            
            if (_up)
            {
                _game.CameraView.Move(new Vector2f(0, -cameraMovement));
            }

            if (_down)
            {
                _game.CameraView.Move(new Vector2f(0, cameraMovement));
            }
            if (_left)
            {
                _game.CameraView.Move(new Vector2f(-cameraMovement, 0));
            }
            if (_right)
            {
                _game.CameraView.Move(new Vector2f(cameraMovement, 0));
            }
            if (_zoomIn)
            {
                _game.CameraView.Zoom(1+cameraMovement);
            }
            if (_zoomOut)
            {
                _game.CameraView.Zoom(1-cameraMovement);
            }
        }

        void Window_KeyReleased(object sender, KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Up:
                    _up = false;
                    break;
                case Keyboard.Key.Down:
                    _down = false;
                    break;
                case Keyboard.Key.Left:
                    _left = false;
                    break;
                case Keyboard.Key.Right:
                    _right = false;
                    break;
                case Keyboard.Key.J:
                    _zoomOut = false;
                    break;
                case Keyboard.Key.K:
                    _zoomIn = false;
                    break;
            }
        }

        void Window_KeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Up:
                    _up = true;
                    break;
                case Keyboard.Key.Down:
                    _down = true;
                    break;
                case Keyboard.Key.Left:
                    _left = true;
                    break;
                case Keyboard.Key.Right:
                    _right = true;
                    break;
                case Keyboard.Key.J:
                    _zoomOut = true;
                    break;
                case Keyboard.Key.K:
                    _zoomIn = true;
                    break;
            }

            if (Keyboard.IsKeyPressed(Keyboard.Key.Escape))// Window closed or escape key pressed: exit
            {
                _game.GameState = MenuType.Quit;
            }
        }
    }
}
