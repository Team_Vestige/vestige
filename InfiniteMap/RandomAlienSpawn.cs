﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using Vestige.Map;

namespace Vestige
{
    class RandomAlienSpawn
    {
        Game _game;
        Random _random;

        public RandomAlienSpawn(Game game)
        {
            _game = game;
            _random = new Random();
        }
        public void AlienSpawn()
        {
            foreach (var chunk in _game.Map)
            {
                int randomNumber = _random.Next(1, 10);
                if (chunk.Value.ChunkData.Units.FirstOrDefault(unit => unit.Team == Unit.Team.Human) != null) continue;
                if (chunk.Value.ChunkData.Units.Count > 6) continue;
                if (chunk.Value.ZoneData == null || randomNumber != 5) continue;
                var pos = new Vector2i(_random.Next(0, 15), _random.Next(0, 15));
                var k = 0;
                while (chunk.Value.ZoneData[pos, 1] == null)
                {
                    k++;
                    pos = new Vector2i(_random.Next(0, 15), _random.Next(0, 15));
                    if (k == 5) break;
                }
                if (k != 5) new Unit.MovableUnit(_game, 0, (Vector2f)pos + Common.MultiplyVector(chunk.Value.Position, 16) + new Vector2f((float)_random.NextDouble(), (float)_random.NextDouble()), 100f, Unit.Team.Alien, 30f, 0, 3f, 3);
            }
        }
    }
}
