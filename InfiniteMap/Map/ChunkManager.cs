﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SFML.Window;

namespace InfiniteMap.Map
{
    class ChunkManager
    {
        readonly Game _game;
        Task _chunkAsyncGenerator;
        List<Chunk> _toAdd;
        bool _running;
        Dictionary<Vector2i, float> toPopulate;
        public ChunkManager(Game game)
        {
            _game = game;
            _running = true;
            _game.Update += _game_Update;
            toPopulate = new Dictionary<Vector2i, float>();
            _toAdd = new List<Chunk>();
        }

        void _game_Update(object sender, Time e)
        {
            if (_chunkAsyncGenerator != null && !_chunkAsyncGenerator.IsCompleted) return;
            foreach (var elem in _toAdd)
            {
                _game.Map.AddChunk(elem);
            }
            _toAdd.Clear();

            Vector2i topLeftChunk = FindTopLeft(_game.CameraView);
            Vector2i bottomRightChunk = FindBotRight(_game.CameraView);
            Vector2i middleChunk = FindMiddle(_game.CameraView);
            toPopulate.Clear();
            for (int x = topLeftChunk.X - 10; x <= bottomRightChunk.X + 10; x++) //Parse all PreChunk present on screen + 1 outside to generate chunk offscreen
            {
                for (int y = topLeftChunk.Y - 10; y <= bottomRightChunk.Y + 10; y++)
                {
                    Vector2i coo = new Vector2i(x, y);
                    if (!_game.Map.ChunkExist(coo))
                    {
                        toPopulate.Add(coo, Common.DistancePow2(coo, middleChunk));
                    }
                }
            }
            _chunkAsyncGenerator = Task.Run(() => { PopulateAroundCamera(); });
        }

        public void StopProcess()
        {
            _running = false;
            _chunkAsyncGenerator.Wait();
        }

        void PopulateAroundCamera()
        {
            if (!_running) return;

            var items = from pair in toPopulate
                        orderby pair.Value
                        select pair;
            int i = 0;
            foreach (var pair in items)
            {
                if (!_running) return;
                i++;
                if (i == 10)
                {
                    break;
                }
                _toAdd.Add(new Chunk(pair.Key, _game.Map));
            }
        }
        public void PopulateSpawn()
        {
            for (int x = -6; x <= 6; x++)
            {
                for (int y = -6; y <= 6; y++)
                {
                    Vector2i coo = new Vector2i(x, y);
                    _game.Map.AddChunk(new Chunk(coo, _game.Map));
                }
            }
        }


        static Vector2i FindTopLeft(View view)
        {
            Vector2f topLeft = view.Center - view.Size / 2;
            Vector2i topLeftChunk = Map.PositionOfChunk((Vector2d)topLeft);
            return topLeftChunk;
        }
        static Vector2i FindBotRight(View view)
        {
            Vector2f botRight = view.Center + view.Size / 2;
            Vector2i botRightChunk = Map.PositionOfChunk((Vector2d)botRight);
            return botRightChunk;
        }
        static Vector2i FindMiddle(View view)
        {
            return Map.PositionOfChunk((Vector2d)view.Center);
        }
    }
}

