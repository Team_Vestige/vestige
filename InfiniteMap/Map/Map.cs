﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace InfiniteMap.Map
{
    class Map : Drawable, IDisposable
    {
        public readonly Perlin Perlin;
        Dictionary<Vector2i, Chunk> _chunks;
        Game _game;
        public Map(Game game, int seed)
        {
            _game = game;
            Perlin = new Perlin(seed);
            _chunks = new Dictionary<Vector2i, Chunk>();//Todo serialize that later
        }


        #region GetterSetter


        /// <summary>
        /// Enumerator of the chunk of the map
        /// </summary>
        /// <returns>Enumerator</returns>
        public IEnumerator<KeyValuePair<Vector2i, Chunk>> GetEnumerator()
        {
            return _chunks.GetEnumerator();
        }
        #endregion
        /// <summary>
        /// Chunks of the map.
        /// </summary>
        /// <param name="x">Axe X of the chunk</param>
        /// <param name="y">Axe Y of the chunk</param>
        /// <returns>An instancied PreChunk or null if it doesn't exist.</returns>
        public Chunk this[int x, int y]
        {
            get
            {
                _chunks.TryGetValue(new Vector2i(x, y), out Chunk c);
                return c;
            }
            set => _chunks[new Vector2i(x, y)] = value;
        }

        public Chunk this[Vector2i coo]
        {
            get => this[coo.X, coo.Y];
            set => this[coo.X, coo.Y] = value;
        }



        /// <summary>
        /// Add a PreChunk to the map at a coordinate.
        /// </summary>
        /// <param name="chunk">PreChunk to add, this PreChunk shouldn't be already in the map</param>
        public void AddChunk(Chunk chunk)
        {
            if (_chunks.ContainsKey(chunk.Position))
            {
                throw new ArgumentException("A chunk already exist at these coordinate.");
            }
            if (_chunks.ContainsValue(chunk))
            {
                throw new ArgumentException("This chunk already exist in this map.");
            }
            _chunks.Add(chunk.Position, chunk);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            Vector2d topLeft = (Vector2d)_game.CameraView.Center - _game.CameraView.Size;
            Vector2i topLeftChunk = PositionOfChunk(topLeft);
            Vector2d botRight = (Vector2d)_game.CameraView.Center + _game.CameraView.Size;
            Vector2i botRightChunk = PositionOfChunk(botRight);
            for (int x = topLeftChunk.X; x < botRightChunk.X + 1; x++)
            {
                for (int y = topLeftChunk.Y; y < botRightChunk.Y + 1; y++)
                {
                    Vector2i coo = new Vector2i(x, y);

                    if (_chunks.TryGetValue(coo, out var chunk ) )
                    {
                        chunk.TileVertexs.Draw(target, states);
                    }
                }
            }
        }

        #region PositionFunctions
        public bool ChunkExist(Vector2i chunkCoo)
        {
            return _chunks.ContainsKey(chunkCoo);
        }
       
       
        /// <summary>
        ///  Return the Vector2i of the PreChunk where is the point. 
        /// </summary>
        /// <param name="point">A point in a PreChunk</param>
        /// <returns>Coordinate of the PreChunk</returns>
        public static Vector2i PositionOfChunk(Vector2d point)
        {
            return new Vector2i((int)Math.Floor(point.X) >> Chunk.TileBits, (int)Math.Floor(point.Y) >> Chunk.TileBits);
        }

       
        #endregion


       
        public void Dispose()
        {
            foreach (var elem in _chunks)
            {
                elem.Value.Dispose();
            }
        }
    }
}

