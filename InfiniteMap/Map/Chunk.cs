﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace InfiniteMap.Map
{
    class Chunk : IDisposable
    {
        protected const int BinaryVersion = 0;
        public const int TileBits = 4;
        public const int TileSize = 1 << TileBits;
        public const int TileMask = TileSize - 1;
        readonly Tile[] _tiles;

        public Chunk(Vector2i position, Map map)
        {
            Position = position;
            Map = map;
            // crée l'unité en fonction de son type 
            _tiles = new Tile[TileSize * TileSize];
            for (int x = 0; x < TileSize; x++)
            {
                for (int y = 0; y < TileSize; y++)
                {

                    float bruit = map.Perlin.ParametablePerlinNoise((x + Position.X * TileSize), (y + Position.Y * TileSize), 0.1f, Perlin.InterpolantType.Wide);
                    if (bruit > 0.5)
                    {
                        _tiles[(x << TileBits) | y] = Tile.Create(TileType.Wall, 0);
                    }
                    else
                    {
                        _tiles[(x << TileBits) | y] = Tile.Create(TileType.Path, 0);
                    }
                }
            }
            UpdateTileVertex();
            CheckInstance();
        }
        void UpdateTileVertex()
        {
            TileVertexs = new TilesVertex(this, Position * TileSize);
        }

        void CheckInstance()
        {
            if (Map == null) throw new Exception("Map is null");
            if (TileVertexs == null) throw new Exception("TileVertexs is null");
            if (Position == null) throw new Exception("Position is null");
            if (_tiles == null) throw new Exception("Tiles is null");

        }
        public Vector2i Position { get; }

        protected Map Map { get; }

        /// <summary>
        /// Return the TileType at a given coo
        /// </summary>
        /// <param name="x"></param>
        /// <returns>A TileType</returns>
        public Tile this[int x]
        {
            get => _tiles[x];
            set
            {
                _tiles[x] = value;
                UpdateTileVertex();
            }
        }

        Tile this[int x, int y]
        {
            get
            {
                CheckCoords(x, y);
                return this[(x << TileBits) | y];
            }
            set
            {
                CheckCoords(x, y);
                this[(x << TileBits) | y] = value;
            }
        }

        Tile this[Vector2i coo]
        {
            get => this[coo.X, coo.Y];
            set => this[coo.X, coo.Y] = value;
        }

        /// <summary>
        /// Check if the local coordinates are in the preChunk.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        static void CheckCoords(int x, int y)
        {
            if (y >= TileSize) throw new ArgumentOutOfRangeException();
            if (x < 0) throw new ArgumentOutOfRangeException(nameof(x));
            if (y < 0) throw new ArgumentOutOfRangeException(nameof(y));
        }

        /// <summary>
        /// Get the vertex array of the preChunk.
        /// </summary>
        public TilesVertex TileVertexs { get; private set; }

        public void Dispose()
        {
            TileVertexs?.Dispose();
        }
    }
}
