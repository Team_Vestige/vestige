﻿using SFML.Graphics;
using SFML.Window;
using System;
using OpenTK;
using OpenTK.Graphics;

namespace InfiniteMap
{
    public enum MenuType
    {
        Game,
        Menu,
        Load,
        Option,
        Quit
    }

    static class Program
    {
        static void Main()
        {
            MenuType choice = MenuType.Menu;
            Console.WriteLine("FullScreen Mode ? y/n");

            var answere = Console.ReadLine()?.ToLower();
            bool windowed = !(answere == "yes" || answere == "y");
            //Change true for window, false for fullscreen
            var window = windowed ? new RenderWindow(VideoMode.FullscreenModes[0], "InfiniteMap", Styles.Titlebar | Styles.Close) : new RenderWindow(VideoMode.FullscreenModes[0], "InfiniteMap", Styles.Fullscreen);
            window.SetActive(true);
            Console.WriteLine("Vendor: " + GL.GetString(GL.PlatformInfo.Renderer));
            while (choice != MenuType.Quit)
            {
                choice = Game.RunGame(window, true);
            }
            window.Close();
        }

        


    }
}
