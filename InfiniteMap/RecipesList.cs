﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige
{
    class RecipesList
    {
        List<Recipe> recipes { get; set; }
        
        public void AddRecipe (Recipe recette)
        {
            recipes.Add(recette);
        }
        
        public bool IsExisting (Recipe recette)
        {
            if (recipes.Contains(recette))
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
