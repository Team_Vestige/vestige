﻿namespace InfiniteMap
{
    static class Extensions
    {
        public static bool IsBlocking(this TileType type)
        {
            return type != TileType.Path;
        }
    }
    
}
