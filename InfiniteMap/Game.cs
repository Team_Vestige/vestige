﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML.Audio;
using System.Threading;

namespace InfiniteMap
{
    class Game : IDisposable
    {
        static int time = 25;
        public readonly RenderWindow Window;
        public readonly View CameraView;
        public readonly Map.Map Map;    
        public MenuType GameState;
        public event EventHandler<Time> Update;
        readonly Clock _frameTime;
        public Time DayTime;
        public int NumberOfDay;
        Sound _sound;
        public readonly GameEvents GameEvents;
        readonly View _guiView;
        readonly Map.ChunkManager _chunkManager;
        public readonly Time _timeInADAY = Time.FromSeconds(60);
        Text text;
        Text seed;
        Vector2f movement;
        float zoom;
        Game(RenderWindow window, bool newGame)
        {   
            
            #region Events subscriding
            GameEvents = new GameEvents(window);
            new Camera().Attach(this);
            window.Closed += Quit;
            Update += Game_Update;
            #endregion
            #region Variable Initialiation
            Window = window;
            GameState = MenuType.Game;
            _frameTime = new Clock();
            DayTime = new Time();
            #endregion
            #region View Initialiation
            /**
             * View Init
             **/
            CameraView = new View(new FloatRect(new Vector2f(0, 0),
                new Vector2f(VideoMode.FullscreenModes[1].Width, VideoMode.FullscreenModes[1].Height)));
            CameraView.Zoom(0.03f);
            CameraView.Center = new Vector2f(25, 21);
            
            _guiView = Window.DefaultView;
            #endregion
            #region Map Initialisation
            /**
             * Map Init
             **/
            var rand = new Random();
            int temp = rand.Next();
            movement = new Vector2f(0.2f*((float)rand.NextDouble()-0.5f), 0.2f*((float)rand.NextDouble()-0.5f));
            zoom =  1+0.005f*(float)rand.NextDouble();
            Map = new Map.Map(this, temp) ; //Init Map
            text = new Text("Des possibilités infinies de terrains...", Fonts.InfiniteMap);
            text.CharacterSize += 80;
            
            seed = new Text("Seed: #" + temp, Fonts.Basic) {Position = new Vector2f(0, 200)};
            _chunkManager = new Map.ChunkManager(this);


            #endregion


            GameEvents.KeyPressed += (sender, args) =>
            {
                switch (args.Code)
                {
                    case Keyboard.Key.Space:
                        GameState = MenuType.Menu;
                        break;
                    case Keyboard.Key.Add:
                        time += 1;
                        break;
                    case Keyboard.Key.Subtract:
                        time -= 1;
                        break;
                }
            };
            if (newGame)
            {
                GamePlayInit();
            }
            

        }

        void GamePlayInit()
        {
            /**
             * GamePlay Init
             **/
            _chunkManager.PopulateSpawn();
            DayTime = new Time();
            Update += Game_Update;
           var rand = new Random();
            ThreadPool.QueueUserWorkItem(threadContext =>
            {
                SoundBuffer buffer = new SoundBuffer("resources/sound.ogg");
                _sound = new Sound(buffer);
                if (GameState == MenuType.Game) _sound.Play();
            });

        }

        void Quit(object sender, EventArgs args)
        {
            GameState = MenuType.Quit;
        }


        void Game_Update(object sender, Time e)
        {
            DayTime += e;
            if (DayTime.AsSeconds() > time) GameState = MenuType.Menu;
            if (!(DayTime > _timeInADAY)) return;
            NumberOfDay++;
            DayTime -= _timeInADAY;
        }

        void GameLoop()
        {
            /**
            * Input management
            **/

            Window.DispatchEvents();
            Update?.Invoke(this, _frameTime.Restart());
            CameraView.Center += movement;
            CameraView.Zoom(zoom);
            /**
             * Drawing 
             **/
            Window.Clear();
            Window.SetView(CameraView);
            Window.Draw(Map);
            
            Window.SetView(_guiView);
            Window.Draw(text);
            Window.Draw(seed);
            //View for hud
            //Game Over
            //Window.Draw(sprite);
            Window.Display();
        }



        public static MenuType RunGame(RenderWindow window, bool newGame)
        {
            Game game = new Game(window, newGame);
            while (game.GameState == MenuType.Game)
            {
                game.GameLoop();
            }
            game.Dispose();
            return game.GameState;
        }


        public void Dispose()
        {
            _chunkManager.StopProcess();
            CameraView?.Dispose();
            _frameTime?.Dispose();
            _sound?.Dispose();
            _guiView?.Dispose();
            Map?.Dispose();
            Window.Closed -= Quit;
            GameEvents.Unsuscribe();
        }
    }
}
