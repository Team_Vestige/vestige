﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vestige
{
    public enum ItemType
    {
        Weapon,
        Wood,
        Stone,
        Gold,
        Artifact,
        Equipement,
        Ammo,
    }

    class Item
    {

    public Item(ItemType type)
        {
            Type = type;           
        }

        public ItemType Type 
        {
            get;
            set;          
        }
    }
}
