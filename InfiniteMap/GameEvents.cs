﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace InfiniteMap
{
    class GameEvents
    {
        public RenderWindow _window;
        public EventHandler<KeyEventArgs> KeyPressed;
        public EventHandler<KeyEventArgs> KeyReleased;
        public GameEvents(RenderWindow window)
        {
            _window = window;
            window.KeyPressed += Window_KeyPressed;
            window.KeyReleased += Window_KeyReleased;
        }


        void Window_KeyReleased(object sender, KeyEventArgs e)
        {
            KeyReleased?.Invoke(sender, e);
        }

        void Window_KeyPressed(object sender, KeyEventArgs e)
        {
            KeyPressed?.Invoke(sender, e);
        }



        public void Unsuscribe()
        {
            _window.KeyPressed -= Window_KeyPressed;
            _window.KeyReleased -= Window_KeyReleased;
        }
    }
}
