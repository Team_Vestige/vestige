﻿using System.Drawing;
using SFML.Graphics;
using SFML.System;
using System.IO;
using System.Drawing.Imaging;

namespace InfiniteMap
{
    public class TextureArray
    {
        static readonly QuadCoords[] QuadCoordsArray;

        static TextureArray()
        {
            QuadCoordsArray = new QuadCoords[byte.MaxValue + 1];
            new TextureArray();
        }

        /// <summary>
        /// Create a bitmap(tileSet) that contains all the texture contained in the resource directory and a array(quadCoordsArray) that contains all the possition x,y of the texture in the bitmap array
        /// </summary>
        TextureArray()
        {
            Bitmap makedTileSet = new Bitmap(ImageLoader.TotalWidth(), ImageLoader.Higher());

            Vector2f vector = new Vector2f(0, 0);
            for (int i = 0; i < ImageLoader.BitmapArrayLength; i++)
            {
                makedTileSet = CopiePixelsTo(vector, i, makedTileSet);
                var toCopy = ImageLoader.Instance[i];
                vector.X += toCopy.Width;

            }

            Bitmap image = makedTileSet;

            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Bmp);
                //  QuadCoords textureCoord = TextureArray.GetTextureCoord(1);
                stream.Seek(0, SeekOrigin.Begin);
                Texture tileset = new Texture(new SFML.Graphics.Image(stream));
                TileSet = tileset;
            }
        }

        /// <summary>
        /// Copie Pixels from the imageNumber index in the ImageLoader array to the bitmap tileset at the given position 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="imageNumber"></param>
        /// <param name="tileset"></param>
        static Bitmap CopiePixelsTo(Vector2f position, int imageNumber, Bitmap tileset)
        {
            Bitmap toCopy = ImageLoader.Instance[imageNumber];
            QuadCoordsArray[imageNumber] = new QuadCoords(position, new Vector2f(toCopy.Width, toCopy.Height) +position);
            for (int i = 0; i < toCopy.Width; i++)
            {
                for (int j = 0; j < toCopy.Height; j++)
                {
                    tileset.SetPixel(i + (int)position.X, j + (int)position.Y, toCopy.GetPixel(i, j));
                }
            }
            return tileset;
        }

        public static QuadCoords GetTextureCoord(int textureNumber)
        {

            return QuadCoordsArray[textureNumber];
        }

        public static Texture TileSet { get; private set; }
    }
}
