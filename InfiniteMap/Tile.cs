﻿using SFML.System;
using System;
using System.Collections.Generic;

namespace InfiniteMap
{
    public enum TileType
    {
        None,
        Wall,
        Path,
        Building,
        Stone_Ore
    }
    /// <summary>
    /// Store data of the tile.
    /// </summary>
    class Tile
    {
        static readonly Dictionary<Tile, Tile> Tiles;
        static Tile()
        {
            Tiles = new Dictionary<Tile, Tile>();
        }

        Tile(TileType type, byte damage)
        {
            Type = type;
            Damage = damage;
        }
        public TileType Type { get; }

        public byte Damage { get; }

        public override bool Equals(object obj)
        {
            return obj is Tile tile && Type == tile.Type && Damage == tile.Damage;
        }
        public override int GetHashCode()
        {
            int hash = 23;
            hash = hash * 31 + (int)Type;
            hash = hash * 31 + Damage;
            return hash;
        }

        public static Tile Create(TileType type, byte damage)
        {
            var candidate = new Tile(type, damage);
            if (Tiles.TryGetValue(candidate, out Tile exists))
            {
                return exists;
            }
            Tiles.Add(candidate, candidate);
            return candidate;
        }
    }
}
