﻿using SFML.Graphics;
using SFML.System;
using System;

namespace InfiniteMap
{
    class Animation
    {
        Time _animationTime;
        Time _currentTime;
        Time _secondePerFrame;
        int[] _frameList;
        bool _loopAnimation;
        public event EventHandler EndAnimation;

        /// <summary>
        /// Create Animation
        /// </summary>
        /// <param name="time">Time of animation</param>
        /// <param name="loopAnimation">True for infinite animation, False for stop animation at the time</param>
        /// <param name="frameList"> table of number(name) of image </param>
        public Animation(Game game, Time time, bool loopAnimation, int[] frameList)
        {
            game.Update += Game_Update;
            _currentTime = new Time();
            _animationTime = time;
            _loopAnimation = loopAnimation;
            _frameList = frameList;
        }

        private void Game_Update(object sender, Time e)
        {
            _currentTime += e;
            if (_currentTime >= _animationTime && _loopAnimation)
            {
                _currentTime -= _animationTime;
                EndAnimation?.Invoke(this, EventArgs.Empty);
            }
            else if (_currentTime >= _animationTime && !_loopAnimation)
                EndAnimation?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Get IntRect of good texture
        /// </summary>
        /// <returns>IntRect good texture in function of time</returns>
        public IntRect GetIntRect()
        {
            _secondePerFrame = Time.FromSeconds(_animationTime.AsSeconds() / _frameList.Length);
            int numberFrame = (int)Math.Floor(_currentTime.AsSeconds() / _secondePerFrame.AsSeconds());

            if (_currentTime <= _animationTime)
                return TextureArray.GetTextureCoord(_frameList[numberFrame]).IntRect();
            else
                return TextureArray.GetTextureCoord(_frameList[_frameList.Length - 1]).IntRect();
        }
    }
}