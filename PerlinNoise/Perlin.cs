﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerlinNoise
{
    class Perlin
    {
        private readonly static Vector2f[] direction = new Vector2f[4] { new Vector2f(0.0f, 0.0f), new Vector2f(1.0f, 0.0f), new Vector2f(1.0f, 1.0f), new Vector2f(0.0f, 1.0f) };

        public static Random RandomGen(long x, long y, int seed)
        {
            return new Random((int)(x / ((long)(int.MaxValue) * 4) * 1424 + y / ((long)(int.MaxValue) * 4)) | seed);
        }


        public struct Fake2DArray<T>
        {
            T[] array;
            int TileBits;
            public int TileSize;
            int TileMask;
            public Fake2DArray(int size)
            {
                TileBits = size;
                TileSize = 1 << TileBits;
                TileMask = TileSize - 1;
                array = new T[TileSize * TileSize];
            }
            public T this[int x]
            {
                get { return array[x]; }
                set { array[x] = value; }
            }
            public T this[int x, int y]
            {
                get
                {
                    CheckCoords(x, y);
                    return array[x << TileBits | y];
                }
                set
                {
                    CheckCoords(x, y);
                    array[x << TileBits | y] = value;
                }
            }
            void CheckCoords(int x, int y)
            {
                if (x < 0 || y < 0 || y >= TileSize) throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Return an array of white noise of the given size
        /// </summary>
        /// <param name="size"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        public static Fake2DArray<float> FillNoise(int size, int seed)
        {
            Random random = new Random(seed);
            Fake2DArray<float> output = new Fake2DArray<float>(size);
            for (int x = 0; x < output.TileSize * output.TileSize; x++)
            {

                output[x] = (float)random.NextDouble();
            }
            return output;
        }

        /// <summary>
        /// Return a drawable from a Noise
        /// </summary>
        /// <param name="noise">Noise</param>
        /// <returns>A vertex array</returns>
        public static Drawable GetDrawableFromNoise(Fake2DArray<float> noise)
        {
            VertexArray output = new VertexArray(PrimitiveType.Quads, (uint)(noise.TileSize * noise.TileSize));
            for (int x = 0; x < noise.TileSize; x++)
            {
                for (int y = 0; y < noise.TileSize; y++)
                {
                    AddTileVertices(output, new Vector2f(x, y), new Color(0, 0, (byte)(noise[x, y] * 255)));
                }
            }
            return output;
        }
        static void AddTileVertices(VertexArray vertexArray, Vector2f position, Color color)
        {
            vertexArray.Append(
                new Vertex((
                    direction[0] + position), color));

            vertexArray.Append(
                new Vertex((
                    direction[1] + position), color));

            vertexArray.Append(
                new Vertex((
                    direction[2] + position), color));

            vertexArray.Append(
                new Vertex((
                    direction[3] + position), color));
        }

        public enum Interpolant
        {
            smoothstep
        }
    }
}


