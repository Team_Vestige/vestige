﻿using System;
using System.Collections.Generic;
using System.Text;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;
using System.Linq;

namespace PerlinNoise
{
    class Program
    {
        static void Main(string[] args)
        {

            RenderWindow window = new RenderWindow(VideoMode.FullscreenModes[2], "LOL", Styles.Titlebar | Styles.Close);
            View view = new View(new FloatRect(new Vector2f(0, 0), new Vector2f(VideoMode.FullscreenModes[1].Width, VideoMode.FullscreenModes[1].Height)))
            {
                Center = new Vector2f(500, 500)
            };
            view.Zoom(10f / VideoMode.FullscreenModes[2].Height);
            window.SetView(view);
            view.Center = new Vector2f(0,0);
            //Playground
            PerlinCopy perlin = new PerlinCopy(35);



            Perlin.Fake2DArray<float> noise = new Perlin.Fake2DArray<float>(10);
            List<float> testList = new List<float>();
            for (int x = 0; x < noise.TileSize; x++) 
            {
                for (int y = 0; y < noise.TileSize; y++)
                {
                    noise[x, y] = 0;
                    if( perlin.ParametablePerlinNoise(x / (float)noise.TileSize, y / (float)noise.TileSize, 10f, PerlinCopy.InterpolantType.wide) > 0.7)
                    {
                        noise[x, y] = 1;
                    }

                    

                    //testList.Add(perlin.ParametablePerlinNoise(x / (float)noise.TileSize, y / (float)noise.TileSize, 20));
                }
            }
            Drawable test = Perlin.GetDrawableFromNoise(noise);
            while (!Keyboard.IsKeyPressed(Keyboard.Key.Escape))
            {
                window.DispatchEvents();
                if (Keyboard.IsKeyPressed(Keyboard.Key.J))
                {
                    view.Zoom(0.95f);
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.K))
                {
                    view.Zoom(1.05f);
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Right))
                    view.Move(new Vector2f(0.05f, 0));

                if (Keyboard.IsKeyPressed(Keyboard.Key.Down))
                    view.Move(new Vector2f(0, 0.05f));

                if (Keyboard.IsKeyPressed(Keyboard.Key.Left))
                    view.Move(new Vector2f(-0.05f, 0));

                if (Keyboard.IsKeyPressed(Keyboard.Key.Up))
                    view.Move(new Vector2f(0, -0.05f));


                window.Clear(Color.Blue);
                window.SetView(view);
                window.Draw(test);
                window.Display();
            }
            window.Close();
        }
    }
}
