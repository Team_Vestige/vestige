﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PerlinNoise
{
    class PerlinCopy
    {
        readonly static int size = 256;
        readonly static int mask = size - 1;
        static int[] perm;
        static PerlinCopy()
        {
            perm = new int[size];
            for (int i = 0; i < size; i++)
            {
                perm[i] = i;
            }
        }


        float[] grads_x = new float[size];
        float[] grads_y = new float[size];
        public PerlinCopy(int seed)
        {
            Random rand = new Random(seed);
            for (int index = 0; index < size; ++index)
            {
                int other = rand.Next() % (index + 1);
                if (index > other)
                    perm[index] = perm[other];
                perm[other] = index;
                grads_x[index] = (float)Math.Cos(2.0f * Math.PI * (float)index / size);
                grads_y[index] = (float)Math.Sin(2.0f * Math.PI * (float)index / size);
            }
        }

        float Cubic(float t)
        {
            t = Math.Abs(t);
            return t >= 1.0f ? 0.0f : 1.0f -
                (3.0f - 2.0f * t) * t * t;
        }
        float Wide(float t)
        {
            t = 1.0f - 0.25f * t * t;
            return t < 0.0f ? 0.0f :
                (4.0f * t - 3.0f) * t * t * t * t;
        }
        float Quintic(float t)
        {
            t = Math.Abs(t);
            return ((6.0f * t - 15.0f) * t + 10.0f) * t * t * t;
        }
        float Surflet(float x, float y, float grad_x, float grad_y, InterpolantType interpolant)
        {
            switch (interpolant)
            {
                case InterpolantType.cubic:
                    return Cubic(x) * Cubic(y) * (grad_x * x + grad_y * y);
                case InterpolantType.wide:
                    return Wide(x) * Wide(y) * (grad_x * x + grad_y * y);
                case InterpolantType.quintic:
                    return Quintic(x) * Quintic(y) * (grad_x * x + grad_y * y);
                default:
                    throw new NotImplementedException();
            }

        }
        public float ParametablePerlinNoise(float x, float y, float frequency, InterpolantType interpolant = InterpolantType.cubic)
        {

            //  float frequency = 5.50f;
            float total = 0;
            float amplitude = 1;
            total += 1 * Noise(x * frequency, y * frequency, interpolant) * amplitude;
            total += 0.5f * Noise(x * frequency * 1.75f, y * frequency * 1.75f, interpolant) * amplitude;



            return total / 1.5f;
        }

        public float Noise(float x, float y, InterpolantType interpolant)
        {
            float result = 0.0f;
            int cell_x = (int)Math.Floor(x);
            int cell_y = (int)Math.Floor(y);
            for (int grid_y = cell_y; grid_y <= cell_y + 1; ++grid_y)
            {
                for (int grid_x = cell_x; grid_x <= cell_x + 1; ++grid_x)
                {
                    int hash = perm[(perm[grid_x & mask] + grid_y) & mask];
                    result += Surflet(x - grid_x, y - grid_y,
                                       grads_x[hash], grads_y[hash], interpolant);
                }
            }
            return ((result) + 1f) / 2;
        }

        public enum InterpolantType
        {
            cubic,
            wide,
            quintic
        }
    }
}
