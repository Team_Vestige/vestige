﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;

namespace ConsoleApp1
{
    public class TextureArray
    {
        static TextureArray instance;
        static Bitmap _tileSet;
        static QuadCoords[] _quadCoordsArray;

        /// <summary>
        /// Create a bitmap(tileSet) that contains all the texture contained in the resource directory and a array(quadCoordsArray) that contains all the possition x,y of the texture in the bitmap array
        /// </summary>
        private TextureArray()
        {
            _quadCoordsArray = new QuadCoords[byte.MaxValue + 1];
            _tileSet = TileSetMaker();
        }
        /// <summary>
        /// Call the CopiePixelsTo function for each image in the Imageloader array and increments the vector.x
        /// </summary>
        private static Bitmap TileSetMaker()
        {
            Bitmap toCopy = ImageLoader.Instance[0];
            Bitmap MakedTileSet = new Bitmap(ImageLoader.Instance.TotalWidth(), ImageLoader.Instance.Higher());

            Vector2f vector = new Vector2f(0, 0);
            for (int i = 0; i < ImageLoader.Instance.BitmapArrayLength; i++)
            {
                MakedTileSet = CopiePixelsTo(vector, i, MakedTileSet);
                toCopy = ImageLoader.Instance[i];
                vector.X += toCopy.Height;

            }
            return MakedTileSet;
        }


        /// <summary>
        /// Copie Pixels from the imageNumber index in the ImageLoader array to the bitmap tileset at the given position 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="imageNumber"></param>
        public static Bitmap CopiePixelsTo(Vector2f position, int imageNumber, Bitmap tileset)
        {
            Bitmap toCopy = ImageLoader.Instance[imageNumber];
            _quadCoordsArray[imageNumber] = new QuadCoords(position, new Vector2f(toCopy.Width, toCopy.Height) + position);

            for (int i = 0; i < toCopy.Height; i++)
            {
                for (int j = 0; j < toCopy.Width; j++)
                {
                    tileset.SetPixel(i + (int)position.X, j + (int)position.Y, toCopy.GetPixel(i, j));
                }
            }
            return tileset;
        }

        public static QuadCoords GetTextureCoord(int textureNumber)
        {

            return _quadCoordsArray[textureNumber];
        }

        public static TextureArray Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TextureArray();
                }
                return instance;
            }
        }

        public Bitmap TileSet
        {
            get
            {
                return _tileSet;
            }
        }

    }
}
