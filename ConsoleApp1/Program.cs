﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using SFML.Graphics;
using SFML.Window;
using SFML.System;
using System.Drawing.Imaging;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            

            RectangleShape nicolas = new RectangleShape();
            nicolas.Size = new Vector2f(500, 500);

            //string s = TextureArray.Instance.ToString();
            //Console.WriteLine(s);

            Bitmap image = TextureArray.Instance.TileSet;


            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Bmp);
              //  QuadCoords textureCoord = TextureArray.GetTextureCoord(1);
                stream.Seek(0, SeekOrigin.Begin);
                nicolas.Texture = new Texture(new SFML.Graphics.Image(stream));
            }
            RenderWindow window = new RenderWindow(VideoMode.FullscreenModes[1], "Vestige", Styles.Titlebar | Styles.Close /*Styles.Fullscreen*/);
            RectangleShape abc = new RectangleShape();
            abc.Size = new Vector2f(4119, 39);
            abc.Texture = nicolas.Texture;
            while (true)
            {
                window.Draw(abc);
                window.Display();
            }
        }
    }
}
