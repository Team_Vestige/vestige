﻿using SFML.System;
namespace ConsoleApp1
{
    /// <summary>
    /// Store 4 Vector2f
    /// </summary>
    public struct QuadCoords
    {
        public readonly Vector2f topLeft;
        public readonly Vector2f topRight;
        public readonly Vector2f bottomLeft;
        public readonly Vector2f bottomRight;
        public QuadCoords(Vector2f topLeft, Vector2f topRight, Vector2f bottomLeft, Vector2f bottomRight)
        {
            this.topLeft = topLeft;
            this.topRight = topRight;
            this.bottomLeft = bottomLeft;
            this.bottomRight = bottomRight;
        }

        public QuadCoords(Vector2f topLeft, Vector2f bottomRight)
        {
            this.topLeft = topLeft;
            this.bottomRight = bottomRight;
            bottomLeft = new Vector2f(topLeft.X, bottomRight.Y);
            topRight = new Vector2f(bottomRight.X, topLeft.Y);
        }
    }
}
