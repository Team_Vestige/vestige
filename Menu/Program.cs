﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;


namespace Menu
{
    class Program
    {
        static void Main(string[] args)
        {
            // Main menu window

            RenderWindow window = new RenderWindow(VideoMode.FullscreenModes[0], "Vestige", /*Styles.Titlebar | Styles.Close*/ Styles.Fullscreen);

            MainMenu menu = new MainMenu(window);
            Vector2f position2 = new Vector2f((float)(window.Size.X / 4.2), (float)(window.Size.Y / 13.5));
            Vector2f size2 = new Vector2f(650, 125);


            Texture imgButton2 = new Texture("resources/newgame.png");
            Button button2 = new Button(position2, size2, window)
            {
                Texture = imgButton2
            };


            Vector2f position3 = new Vector2f((float)(window.Size.X / 4.2), (float)(window.Size.Y / 4.3));
            Vector2f size3 = new Vector2f(650, 125);


            Texture imgButton3 = new Texture("resources/loadgame.png");
            Button button3 = new Button(position3, size3, window)
            {
                Texture = imgButton3
            };

            Vector2f position4 = new Vector2f((float)(window.Size.X / 4.2), (float)(window.Size.Y / 2.3));
            Vector2f size4 = new Vector2f(650, 125);


            Texture imgButton4 = new Texture("resources/start.png");
            Button button4 = new Button(position4, size4, window)
            {
                Texture = imgButton4
            };

            //View for Display menu
            menu.Display();
            window.Draw(button2);
            window.Draw(button3);
            window.Draw(button4);
            //Render
            window.Display();
        }
    }
}








// Texture for the  menu

//            Texture texture = new Texture("background.png");

//            // Sprites declaration

//            RectangleShape  button1 = new RectangleShape();
//            RectangleShape button2 = new RectangleShape();
//            RectangleShape button3 = new RectangleShape();
//            RectangleShape button4 = new RectangleShape();

//            // Setting the texture for the sprites

//            button1.Texture = texture;
//            button2.Texture = texture;
//            button3.Texture = texture;
//            button4.Texture = texture;

//            // Setting space and position for the sprites

//            button1.Texture(IntRect(250, 200, 500, 100));
//            button1.Position(Vector2f(650, 125));


//            button2.Texture(IntRect(250, 350, 500, 100));
//            button2.Position(Vector2f(650, 300));

//            button3.Texture(IntRect(250, 500, 500, 100));
//            button3.Position(Vector2f(650, 500));

//            //button4.Texture(IntRect(250, 650, 500, 100));
//            ////button4.Position(Vector2f(250, 650));

//            //  loop
//            /*
//            while (window.isOpen())
//            {
//                Event event = new Event();
//            while (window.pollEvent(event))
//            {
//            if (event.type == Event.Closed)

//            {
//            window.close();
//        }
//        }

//        window.clear();
//            */

//            // Sprites drawing

//            window.draw(button1);
//            window.draw(button2);
//            window.draw(button3);
//            window.draw(button4);

//            window.display();
//        }

//        return 0;
//    }
//    }
//}
