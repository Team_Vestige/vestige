﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Audio;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using SFML;


namespace Vestige
{

    class MainMenu
    {
        RenderWindow _window;
        RectangleShape menu;
        Text _text1;
        Text _text2;
        Text _text3;
        Font _font;

        public MainMenu(RenderWindow window)
        {
            _window = window;
            _font = new Font("resources/sansation.ttf");
             
            menu = new RectangleShape
            {
                Size = new Vector2f(200, 200),
                Position = new Vector2f(1200, 500),
                FillColor = Color.Blue
            };

            //_font = new Font("resources/sansation.ttf");
            _text1 = new Text("Play GAME", _font)
            {
                Color = Color.Black,
                CharacterSize = 50,
                Position = new Vector2f(650, 125) 
            };

            _text2 = new Text("Load GAME", _font)
            {
                Color = Color.Black,
                CharacterSize = 50,
                Position = new Vector2f(650, 300)
            };

            _text3 = new Text("OPTION", _font)
            {
                Color = Color.Black,
                CharacterSize = 50,
                Position = new Vector2f(650, 500)
            };

        }
        public void Display()
        {
            _window.Draw(menu);
            _window.Draw(_text1);
            _window.Draw(_text2);
            _window.Draw(_text3);
        }
    }
}
