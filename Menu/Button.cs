﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{

    /// <summary>
    /// Callback function
    /// </summary>
    /// <param name="a">Top left button</param>
    /// <param name="b">Buttom right</param>
    /// <returns></returns>
    public delegate bool ButtonIsClicked(Vector2f a, Vector2f b);
    public delegate bool MouseInButton(Vector2f c);

    public class Button : RectangleShape
    {
        RenderWindow _window;
        ButtonIsClicked ButtonCallback;
        MouseInButton MouseCallback;
        static List<Button> _list = new List<Button> { };

        public Button(Vector2f position, Vector2f size, RenderWindow window) : base()
        {
            _window = window;
            base.Position = position;
            base.Size = size;
            base.FillColor = Color.White;
            _list.Add(this);
        }

        public static void ButtonInList()
        {
            foreach (Button x in _list)
            {
                x.MouseOvered(x.Position, x.Size);
            }
        }

        public void SetButtonOverCallback(ButtonIsClicked callback)
        {
            ButtonCallback = callback;
        }

        public void SetMouseOverCallback(MouseInButton callback)
        {
            MouseCallback = callback;
        }

        private void MouseOvered(Vector2f a, Vector2f b)
        {
            Vector2f c = (Vector2f)Mouse.GetPosition(_window);
            if (c.X > a.X && c.X < b.X && c.Y > a.Y && c.Y < b.Y)
            {
                MouseCallback(c);
                if (Mouse.IsButtonPressed(Mouse.Button.Left))
                {
                    ButtonCallback(a, b);
                }
            }
        }

        private void ButtonOvered(RectangleShape button)
        {
            Vector2f a = button.Position;
            Vector2f b = a + button.Size;
            MouseOvered(a, b);
        }

        /// <summary>
        /// Return true if the point is on axes aligned rectangle.
        /// </summary>
        /// <param name="a">top left rectangle</param>
        /// <param name="b">buttom right</param>
        /// <param name="c">mouse position</param>
        /// <returns>return true if "c" is in rectangle between "a" and "b"</returns>
        public bool CursorInRect(Vector2f a, Vector2f b, Vector2f c)
        {
            if (b.X < a.X || b.Y < a.Y)
            {
                throw new ArgumentException("BAD RECTANGLE");
            }
            return c.X > a.X && c.X < b.X && c.Y > a.Y && c.Y < b.Y;
        }

        public bool CursorInButton(RectangleShape button)
        {
            Vector2f a = button.Position;
            Vector2f b = a + button.Size;
            Vector2f c = (Vector2f)Mouse.GetPosition(_window);
            return CursorInRect(a, b, c);
        }
    }
}
